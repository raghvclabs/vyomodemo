//
//  LoginViewController.swift
//  Vyomo
//
//  Created by clicklabs on 4/1/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UIScrollViewDelegate {

    var activeTextField = UITextField()
    @IBOutlet weak var scrollView: UIScrollView!
    
  //MARK: TextField Variable Declration
  @IBOutlet weak var passwordTF: UITextField!
  @IBOutlet weak var phoneNumberTF: UITextField!
  @IBOutlet weak var loadingView: UIView!
  @IBOutlet weak var activityBar: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NSUserDefaults.standardUserDefaults().setValue("34535345fdgdfgdfgdgfgtdrtert", forKey: DeviceToken)

    var tapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewTapped")
    scrollView.addGestureRecognizer(tapRecognizer)

  }
    
  func scrollViewTapped() {
    self.view.endEditing(true)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    registerForKeyboardNotifications()
    
    if let accessToken = NSUserDefaults.standardUserDefaults().valueForKey(Access_token) as? String {
      let mainStoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
      let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("signUp") as SignUpViewController
      self.navigationController!.pushViewController(destinationView, animated: true)
    } else {
      
    }
    
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  
  //MARK: Tap Here Action
  
  @IBAction func registerButton(sender: AnyObject) {
  }
  
  //MARK: Tap Here Action
  
  @IBAction func tapHereButton(sender: AnyObject) {
    let mainStoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
    let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("signUp") as SignUpViewController
    self.navigationController!.pushViewController(destinationView, animated: true)
  }
  
  //MARK: Login Action
  
  @IBAction func loginButton(sender: AnyObject) {
    
    var token = NSUserDefaults.standardUserDefaults().valueForKey(DeviceToken) as? String
    
    if token == "" {
      token = "0"
    }
    
    if phoneNumberTF.text! == "" {
      var alert = UIAlertView()
      alert.message = "Please enter username"
      alert.addButtonWithTitle("Dismiss")
      alert.dismissWithClickedButtonIndex(0, animated: true)
      alert.show()
      
    } else if passwordTF.text! == "" {
      var alert = UIAlertView()
      alert.message = "Please enter password"
      alert.addButtonWithTitle("Dismiss")
      alert.dismissWithClickedButtonIndex(0, animated: true)
      alert.show()
      
    } else {
      startloading()
      let parameters = NSDictionary(objectsAndKeys:
        "\(phoneNumberTF.text)", "username",
        "\(passwordTF.text)", "password",
        "0", "longitude",
        "0", "latitude",
        "2", "device_type",
        token!, "device_token")
      
      json.session("service_provider_login", parameters: parameters, completion: {
        result in
        
        let jsonResult = result as NSDictionary
        println(jsonResult)
        
        let status = jsonResult["status"]! as Int
        let message = jsonResult["message"]! as NSString
        
        let data   = jsonResult["data"]! as NSDictionary
        
        dispatch_async(dispatch_get_main_queue()){
          
          if status == 200 {
            
            self.showAlert(message)
            
            if let resultArray = data["service_provider_data"] as? NSDictionary {
              
              if let id = resultArray["service_provider_id"]! as? Int {
                NSUserDefaults.standardUserDefaults().setInteger(id, forKey: StylistId)
              }
              
              if let role = resultArray["current_user_role"]! as? Int {
                NSUserDefaults.standardUserDefaults().setValue(role, forKey: Role_id)
              }
              
              if let businessId = resultArray["business_id"]! as? Int {
                NSUserDefaults.standardUserDefaults().setInteger(businessId, forKey: Business_id)
              }
              
              if let AccessTokenString = resultArray["access_token"]! as? String {
                NSUserDefaults.standardUserDefaults().setValue(AccessTokenString, forKey: Access_token)
              }
              
              if let stylistImageUrl = resultArray["image"] as? String {
                
              }
              NSUserDefaults.standardUserDefaults().synchronize()
              
              self.stopLoading()
              
              let mainStoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
              let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("viewAppointment") as ViewAppointmentViewController
              self.passwordTF.text = ""
              self.phoneNumberTF.text = ""
              self.navigationController!.pushViewController(destinationView, animated: true)
              
            } else {
              
              var alert = UIAlertView()
              alert.message = message
              alert.addButtonWithTitle("Dismiss")
              alert.dismissWithClickedButtonIndex(0, animated: true)
              alert.show()
              self.stopLoading()
            }
          }
        }
      })
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  func showAlert(string : NSString) {
    var alert = UIAlertView()
    alert.message = string
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
    self.stopLoading()
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    
    self.view.endEditing(true)
    
  }
  
  
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    
   // self.view.endEditing(true)
    
  }
  
  func startloading() {
    loadingView.hidden = false
    activityBar.startAnimating()
  }
  
  func stopLoading() {
    loadingView.hidden = true
    activityBar.stopAnimating()
  }
    
    //MARK: - Keyboard Management Methods
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        let activeTextFieldRect: CGRect? = activeTextField.frame
        let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
        if (!CGRectContainsPoint(aRect, activeTextFieldOrigin!)) {
            scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
        }
    }
    
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField!) {
        activeTextField = textField
    }
    
        
    func textFieldDidEndEditing(textField: UITextField!) {
            activeTextField = UITextField()
        }


  
}
