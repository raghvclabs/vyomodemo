//
//  MoreController.swift
//  Vyomo
//
//  Created by Click Labs on 4/7/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class MoreController: UIViewController ,UITableViewDelegate{
  var sectionHeader = ["Profile Setup","Service Setup","Service Setup"]
  var profileMenu = ["View Saloon Profile","Personal Info","Username & Password","Logout"]
  var serviceOneMenu = ["Services","Availibility"]
  var serviceTwoMenu = ["Help and Support","Terms of Service","EULA","FAQ"]
  @IBOutlet var StylishNameView: UIView!
  var flag = true
  @IBOutlet var moreTable: UITableView!
  override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup arfter loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  @IBAction func managerButton(sender: AnyObject) {
    if flag {
    StylishNameView.hidden = true
      flag = false
    } else {
      StylishNameView.hidden = false
      flag = true
    }
  }
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return sectionHeader[section]
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 1{
      return 2
    }else{
       return 4
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("more") as UITableViewCell
    cell.textLabel?.font =  UIFont(name: "Raleway-Regular" , size: 18)
    if indexPath.section == 0{
       cell.textLabel?.text = profileMenu[indexPath.row]
    }else if indexPath.section == 1{
    cell.textLabel?.text = serviceOneMenu[indexPath.row]
    }else{
       cell.textLabel?.text = serviceTwoMenu[indexPath.row]
    }
//    let image = UIImage(named: "small_right_arrow_icon_3x.png") as UIImage?
//    let button   = UIButton.buttonWithType(UIButtonType.System) as UIButton
//    println(cell.frame.size)
//    button.frame = CGRectMake(cell.frame.size.width-90, 10, 20, 20)
//      //CGRectMake(320, 20, 20, 20)
//    button.setImage(image, forState: .Normal)
//    button.tintColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
////    button2.addTarget(self, action: "Action2", forControlEvents:.TouchUpInside)
//    cell.addSubview(button)
    return cell
    
  }
  
  func transfer(storyboard :String){
    
    
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
