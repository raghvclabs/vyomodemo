//
//  SalonAddressViewController.swift
//  Vyomo
//
//  Created by Bhasker on 3/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SalonAddressViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate,UISearchBarDelegate, UITableViewDelegate {

    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var mapView: GMSMapView!
    var manager = CLLocationManager()
    
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var address = ""
    var city = ""
    var state = ""
    var country = ""
    var zipCode = ""
    
    var marker = GMSMarker()
    
    var backButtonClickedAgain = true
    
    @IBAction func backButtonClicked(sender: AnyObject) {
        self.view.endEditing(true)
        if backButtonClickedAgain {
            println("pass address")
          let n: Int! = self.navigationController?.viewControllers?.count
          
          let prevViewController = self.navigationController?.viewControllers[n-2] as AddSaloonInfoViewController
          
            prevViewController.business_address = address
           prevViewController.business_country = country
           prevViewController.business_state = state
           prevViewController.business_city = city
           prevViewController.business_zip_code = zipCode
           prevViewController.business_longitude = "\(longitude)"
          prevViewController.business_latitude = "\(latitude)"

          
          
          
          
          self.navigationController?.popViewControllerAnimated(true)
          

        } else {
            TitleLabel.text = "Select Your Address"
            backButtonClickedAgain = true
        }
      
      
    }
  
    
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
        
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            manager.startUpdatingLocation()
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if let location = locations.first as? CLLocation {
            
            updateLocationMarker(location)
            manager.stopUpdatingLocation()
        }
    }
    
    func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        println("gps")
        manager.startUpdatingLocation()
        return true
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        reverseGeocodeCoordinate(marker.position)
    }
    
    func mapView(mapView: GMSMapView!, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
        updateLocationMarker(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
        reverseGeocodeCoordinate(coordinate)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {

                println(address.lines)
                println("\(self.latitude)  \(self.longitude)")
                
                if address.country != nil {
                    self.country = address.country
                } else {
                    let alertController = UIAlertController(title: "Error", message: "Invalid location", preferredStyle: .Alert)
                    let OkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action) in
                        return
                        }
                    alertController.addAction(OkAction)
                    self.presentViewController(alertController, animated: true) { return
                    }
                    return
                }
                if address.thoroughfare != nil {
                    self.address = address.thoroughfare
                }
                if address.subLocality != nil {
                    self.address = self.address + address.subLocality
                }
                if address.locality != nil {
                    self.city = address.locality
                }
                if address.administrativeArea != nil {
                    self.state = address.administrativeArea
                }
                if address.postalCode != nil {
                    self.zipCode = address.postalCode
                }
                
            }
        }
    }

    func updateLocationMarker(location : CLLocation) {
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        marker.map = nil
        marker = GMSMarker()
        var cameraUpdate = GMSCameraUpdate.setTarget(location.coordinate, zoom: 15)
        mapView.animateWithCameraUpdate(cameraUpdate)
        let markerIcon = UIImage(named: "small_locater_icon_3x.png")
        marker.icon = markerIcon
        marker.position = location.coordinate
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Google places auto complete
    
    
    // search bar implementation
    
    var places:[String] = []
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        TitleLabel.text = "Location"
        backButtonClickedAgain = false
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchText == "") {
            places = []
            tableView.hidden = true
        } else {
            places = []
            getPlaces(searchText)
        }
    }
    
    func getPlaces(searchString: String) {
        
        var request = requestForSearch(searchString)
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithRequest(request) { (data, response, error) in
            
            self.handleResponse(data, response: response as? NSHTTPURLResponse, error: error)
        }
        task.resume()
    }
    
    func requestForSearch(searchString: String) -> NSURLRequest {
        let params = ["input": searchString, "types": "address", "key":googleApiServerKey]
        
        return NSMutableURLRequest(
            URL: NSURL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")!
        )
    }
    
    func query(parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in sorted(Array(parameters.keys), <) {
            let value: AnyObject! = parameters[key]
            components += [(escape(key), escape("\(value)"))]
        }
        
        return join("&", components.map{"\($0)=\($1)"} as [String])
    }
    
    func escape(string: String) -> String {
        let legalURLCharactersToBeEscaped: CFStringRef = ":/?&=;+!@#$()',*"
        return CFURLCreateStringByAddingPercentEscapes(nil, string, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue)
    }
    
    func handleResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            println("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            //create alert view controller
            return
        }
        if response == nil {
            println("GooglePlacesAutocomplete Error: No response from API")
            return
        }
        if response.statusCode != 200 {
            println("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            return
        }
        
        var serializationError: NSError?
        var json: NSDictionary = NSJSONSerialization.JSONObjectWithData(data,options: NSJSONReadingOptions.MutableContainers,error: &serializationError) as NSDictionary
        
        if let error = serializationError {
            println("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            return
        }
        
        // Perform table updates on UI thread
        dispatch_async(dispatch_get_main_queue(),{
            
            /*
            if let predictions = json["predictions"] as? Array<AnyObject> {
                self.places = predictions.map { (prediction: AnyObject) -> Place in
                    return Place(
                        id: prediction["place_id"] as String,
                        description: prediction["description"] as String
                    )
                }
                */
            println(json)
            if let predictions = json["predictions"] as? NSArray {
                for index in 0..<predictions.count {
                    if let placeOut = predictions[index] as? NSDictionary {
                        if let placeDescription = placeOut["description"] as? NSString {
                            self.places.append("\(placeDescription)")
                        }
                }
                println(self.places[index])

            }
            self.tableView.reloadData()
            self.tableView.hidden = false
                //self.delegate?.placesFound?(self.places)
            }
        })
    }

    // table view implementation

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel!.text = places[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("selected \(places[indexPath.row])")
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject((places[indexPath.row]), forKey: UserNameKeyConstant)
        getLocationFromAddress(places[indexPath.row])
    
        //delegate?.placeSelected?(self.places[indexPath.row])
    }

    
    //MARK: -get location from address
    
    func getLocationFromAddress(address:String) {
        let urlPath: String = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&key=AIzaSyDaGfeKX4Fkh-1nFajQyEWwQyAAa4vbHgA"
        
        let urlEncodedString = urlPath.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)

        let url = NSURL(string: urlEncodedString!)
        var request = NSURLRequest(URL: url!)
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithRequest(request) { (data, response, error) in
            self.handleGetLocationResponse(data, response: response as? NSHTTPURLResponse, error: error)
        }
        task.resume()
    }
    
    func handleGetLocationResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            println("GoogleGeocode Error: \(error.localizedDescription)")
            //create alert view controller
            return
        }
        if response == nil {
            println("GoogleGeocode Error: No response from API")
            return
        }
        if response.statusCode != 200 {
            println("GoogleGeocode Error: Invalid status code \(response.statusCode) from API")
            return
        }
        
        var serializationError: NSError?
        var json: NSDictionary = NSJSONSerialization.JSONObjectWithData(data,options: NSJSONReadingOptions.MutableContainers,error: &serializationError) as NSDictionary
        
        if let error = serializationError {
            println("GoogleGeocode Error: \(error.localizedDescription)")
            return
        }
        
        dispatch_async(dispatch_get_main_queue(),{
            println(json)
            if let results = json["results"] as? NSArray {
                    if let index = results[0] as? NSDictionary {
                        if let geometry = index["geometry"] as? NSDictionary {
                            if let location = geometry["location"] as? NSDictionary {
                                if let lat = location["lat"] as? Double {
                                    println(lat)
                                    self.latitude = lat as CLLocationDegrees
                                }
                                if let lng = location["lng"] as? Double {
                                    println(lng)
                                    self.longitude = lng as CLLocationDegrees
                                }
                            }
                        }
                    }
                    //println(self.places[index])
                    
                }
            let location = CLLocation(latitude: self.latitude, longitude: self.longitude)
            
            
                //self.tableView.reloadData()
            self.tableView.hidden = true
            self.view.endEditing(true)
            self.backButtonClickedAgain = true
            self.updateLocationMarker(location)

            
        })
    }

    //MARK: - Keyboard Management Methods
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        tableView.contentInset = contentInsets
        tableView.scrollIndicatorInsets = contentInsets
        
    }
    
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        tableView.contentInset = contentInsets
        tableView.scrollIndicatorInsets = contentInsets
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
