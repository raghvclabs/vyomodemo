//
//  selectServicesViewController.swift
//  Vyomo
//
//  Created by click Labs on 3/30/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//


// tag 36 for subtiltle label 37 for title label
// tag 38 for button

import UIKit

class SelectServicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var activityBar: UIActivityIndicatorView!
    @IBOutlet weak var categoryTable: UITableView!
    //var category = [String]()
    // whether category is selected or not --- 0 = not selected , 1 = selected
    var selectState = [Int]()
    //let categoriesAvailable = SelectCategoryData()
    //var selectedCategories = [String]()
    //var data : NSDictionary!
    //var count = 0
    
    //var categoryIDArray = [Int]()
    var categoryNameArray = [String]()
    
    //var serviceListArray = [NSDictionary]()
    var serviceName = [[String]]()
    var indexCountArray = [[Int]]()
    var selectedIndexArray = [Int]()
    
    var service = [String]()
    var businessServiceId = [Int]()
    var serviceDuration = [Int]()
    var serviceProductCost = [Int]()
    var serviceCost = [Int]()
    
    var selectedService = [String]()
    var selectedBusinessServiceId = [Int]()
    var selectedServiceDuration = [Int]()
    var selectedServiceProductCost = [Int]()
    var selectedServiceCost = [Int]()
    
    //var selectedId = ""
    var selectedDuration = 0
    var selectedProductCost = 0
    var selectedCost = 0
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        let n: Int! = self.navigationController?.viewControllers?.count
        let prevViewController = self.navigationController?.viewControllers[n-2] as AddAppointmentViewController
        
        for element in selectedServiceDuration {
            selectedDuration += element
            println(selectedDuration)
        }
        for element in selectedServiceProductCost {
            selectedProductCost += element
        }
        for element in selectedServiceCost {
            selectedCost += element
        }
        println(selectedService)
        println(selectedBusinessServiceId)
        
        println(selectedDuration)
        println(selectedProductCost)
        println(selectedCost)

      
        prevViewController.selectedService = selectedService
        //prevViewController.selectState = selectState

        prevViewController.selectedBusinessServiceId = selectedBusinessServiceId
        prevViewController.selectedIndexArray = selectedIndexArray
        
        prevViewController.selectedDuration = selectedDuration
        prevViewController.selectedProductCost = selectedProductCost
        prevViewController.selectedCost = selectedCost

        self.navigationController?.popViewControllerAnimated(true)
      

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        println(selectedIndexArray)
        println(selectState)
        
        let json = JSON()
        var arr_parameters = NSDictionary(objectsAndKeys: "\(AccessToken)","access_token")
        dispatch_async(dispatch_get_main_queue()){
            json.session("get_category_services", parameters: arr_parameters, completion: {
                response in
                var data = response as NSDictionary
                println(data)
                self.extractData(data)
        })
            
            self.activityBar.startAnimating()
            self.activityBar.hidden = false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return serviceName.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categoryNameArray[section]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serviceName[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("service") as ServicesTableViewCell
        cell.backgroundColor = LightGrayColor
        let index = indexCountArray[indexPath.section][indexPath.row]

        cell.titleLabel.text = serviceName[indexPath.section][indexPath.row]
        cell.subtitleLabel.text = "$\(serviceCost[index]) for \(serviceDuration[index])"
        
        if selectState[index] == 0 {
            cell.selectButton.setBackgroundImage(UIImage(named: "unselecteded_icon_3x.png"), forState: UIControlState.Normal)
            
        } else {
            cell.selectButton.setBackgroundImage(UIImage(named: "selected_icon_3x.png"), forState: UIControlState.Normal)
        }

        cell.selectButton.addTarget(self, action: "selectButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        cell.backgroundColor = LightGrayColor
        println(indexPath.row)
    }
    /*
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 78.0
    }*/
    
    func selectButtonClicked(sender : UIButton) {
        var  index = 0
        var position: CGPoint = sender.convertPoint(CGPointZero, toView: self.categoryTable)
        if let indexPath = self.categoryTable.indexPathForRowAtPoint(position)
        {
            index = indexCountArray[indexPath.section][indexPath.row]
            println(index)
            
        }
        //var index = sender.tag - 38
        if selectState[index] == 0 {
            sender.setBackgroundImage(UIImage(named: "selected_icon_3x.png"), forState: UIControlState.Normal)
            selectState[index] = 1
            selectedIndexArray.append(index)
            selectedService.append(service[index])
            selectedBusinessServiceId.append(businessServiceId[index])
            selectedServiceCost.append(serviceCost[index])
            selectedServiceProductCost.append(serviceProductCost[index])
            selectedServiceDuration.append(serviceDuration[index])

        } else {
            sender.setBackgroundImage(UIImage(named: "unselecteded_icon_3x.png"), forState: UIControlState.Normal)
            selectState[index] = 0
            for i in 0...selectedService.count - 1 {
                if selectedService[i] == service[index] {
                    selectedService.removeAtIndex(i)
                    selectedIndexArray.removeAtIndex(i)

                    selectedBusinessServiceId.removeAtIndex(i)
                    selectedServiceCost.removeAtIndex(i)
                    selectedServiceProductCost.removeAtIndex(i)
                    selectedServiceDuration.removeAtIndex(i)
                    break
                }
            }
        }
    }
    
    /*@IBAction func continueButtonPressed(sender : AnyObject) {
        let othstoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
        let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("availability") as SetAvailabilityViewController
        let secondViewAnimation = Transition()
        secondViewAnimation.transitionForward(self, destViewController: destinationView)
    }*/
    
    func extractData(data : NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            if let categoryArray = data["category_services_list"] as? NSArray {
                var index = 0
                for category in categoryArray {
                    var nameArray = [String]()
                    var count = [Int]()
                    /*if let categoryID = category["category_id"]! as? Int {
                        self.categoryIDArray.append(categoryID)
                    }*/
                    if let categoryNameArray = category["category_name"]! as? String {
                        self.categoryNameArray.append(categoryNameArray)
                    }
                    if let serviceList = category["service_list"] as? NSArray {
                        
                        for list in serviceList {
                            if let serviceName = list["service_name"] as? String     {
                                self.service.append(serviceName)
                                nameArray.append(serviceName)
                                count.append(index)
                                index++
                            }
                            if let serviceProductCost = list["service_product_cost"] as? Int {
                                self.serviceProductCost.append(serviceProductCost)
                            }
                            if let serviceCost = list["service_cost"] as? Int {
                                self.serviceCost.append(serviceCost)
                            }
                            if let serviceDuration = list["service_duration"] as? Int {
                                self.serviceDuration.append(serviceDuration)
                            }
                            if let businessServiceId = list["business_service_id"] as? Int {
                                self.businessServiceId.append(businessServiceId)
                            }

                        }
                        
                    }
                    self.serviceName.append(nameArray)
                    self.indexCountArray.append(count)
                }
            }
            println(self.serviceName)
            println(self.service)
            println(self.indexCountArray)

            for i in 0...self.service.count - 1 {
                self.selectState.append(0)
            }
            println(self.selectedIndexArray)
            for index in self.selectedIndexArray {
                self.selectState[index] = 1
                
                //self.selectedIndexArray.append(index)
                self.selectedService.append(self.service[index])
                self.selectedBusinessServiceId.append(self.businessServiceId[index])
                self.selectedServiceCost.append(self.serviceCost[index])
                self.selectedServiceProductCost.append(self.serviceProductCost[index])
                self.selectedServiceDuration.append(self.serviceDuration[index])

            }
            println(self.selectState)
            self.categoryTable.reloadData()
            self.activityBar.stopAnimating()
            self.activityBar.hidden = true
        }
    }
}

/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
// Get the new view controller using segue.destinationViewController.
// Pass the selected object to the new view controller.
}
*/
