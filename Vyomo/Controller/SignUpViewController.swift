//
//  ViewController.swift
//  Vyomo
//
//  Created by clicklabs on 3/3/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

// tags 10 to 17 for text fields in sign up screen

class SignUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
  @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
  @IBOutlet weak var activitybar: UIActivityIndicatorView!
    var activeTextField = UITextField()
    @IBOutlet var textFieldArray: [UITextField]!
    
    @IBOutlet weak var pickerCustomView: UIView!

    @IBOutlet weak var iAmPickerView: UIPickerView!
    var iAmPickerArray = ["Stylist","Manager"]
    @IBOutlet weak var stylistOrManagerButton: UIButton!
    @IBOutlet weak var stylistOrManagerLabel: UILabel!

    @IBOutlet weak var countryCodePickerView: UIPickerView!
    var countryCodeArray = ["+91", "+11", "+01", "+44"]
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    @IBOutlet weak var salonNameButton: UIButton!
    @IBOutlet weak var salonSelectedLabel: UILabel!
    var salonSelected = ""
  
    var businessId = BuisnessIdForRegistration.toInt()
    
    var isTermsAgreed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var tapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewTapped")
        scrollView.addGestureRecognizer(tapRecognizer)
    }
    
    func scrollViewTapped() {
        self.view.endEditing(true)
        //iAmPickerView.hidden = true
        //countryCodePickerView.hidden = true
        pickerCustomView.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
        if salonSelected != "" {
            salonSelectedLabel.text = salonSelected
            salonSelectedLabel.textColor = PinkColor
        }
      /*if salonSelectedLabel.text != "Add New" || salonSelectedLabel.text != "Select Salon" {
        salonSelectedLabel.textColor = PinkColor
      }*/
      
        var systemVersion = UIDevice.currentDevice().systemVersion
        var checkSystemVersion = (systemVersion as NSString).doubleValue
        if checkSystemVersion >= 8 {
            // Register for push in iOS 8
            let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert | UIUserNotificationType.Sound | UIUserNotificationType.Badge, categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        } else {
            // Register for push in iOS 7
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func stylistOrManagerSelect(sender: AnyObject) {
        self.view.endEditing(true)
        countryCodePickerView.hidden = true
        pickerCustomView.hidden = false
        iAmPickerView.hidden = false
    }
    
    @IBAction func countryCodeSelect(sender: AnyObject) {
        self.view.endEditing(true)
        iAmPickerView.hidden = true
        pickerCustomView.hidden = false
        countryCodePickerView.hidden = false
    }
    
    @IBAction func donePickerSelect(sender: AnyObject) {
        pickerCustomView.hidden = true
        //countryCodePickerView.hidden = true
        //iAmPickerView.hidden = true
    }
    
    @IBAction func agreedButtonClicked(sender: UIButton) {
        //self.view.endEditing(true)

        if isTermsAgreed {
            sender.setBackgroundImage(UIImage(named: "Unchecked_box_icon_3x.png"), forState: UIControlState.Normal)
            isTermsAgreed = false
        } else {
            sender.setBackgroundImage(UIImage(named: "checked_icon_3x.png"), forState: UIControlState.Normal)
            isTermsAgreed = true
        }
    }
    
    @IBAction func joinNowPressed(sender: AnyObject) {
        self.view.endEditing(true)
  
       let token = NSUserDefaults.standardUserDefaults().valueForKey(DeviceToken) as? String
      
        var checksCount = 0
        var fieldsEmptyCount  = 0
        
        for textField in textFieldArray {
            if textField.text == "" {
                fieldsEmptyCount++
                if fieldsEmptyCount == 8 {
                    showAlert("All fields are empty")
                    return
                }
            }
        }

        for (index, textField) in enumerate(textFieldArray) {
            
            if textField.text == "" {
                switch index {
                case 0:
                    showAlert("Please Enter Username ")
                case 1:
                    showAlert("Please Enter First")
                case 2:
                    showAlert("Please Enter Last Name")
                case 3:
                    showAlert("Please Enter Phone no.")
                case 4:
                    showAlert("Please Enter Email")
                case 5:
                    showAlert("Please Enter Password")
                case 6:
                    showAlert("Please Enter Confirm Password")
                case 7:
                    showAlert("Please Enter Promotor Phone no.")
                default:
                    println("default")
                }
                return
                
            } else {
                switch index {
                case 0:
                    var length = "\(textField.text.endIndex)"
                    if length.toInt() > 5 {
                        checksCount++
                    } else {
                        println("enter username more than 6")
                        showAlert("enter username more than 6")
                        return
                    }
                case 1:
                    checksCount++
                case 2:
                    checksCount++
                case 3:
                    if validatePhoneNumber(textField.text) {
                        println("validated phone no.")
                        checksCount++
                    } else {
                        println("not valid phone no.")
                        showAlert("Not a valid phone no.")
                        return
                    }
                case 4:
                    if validateEmail(textField.text) {
                        println("validated email")
                        checksCount++
                    } else {
                        println("not valid email")
                        showAlert("Not valid email address")
                        return
                    }
                case 5:
                    var length = "\(textField.text.endIndex)"
                    if length.toInt() > 5 {
                        checksCount++
                    } else {
                        println("enter password more than 6")
                        showAlert("enter password more than 6")
                        return
                    }
                case 6:
                    if textFieldArray[5].text == textField.text {
                        println("pass ok")
                        checksCount++
                    } else {
                        println("pass not ok")
                        showAlert("Password not matching")
                        return
                    }
                case 7:
                    if validatePhoneNumber(textField.text) {
                        println("validated phone no.")
                        checksCount++
                    } else {
                        println("not valid phone no.")
                        showAlert("Not a valid phone no.")
                        return
                    }
                default:
                    println("default")
                }
            }
        }
        println(checksCount)
        
        if isTermsAgreed {
            checksCount++
        } else {
            println("not agreed")
            showAlert("Terms not agreed")
            return
        }
        
        if salonSelected != "" {
            checksCount++
        } else {
            println("Please select a salon")
            showAlert("Please select a salon")
            return
        }
        
        if(checksCount == 10) {
            println("Proceed")
    blurView.hidden = false
          activitybar.startAnimating()
            var json = JSON()
            let null = NSNull()
            var arr_parameters = NSDictionary(objectsAndKeys:
            "\(textFieldArray[0].text)", "username",
            "\(textFieldArray[5].text)", "password",
            "\(textFieldArray[1].text)", "first_name",
            "\(textFieldArray[2].text)", "last_name",
            "\(countryCodeLabel.text)", "phone_number_prefix",
            "\(textFieldArray[3].text)", "phone_number",
            "\(textFieldArray[4].text)", "email_id",
            "0", "longitude",
            "0", "latitude",
            "\(textFieldArray[7].text)", "sale_promotor_phone_number",
            "2", "device_type",
            token!, "device_token",
            "\(RoleId)", "role_id",
            "\(BuisnessIdForRegistration)", "business_id")
          dispatch_async(dispatch_get_main_queue()){
            json.session("service_provider_sign_up", parameters: arr_parameters, completion: {
                result in
              if let id = result["service_provider_id"] as? Int {
                ServiceProviderId = id
              }

                if let resultArray = result["access_token"] as? String {
                  AccessToken = resultArray
                  println(AccessToken)
                  dispatch_async(dispatch_get_main_queue()){
                    self.blurView.hidden = true
                    let mainStoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
                    let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("category") as SelectCategoryViewController
                    destinationView.stringPass = "hello"
                    self.navigationController!.pushViewController(destinationView, animated: true)
                    }
                    
                } else {
                    self.showAlert("Sign up not successful. Try again.")
                }
            })
          }
          
        } else {
            var alert = UIAlertView()
          alert.message = message
          alert.addButtonWithTitle("Dismiss")
          alert.dismissWithClickedButtonIndex(0, animated: true)
          alert.show()
          self.blurView.hidden = true
        }

    }
    
    func showAlert(message : String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let OkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action) in
            return        }
        alertController.addAction(OkAction)
        self.presentViewController(alertController, animated: true) {
        }
    }
    
    func validateEmail(email : String ) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = email.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        return result

    }
    
    func validatePhoneNumber (phoneNum : String) -> Bool {
        let phoneRegEx = "^[789]\\d{9}$"
        let range = phoneNum.rangeOfString(phoneRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        return result
    }
    
    
  @IBAction func salonName(sender: AnyObject) {
    let othstoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("searchScreen") as SearchScreenViewController
    self.navigationController!.pushViewController(destinationView, animated: true)
  }
    
    //MARK: -Picker View implemented
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == iAmPickerView {
            return iAmPickerArray.count
        } else {
            return countryCodeArray.count
        }
    }
    
    /*func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    
    return iAmPickerArray[row]
    }*/
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerLabel = view as UILabel!
        if view == nil {
            pickerLabel = UILabel()
            pickerLabel.font = UIFont(name: "Raleway", size: 22.0)
            pickerLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
            pickerLabel.textAlignment = .Center
        }
        
        if pickerView == iAmPickerView {
            pickerLabel.text = iAmPickerArray[row]
        } else {
            pickerLabel.text = countryCodeArray[row]
        }
        
        return pickerLabel
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == iAmPickerView {
            if iAmPickerArray[row] == "Manager"{
                salonSelectedLabel.text = "Add New"
                //salonSelectedLabel.textColor = UIColor(red: 188/255, green: 188/255, blue: 188/255, alpha: 1)
                //salonNameButton.setImage(UIImage(named: "small_grey_right_arrow_3x.png"), forState: UIControlState.Normal)
                RoleId = 2

            } else {
                salonSelectedLabel.text = "Select Salon"
                //salonSelectedLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
                //salonNameButton.setImage(UIImage(named: "small_right_arrow_icon_3x.png"), forState: UIControlState.Normal)
                RoleId = 1
            }
            stylistOrManagerLabel.text = iAmPickerArray[row]
        } else {
            countryCodeLabel.text = countryCodeArray[row]
            countryCodeLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
        }
    }
    
    //MARK: - Keyboard Management Methods

    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        let activeTextFieldRect: CGRect? = activeTextField.frame
        let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
        if (!CGRectContainsPoint(aRect, activeTextFieldOrigin!)) {
            scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
        }
    }
    
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    
    //MARK: - UITextField Delegate Methods and Customising Keyboard
    
    // tags 10 to 17 for text fields in sign up screen
    
    func textFieldDidBeginEditing(textField: UITextField!) {
        activeTextField = textField
        iAmPickerView.hidden = true
        countryCodePickerView.hidden = true
        if inputAccessoryView == nil {
            let keyboardTopCustomView = UIToolbar()
            keyboardTopCustomView.sizeToFit()
            
            let prevButton = UIBarButtonItem(image: UIImage(named: "small_grey_left_arrow_3x.png"), style: UIBarButtonItemStyle.Plain, target: self, action:"previousTextField" )
            
            let nextButton = UIBarButtonItem(image: UIImage(named: "small_grey_right_arrow_3x.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "nextTextField")
            
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
            
            let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "endEditingNow")
            
            if textField.tag == 17 {
                nextButton.enabled = false
            }
            if textField.tag == 10 {
                prevButton.enabled = false
            }
            
            var toolbarButtons = [prevButton, nextButton,flexibleSpace, doneButton]
            keyboardTopCustomView.setItems(toolbarButtons, animated: false)
            
            textField.inputAccessoryView = keyboardTopCustomView
        }
    }
    
    func previousTextField() {
        if activeTextField.tag != 10 {
            let prevTextField = textFieldArray[activeTextField.tag-11].becomeFirstResponder()
        }
    }
    
    func nextTextField() {
        if activeTextField.tag != 17 {
            textFieldArray[activeTextField.tag-9].becomeFirstResponder()
        }
    }
    
    func endEditingNow() {
        activeTextField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(textField: UITextField!) {
        activeTextField = UITextField()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // restricting length of phone no. to 10 characters
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 13 || textField.tag == 17 {
            let length = "\(textField.text.endIndex)"
            if length.toInt() < 10 || string == ""{
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
}

