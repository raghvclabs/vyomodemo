//
//  SaloonProfileViewController.swift
//  Vyomo
//
//  Created by Click Labs on 4/7/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SaloonProfileViewController: UIViewController, UITableViewDelegate {
  var ratingView :UIView = UIView()
  @IBOutlet var ratingTable: UITableView!
  @IBOutlet var serviceTable: UITableView!
  @IBOutlet var profileImageView: UIImageView!
  override func viewDidLoad() {
    super.viewDidLoad()
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
    self.profileImageView.clipsToBounds = true
    self.profileImageView.layer.borderWidth = 8.0
    self.profileImageView.layer.borderColor = UIColor.blackColor().CGColor
    ratingTable.rowHeight = 146
     self.serviceTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
     self.ratingTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    // Do any additional setup after loading the view.
  }
  
  
  @IBAction func servicesButton(sender: AnyObject) {
    serviceTable.hidden = false
  }
  
  
  @IBAction func reviewsButton(sender: AnyObject) {
    serviceTable.hidden = true
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "apple"
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView.tag == 11{
      return 4
    }else {
      return 3
    }
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
    
    if tableView.tag == 11{
      for  lbl in cell.subviews {
        if lbl.isKindOfClass(UIView)
        {
          lbl.removeFromSuperview()
        }
      }
      ratingView = UIView(frame: CGRectMake(10, 30, cell.frame.size.width, cell.frame.size.height/4))
      cell.addSubview(ratingView)
      createRatingView()
      var subTitleLabel = UILabel(frame: CGRectMake(10, 63,cell.frame.size.width , 30))
      subTitleLabel.text = "Delivery"
      subTitleLabel.font = UIFont(name: "Raleway-Regular" , size: 18)
      //subTitleLabel.textAlignment = NSTextAlignment.Center
      cell.addSubview(subTitleLabel)
    }else{
      
    }
    return cell
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
    func scrollViewDidScroll(sender: UIScrollView ){
      if (sender.contentOffset.x != 0) {
        var offset: CGPoint  = sender.contentOffset
        offset.x = 0
        sender.contentOffset = offset
      }
    }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  func createRatingView(){
      var starButtonOriginX = ratingView.center.x - 200
      for var i = 0; i < 5; i++ {
      var ratingButton = UIImageView(frame: CGRectMake(starButtonOriginX, 5, 30, 30))
      //ratingButton.backgroundColor = PinkColor
      ratingButton.tag = 600 + i
      //ratingButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
      var image = UIImage(named: "star-filled.png")
         ratingButton.image = image
        ratingButton.image = ratingButton.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
       ratingButton.tintColor = PinkColor
      ratingView.addSubview(ratingButton)
      starButtonOriginX = starButtonOriginX + 45
    }
  }
  
  func buttonAction(){
    
  }
  
}
