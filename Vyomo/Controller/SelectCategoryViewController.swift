import UIKit

class SelectCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  

  @IBOutlet weak var loadingView: UIView!
  @IBOutlet weak var activityBar: UIActivityIndicatorView!
  @IBOutlet weak var categoryTable: UITableView!
  var category = [String]()
  // whether category is selected or not 0 = not selected 1 = selected
  var selectState = [Int]()
  var selectedCategories = [String]()
  var serviceIdString = ""
  var selectedServicesId = [String]()
  var categoryIDArray = [Int]()
  var categoryNameArray = [String]()
  var serviceListArray = [[NSDictionary]]()
  var selectedServices = [[NSDictionary]]()
  var stringPass : String?
  var serviceList = [[NSDictionary]]()
  override func viewDidLoad() {
    startLoading()
    super.viewDidLoad()
    //let json = JSON()
    var arr_parameters = NSDictionary(objectsAndKeys: "\(AccessToken)","access_token")
    dispatch_async(dispatch_get_main_queue()){
      json.session("get_category_services", parameters: arr_parameters, completion: {
        response in
        let data = response as NSDictionary
        //println(data)
        dispatch_async(dispatch_get_main_queue()){
          self.extractData(data)
        }
      })      
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

    return category.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("category") as UITableViewCell
    cell.backgroundColor = LightGrayColor
    
      if let selectButton : UIButton = cell.viewWithTag(600) as? UIButton {
        selectButton.tag = 600 + indexPath.row
        selectButton.addTarget(self, action: "categorySelectButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
      }
      
      let textLabel = cell.viewWithTag(700) as? UILabel
      textLabel?.font = UIFont(name: "Raleway", size: 18)
      textLabel?.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
      textLabel?.text = category[indexPath.row]
    
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let cell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
    cell.backgroundColor = LightGrayColor
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 78.0
  }
  
  func categorySelectButtonClicked(sender : UIButton) {
    var index = sender.tag - 600
    //println(index)
    if selectState[index] == 1 {
      sender.setBackgroundImage(UIImage(named: "unselecteded_icon_3x.png"), forState: UIControlState.Normal)
      selectState[index] = 0
      for i in 0...selectedCategories.count - 1 {
        if selectedCategories[i] == category[index] {
          selectedCategories.removeAtIndex(i)
          selectedServices.removeAtIndex(i)
          break
        }
      }
      
    } else {
      sender.setBackgroundImage(UIImage(named: "selected_icon_3x.png"), forState: UIControlState.Normal)      
      selectState[index] = 1
      selectedCategories.append(category[index])
      selectedServices.append(serviceList[index])
    }
  }
  
  @IBAction func continueButtonPressed(sender : AnyObject) {
    startLoading()
    for serviceArray in selectedServices {
      for service in serviceArray {
        let id = service["business_service_id"]! as Int
        selectedServicesId.append("\(id)")
      }
    }
    
    for counter in 0..<selectedServicesId.count - 1 {
      serviceIdString += selectedServicesId[counter] + ","
    }
    
    serviceIdString += selectedServicesId[selectedServicesId.count - 1]
    
    var arr_parameters = NSDictionary(objectsAndKeys: "\(AccessToken)","access_token",serviceIdString,"business_service_ids")
    dispatch_async(dispatch_get_main_queue()){
      json.session("select_category_services", parameters: arr_parameters, completion: {
        response in
        dispatch_async(dispatch_get_main_queue()){
          let othstoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
          let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("availability") as SetAvailabilityViewController
          self.stopLoading()
          self.navigationController!.pushViewController(destinationView, animated: true)
        }
      })
    }    
  }
  
  func extractData(data : NSDictionary) {
    let categoryDictionary : [NSDictionary] = data["category_services_list"] as [NSDictionary]
    if categoryDictionary.count != 0 {
      for category in categoryDictionary {
        let categoryID : Int = category["category_id"]! as Int
        self.categoryIDArray.append(categoryID)
        self.categoryNameArray.append(category["category_name"]! as String)
        let services = category["service_list"]! as [NSDictionary]
        serviceListArray.append(services)
      }
    }
    
    serviceList = serviceListArray
    selectedServices = serviceList
    category = categoryNameArray
    selectedCategories = category
    for i in 0...category.count - 1 {
      selectState.append(1)
    }
    categoryTable.reloadData()
    stopLoading()
  }
  
  func startLoading() {
    loadingView.hidden = false
    activityBar.startAnimating()
  }
  
  func stopLoading() {
    loadingView.hidden = true
    activityBar.stopAnimating()
  }
}