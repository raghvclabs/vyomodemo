//
//  AddSaloonInfoViewController.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.

// For cells being added use as accordingly below
// 0 title and subtitle
// 1 select button + subtitle
// 2 select + button
// 3 phone number
// 4 title + textfield
// 5 text Area

import UIKit


class AddSaloonInfoViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate , UITextFieldDelegate , UITextViewDelegate , UIPickerViewDataSource, UIPickerViewDelegate{
  @IBOutlet weak var infoTable: UITableView!
  @IBOutlet weak var picker: UIView!
  @IBOutlet weak var pickerOutlet: UIPickerView!
  
  var sectionElements = ["Role","Salon Name","Address","Phone Number","Salon Type"]
  var sectionTwoElements = ["Front View","Inside View","Side View","Description"]
  var sectionElementsTypesArray = [0,4,1,3,2]
  var sectionTwoElementsTypesArray = [1,1,1,5]
  var pinAddressString:String?
  var net: Net!
  var buisness_name = ""
  var business_address = "Pin Your Address"
  var business_country = ""
  var business_state = ""
  var business_city = ""
  var business_zip_code = ""
  var business_longitude = ""
  var business_latitude = ""
  var business_description = ""
  var business_type_id = "-1"
  var front_view_images_count = 0
  var inside_view_images_count = 0
  var side_view_images_count = 0
  var business_phone_number_prefix = "+91"
  var business_phone_number = ""
  var characterCount = 0
  var countLabel: UILabel!
  var pickerData = []
  var pickerReturnData = []
  var selectedRow = 0
  var imageArray = [UIImage]()
  var frontImageArray = [UIImage]()
  var sideImageArray = [UIImage]()
  var insideImageArray = [UIImage]()
  var salonTypes = [String]()
  var salonId = [Int]()
  var dataCheck = 0
  override func viewDidLoad() {
    super.viewDidLoad()
    NSUserDefaults.standardUserDefaults().removeObjectForKey(UserNameKeyConstant)
    infoTable.registerClass(SaloonInfoCell.classForCoder(), forCellReuseIdentifier: "cell")
    net = Net(baseUrlString: "http://54.173.217.54:8080/")
    
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if let name = NSUserDefaults.standardUserDefaults().stringForKey(UserNameKeyConstant) {
      pinAddressString = name
      infoTable.reloadData()
    } else {
      pinAddressString = "Pin Your Address"
    }
    
    var arr_paramerters = NSDictionary()
    json.session("get_business_type", parameters: arr_paramerters) { (result) -> Void in
      
     if let resultArray = result["business_type"] as? NSArray {
      for i in 0..<resultArray.count {
       self.salonTypes.append(resultArray[i]["business_type_name"] as String)
        self.salonId.append((resultArray[i]["business_type_id"]! as Int))
      }
      }
      
    }
   
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: Table View
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var rows = 0
    if section == 0 {
      rows = 5
    } else if section == 1 {
      rows = 4
    }
    return rows
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    var title = ""
    if section == 1 {
      title = "Upload Salon Photos"
    }
    return title
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as SaloonInfoCell
    
    cell.midsubTitleLabel.hidden = true
    cell.salonPhotoOne.hidden = true
    cell.salonPhotoTwo.hidden = true
    cell.salonPhotoThree.hidden = true
    cell.salonPhotoFour.hidden = true
    cell.cellFrame = tableView.frame
    cell.addressButton.hidden = true
    cell.textInputField.hidden = true
    cell.countryCodeLabel.hidden = true
    cell.phoneNumberField.hidden = true
    cell.countryCodeButton.hidden = true
    cell.selectButton.hidden = true
    cell.selectLabel.hidden = true
    cell.subTitleLabel.hidden = true
    cell.descriptionTextArea.hidden = true
    cell.viewWithTag(600)?.removeFromSuperview()
    cell.phoneSeparator.hidden = true
    cell.countryCodePicker.hidden = true
    cell.textInputField.delegate = self
    cell.descriptionTextArea.delegate = self
    
    if indexPath.section == 0 {
      
      cell.titleLabel.text = sectionElements[indexPath.row]
      
      if sectionElementsTypesArray[indexPath.row] == 0 {
        cell.subTitleLabel.hidden = false
        cell.subTitleLabel.text = "Manager"
      } else if sectionElementsTypesArray[indexPath.row] == 1 {
        
        let image = UIImage(named: "location_icon_3x")
        cell.addressButton.setImage(image, forState: UIControlState.Normal)
        cell.addressButton.addTarget(self, action: "addressView", forControlEvents: UIControlEvents.TouchUpInside)
        cell.addressButton.hidden = false
        cell.subTitleLabel.hidden = false
        cell.subTitleLabel.text = "\(business_address)"
        cell.subTitleLabel.textColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
      } else if sectionElementsTypesArray[indexPath.row] == 2 {
        cell.selectButton.hidden = false
        cell.selectLabel.hidden = false
        cell.selectButton.addTarget(self, action: "saloonTypes", forControlEvents: UIControlEvents.TouchUpInside)
      } else if sectionElementsTypesArray[indexPath.row] == 3 {
        cell.phoneNumberField.hidden = false
        cell.phoneNumberField.delegate = self
        cell.phoneNumberField.text = "\(business_phone_number)"
        cell.phoneNumberField.tag = 501
        cell.countryCodeButton.hidden = false
        cell.countryCodeButton.addTarget(self, action: "phoneCode:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.countryCodeLabel.hidden = false
        cell.phoneSeparator.hidden = false
        cell.countryCodePicker.hidden = true
      } else if sectionElementsTypesArray[indexPath.row] == 4 {
        cell.textInputField.hidden = false
        cell.textInputField.placeholder = "Enter Salon Name"
        cell.textInputField.text = "\(buisness_name)"
        cell.textInputField.tag = 500
        
      }
    }
      
    else if indexPath.section == 1 {
      cell.titleLabel.text = sectionTwoElements[indexPath.row]
      
      if sectionTwoElementsTypesArray[indexPath.row] == 0 {
        cell.subTitleLabel.hidden = false
        cell.subTitleLabel.text = "Manager"
        
      } else if sectionTwoElementsTypesArray[indexPath.row] == 1 {
        let selectImage = UIImage(named: "rightside_arrow_icon_3x")
        cell.selectButton.setImage(selectImage, forState: UIControlState.Normal)
        cell.selectButton.hidden = false
        cell.selectButton.addTarget(self, action: "saloonViews:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.salonPhotoOne.hidden = false
        cell.salonPhotoTwo.hidden = false
        cell.salonPhotoThree.hidden = false
        cell.salonPhotoFour.hidden = false
        
      } else if sectionTwoElementsTypesArray[indexPath.row] == 2 {
        cell.selectButton.hidden = false
        cell.selectLabel.hidden = false
        
      } else if sectionTwoElementsTypesArray[indexPath.row] == 3 {
        cell.phoneNumberField.hidden = false
        cell.countryCodeButton.hidden = false
        cell.countryCodeLabel.hidden = false
        
      }  else if sectionTwoElementsTypesArray[indexPath.row] == 5 {
        cell.descriptionTextArea.hidden = false
        // MARK: Count Label
        let countLabelFrame = CGRect(x: cell.frame.maxX-70-26.66, y: 190 , width: 72, height: 72)
        countLabel = UILabel(frame: countLabelFrame)
        countLabel.textColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
        countLabel.font = UIFont(name: "raleway", size: 18)
        countLabel.text = "\(characterCount)/250"
        countLabel.tag = 600
        cell.contentView.addSubview(countLabel)
        countLabel.hidden = false
        cell.descriptionTextArea.tag = 200
        if business_description == "" {
          
        } else {
        cell.descriptionTextArea.text = business_description
        }
        }
    }
    
    return cell
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    var height = CGFloat(91)
    if indexPath.row == 2 && indexPath.section == 0 {
      height = CGFloat(93.8)
    }
    
    if indexPath.row == 3 && indexPath.section == 1 {
      height = CGFloat(250)
      
    }
    
    return height
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    self.view.endEditing(true)
    // if indexPath.row == 3 && indexPath.section == 1 {
    // infoTable.contentOffset = CGPoint(x: infoTable.frame.origin.x, y: infoTable.frame.origin.y+500)
    //}
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    self.view.endEditing(true)
    self.infoTable.endEditing(true)
    
  }
  
  //MARK: Changes when table scrolls
  func scrollViewDidScroll(scrollView: UIScrollView) {
    view.endEditing(true)
    
  }
  
  func adjustingView(click: Int) {
    if click == 1 {
      infoTable.contentOffset = CGPoint(x: infoTable.frame.origin.x, y: infoTable.frame.origin.y+500)
    } else if click == 2 {
      infoTable.contentOffset = CGPoint(x: infoTable.frame.origin.x, y: infoTable.frame.origin.y-500)
    }
    
    
  }
  
  func saloonViews(sender : UIButton) {
    var buttonOriginInTableView = sender.convertPoint(CGPointZero, toView: infoTable)   as CGPoint
    var indexPath = infoTable.indexPathForRowAtPoint(buttonOriginInTableView)!
    var cell = infoTable.cellForRowAtIndexPath(indexPath) as SaloonInfoCell
    
    let othstoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("frontView") as FrontViewViewController
    //println(cell.titleLabel!.text!)
    destinationView.titleString = cell.titleLabel!.text!
    self.navigationController!.pushViewController(destinationView, animated: true)
    
  }
  @IBAction func saveButtonPressed(sender: AnyObject) {
    
    let url = "add_business/"
    let img = UIImage(named: "min_location")
    var params = Dictionary<String, AnyObject>()
    params = ["business_name": buisness_name,
      "business_address": business_address,
      "business_country": business_country,
      "business_state": business_state,
      "business_city": business_city,
      "business_zip_code": business_zip_code,
      "business_phone_number_prefix": business_phone_number_prefix,
      "business_phone_number": "\(business_phone_number)",
      "business_longitude": business_longitude,
      "business_latitude": business_latitude,
      "business_description": business_description,
      "business_type_id": business_type_id,
      "front_view_images_count":"\(front_view_images_count)",
      "inside_view_images_count":"\(inside_view_images_count)",
       "side_view_images_count":"\(side_view_images_count)"]
      
     /* "front_view_image_1": NetData(pngImage: img!, filename: "min_location"),
      "inside_view_image_1": NetData(pngImage: img!, filename: "min_location"),
     
      "side_view_image_1": NetData(pngImage: img!, filename: "min_location")]*/
    
    /*let params = ["string": "test",
    "integerNumber": 1,
    "floatNumber": 1.5,
    "array": [10, 20, 30],
    "dictionary": ["x": 100.0, "y": 200.0],
    "icon": NetData(pngImage: img!, filename: "myIcon")] */
    if frontImageArray.count != 0 {
      for i in 0..<frontImageArray.count {
        params["front_view_image_\(i+1)"] = NetData(pngImage: frontImageArray[i], filename: "front_\(i+1)")
      }
    }
    
    if insideImageArray.count != 0 {
      for i in 0..<insideImageArray.count {
        params["inside_view_image_\(i+1)"] = NetData(pngImage: insideImageArray[i], filename: "inside_\(i+1)")
      }
    }
    
    if frontImageArray.count != 0 {
      for i in 0..<frontImageArray.count {
        params["side_view_image_\(i+1)"] = NetData(pngImage: sideImageArray[i], filename: "side_\(i+1)")
      }
    }
    
   if buisness_name.endIndex < "aaaaaa".endIndex {
      var alert = UIAlertView()
      alert.message = "Please Enter Atleast 6 Characters In Salon Name"
      alert.addButtonWithTitle("OK")
      alert.dismissWithClickedButtonIndex(0, animated: true)
      alert.show()
      
   } else if business_address == "Pin Your Address" {
    var alert = UIAlertView()
    alert.message = "Please Pin A Correct Address"
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
   }
   else if business_phone_number.endIndex == "1234567890".endIndex {
    var alert = UIAlertView()
    alert.message = "Please Enter A Valid Number"
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
   } else if business_type_id == "-1" {
    var alert = UIAlertView()
    alert.message = "Please Choose A Saloon Type"
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
   }
   else if insideImageArray.count == 0 || frontImageArray.count == 0 || sideImageArray.count == 0 {
      var alert = UIAlertView()
      alert.message = "Please add atleast one image in every field"
      alert.addButtonWithTitle("OK")
      alert.dismissWithClickedButtonIndex(0, animated: true)
      alert.show()
    } else if buisness_name.endIndex < "aaaaaa".endIndex {
      var alert = UIAlertView()
      alert.message = "Please Enter Atleast 6 characters in salon name"
      alert.addButtonWithTitle("OK")
      alert.dismissWithClickedButtonIndex(0, animated: true)
      alert.show()

    } else {
      net.POST(url, params: params, successHandler: {
        responseData in
        let result = responseData.json(error: nil)!
        NSLog("result: \(result)")
        
        var resultData = result["data"]! as NSDictionary
        var buisnessId = resultData["business_id"]! as Int
        BuisnessIdForRegistration = "\(buisnessId)"
        println(BuisnessIdForRegistration)
        dispatch_async(dispatch_get_main_queue()){
        let n: Int! = self.navigationController?.viewControllers?.count
        let prevViewController = self.navigationController?.viewControllers[n-3] as SignUpViewController
        prevViewController.salonSelected = self.buisness_name
        self.navigationController?.popViewControllerAnimated(true)
        }
        }, failureHandler: { error in
          var alert = UIAlertView()
          alert.message = "\(error)"
          alert.addButtonWithTitle("OK")
          alert.dismissWithClickedButtonIndex(0, animated: true)
          alert.show()
      })
    }
    
    
    
    
    
  }
  
  func addressView() {
    let othstoryboard = UIStoryboard(name: "Main", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("mapView") as SalonAddressViewController
    self.navigationController!.pushViewController(destinationView, animated: true)
  }
  
  @IBAction func backButtonPressed(sender : AnyObject) {
    self.navigationController!.popViewControllerAnimated(true)
    
  }
  // MARK: Text Field Implementation
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
    if textField.tag == 500 {
     buisness_name = textField.text
    } else if textField.tag == 501 {
      
      if range.location < 11 {
        business_phone_number = textField.text
      } else {
        textField.endEditing(true)
      }
    }
    return true
  }
  
  // MARK: Text view implementation
  
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    if range.location <= 250 {
      characterCount = range.location + 1
      countLabel.text = "\(characterCount)/250"
     
      business_description = textView.text
      }
    else {
      textView.endEditing(true)
    }
    return true
  }
  
  func textViewDidBeginEditing(textView: UITextView) {
    if textView.text == "Enter Description Here" {
      textView.text = ""
      textView.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
      
    }
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    if textView.text == "" {
      textView.text = "Enter Description Here"
      textView.textColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
    }
  }
  //MARK: Picker View Implementation for selecting stylist
  
  func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // number of rows in picker
    return pickerData.count
  }
  
  func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 60
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var pickerLabel = view as UILabel!
    if view == nil {
      pickerLabel = UILabel()
      pickerLabel.font = UIFont(name: "Raleway", size: 22.0)
      pickerLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
      pickerLabel.textAlignment = .Center
    }
    
    pickerLabel.text = "\(pickerData[row])"
    
    return pickerLabel
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if selectedRow != 0 {
      business_type_id = "\(pickerReturnData[row])"
      picker.hidden = true
      
      var indexPath = NSIndexPath(forRow: 3, inSection: 0)
      var cell = infoTable.cellForRowAtIndexPath(indexPath) as SaloonInfoCell
      cell.countryCodeLabel.text = pickerData[row] as? String
      cell.countryCodeLabel.textColor = PinkColor
      business_phone_number_prefix = "\(pickerReturnData[row])"
      
      
      
    } else {
      
      business_type_id = "\(pickerReturnData[row])"
      picker.hidden = true
      
      var indexPath = NSIndexPath(forRow: 4, inSection: 0)
      var cell = infoTable.cellForRowAtIndexPath(indexPath) as SaloonInfoCell
      cell.selectLabel.text = pickerData[row] as? String
    }
  }
  
  func saloonTypes()  {
    
    
    pickerData = salonTypes
    pickerReturnData = salonId
    pickerOutlet.reloadAllComponents()
    picker.hidden = false
    selectedRow = 0
  }
  
  func phoneCode(sender:UIButton) {
    
    var buttonOriginInTableView = sender.convertPoint(CGPointZero, toView: infoTable)   as CGPoint
    var indexPath = infoTable.indexPathForRowAtPoint(buttonOriginInTableView)!
    selectedRow = indexPath.row
    
    pickerData = ["+91", "+11", "+01", "+44"]
    pickerReturnData = ["+91", "+11", "+01", "+44"]
    pickerOutlet.reloadAllComponents()
    picker.hidden = false
  }
  
}
