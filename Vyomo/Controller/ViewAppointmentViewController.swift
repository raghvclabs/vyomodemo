//
//  ViewAppointmentViewController.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/18/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit
import QuartzCore

class ViewAppointmentViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UIPickerViewDelegate , UIPickerViewDataSource {
  
  @IBOutlet weak var table: UITableView!
  @IBOutlet weak var addAppointmentButton: UIButton!
  @IBOutlet weak var AddedAppointmentView: UIView!
  @IBOutlet weak var todayLabel: UILabel!
  @IBOutlet weak var noAppointmentView: UIView!
  @IBOutlet weak var stylistPickerView: UIView!
  @IBOutlet weak var stylistPicker: UIPickerView!
  @IBOutlet weak var selectStylistLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var blurView: UIView!
  @IBOutlet weak var todayDate: UILabel!
  @IBOutlet weak var selectStylistView: UIView!
  var managerCheck = 0
  let roleId = NSUserDefaults.standardUserDefaults().valueForKey(Role_id) as? Int
  let accessToken = NSUserDefaults.standardUserDefaults().valueForKey(Access_token) as? String
  let businessId = NSUserDefaults.standardUserDefaults().integerForKey(Business_id)
  let stylistIdForDefault = NSUserDefaults.standardUserDefaults().integerForKey(StylistId)
  
  var sema: dispatch_semaphore_t = dispatch_semaphore_create(0)
  var defaultStylistId = -1
  var highestNumberOfServices = 0
  var numberOfRows = 0
  var height = CGFloat(0)
  var totalServices = [CGFloat]()
  var index = NSIndexPath()
  var arr = [Int]()
  var serviceArray = [[String]]()
  var costArray = [[Int]]()
  var containsAppointment = true
  var customerNames = [String]()
  var appointmentDuration = [String]()
  var totalCostArray = [Int]()
  var stylistNames = [String]()
  var stylistId = [Int]()
  var todaysDate = NSDate()
  var dayName = String()
  var date = String()
  var year = String()
  var month = String()
  var imageCache = [String : UIImage]()
  var imageUrls = [String]()

  @IBAction func selectStylistButtonTapped(sender: AnyObject) {
    stylistPickerView.hidden = false
  }
  
  @IBAction func appointmentButtonTapped(sender: AnyObject) {
    let othstoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("calendar") as CalendarViewController
   self.navigationController!.pushViewController(destinationView, animated: true)
    destinationView.stylistId = stylistId
    destinationView.stylistNames = stylistNames
    destinationView.defaultStylistId = defaultStylistId
  }
  // @IBOutlet weak var checkoutDirectlyButton: UIButton!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    table.separatorColor = DarkGrayColor
    table.bounces = false
    
    var dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd"
    var extractedDate = dateFormatter.stringFromDate(todaysDate)
    date = extractedDate
    dateFormatter.dateFormat = "MMMM"
    var extractedMonthName = dateFormatter.stringFromDate(todaysDate) as NSString
    dateFormatter.dateFormat = "YYYY"
    var extractedYear = dateFormatter.stringFromDate(todaysDate)
    year = extractedYear
    dateFormatter.dateFormat = "EEEE"
    var extractedday = dateFormatter.stringFromDate(todaysDate)
   var truncatedMonthName = extractedMonthName.substringWithRange(NSMakeRange(0, 3))
    
    todayDate.text = "\(extractedday) \(date) \(truncatedMonthName), \(year)"
    
    
    
    table.registerClass(AppointmentCell.classForCoder(), forCellReuseIdentifier: "cell")
    activityIndicator.startAnimating()
    
    var rightBorder = UIView()
    rightBorder.backgroundColor = UIColor(red: 146/255, green: 155/255, blue: 162/255, alpha: 1)
    rightBorder.frame = CGRectMake(UIScreen.mainScreen().bounds.maxX/2,AddedAppointmentView.bounds.maxY - addAppointmentButton.frame.height + 1, 1, CGRectGetHeight(addAppointmentButton.bounds))
    
    AddedAppointmentView.addSubview(rightBorder)
    AddedAppointmentView.bringSubviewToFront(rightBorder)

    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(businessId)","business_id",
      "\(stylistId)","stylist_id",
      getFlagForStylistId(),"flag")
    
    println(arr_parameters)
    
    json.session("get_default_stylist_appointment_today", parameters: arr_parameters, completion: {
      result in
      let jsonResult = result as NSDictionary
      let status = jsonResult["status"]! as Int
      let message = jsonResult["message"]! as NSString
      let parsedResult = jsonResult["data"]! as NSDictionary
      var emptyDictionary = NSDictionary()
      
      println("GetdefaultStylistAppointment_today: \(parsedResult)")

      dispatch_async(dispatch_get_main_queue()){
        
        if status == 200 {
          
          if parsedResult ==  emptyDictionary {
            self.AddedAppointmentView.hidden = true
            self.noAppointmentView.hidden = false
            println("imhere")
            
          } else {
            
            println("here")
            var data = parsedResult["AppointmentDetail"]! as NSArray
            self.defaultStylistId = data[0]["stylist_id"]! as Int
            self.numberOfRows = data.count
            var counter = loopIntial
            
            for names in data {
              var startTime : NSString = data[counter]["appointment_start_time"]! as NSString
              startTime = startTime.componentsSeparatedByString("T")[1] as NSString
              startTime = startTime.componentsSeparatedByString(":00.000Z")[0] as NSString
              
              var endTime : NSString = data[counter]["appointment_end_time"]! as NSString
              endTime = endTime.componentsSeparatedByString("T")[1] as NSString
              endTime = endTime.componentsSeparatedByString(":00.000Z")[0] as NSString
              
              if data[counter]["customer_name"]! as NSString == "" {
                self.customerNames.append("No Name")
                self.appointmentDuration.append("\(startTime) - \(endTime)")
                
              } else {
                
                self.customerNames.append(data[counter]["customer_name"]! as NSString)
                self.appointmentDuration.append("\(startTime)-\(endTime)")
              }
              
              var customerServices : NSArray = data[counter]["Services"] as NSArray
              println(customerServices)
              self.serviceArray.append([])
              self.costArray.append([])
              var serviceCounter = loopIntial
              var totalCost = loopIntial
              
              for services in customerServices {
                
                self.serviceArray[counter].append(customerServices[serviceCounter]["service_name"] as String)
                println(customerServices[serviceCounter]["business_service_id"])
                self.costArray[counter].append(customerServices[serviceCounter]["service_cost"] as Int)
                totalCost += customerServices[serviceCounter]["service_cost"]! as Int
                serviceCounter++
              }
              
              self.totalServices.append(CGFloat(serviceCounter))
              if self.highestNumberOfServices < serviceCounter {
                self.highestNumberOfServices = serviceCounter
              }
              
              self.arr.append(0)
              self.totalCostArray.append(totalCost)
              var imageLink = data[counter]["customer_image"] as String
              self.imageUrls.append(imageLink)
              
              counter++
            }
            self.table.reloadData()
          }
          self.defaultStylistName()
        }
      }
      
    })
  }
  // populating stylist list
  
  override func viewWillAppear(animated: Bool) {
    
    
    
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  //MARK: Table Cell Implementation
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return numberOfRows
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as AppointmentCell
    
    var count = 0
    for i in 0...highestNumberOfServices {
      cell.viewWithTag(100+count)?.removeFromSuperview()
      cell.viewWithTag(150+count)?.removeFromSuperview()
      count++
    }
    cell.viewWithTag(300)?.removeFromSuperview()
    cell.viewWithTag(301)?.removeFromSuperview()
    cell.viewWithTag(302)?.removeFromSuperview()
    cell.viewWithTag(303)?.removeFromSuperview()
    
    
    if arr[indexPath.row] == 1 {
      cell.customerNameLabel.text = customerNames[indexPath.row]
      cell.subTitleLabel.text = appointmentDuration[indexPath.row]
      
      cell.totalLabel.hidden = true
      cell.costLabel.hidden = true
      expandView(cell,row: indexPath.row)
      
    } else if arr[indexPath.row] == 0 {
      cell.customerNameLabel.text = customerNames[indexPath.row]
      cell.subTitleLabel.text = appointmentDuration[indexPath.row]
      cell.costLabel.text = "$\(totalCostArray[indexPath.row])"
      cell.totalLabel.hidden = false
      cell.costLabel.hidden = false
      
    }
    
    cell.selectButton.addTarget(self, action: "increaseHeight:", forControlEvents: UIControlEvents.TouchUpInside)
    
    //images
    
    
    let urlString = "\(imageUrls[indexPath.row])"
    
    var image = self.imageCache[urlString]
   
    if( image == nil ) {
      // If the image does not exist, we need to download it
      var imgURL: NSURL = NSURL(string: urlString)!
      
      // Download an NSData representation of the image at the URL
      let request: NSURLRequest = NSURLRequest(URL: imgURL)
      NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
        if error == nil {
          image = UIImage(data: data)
          
          // Store the image in to our cache
          self.imageCache[urlString] = image
          dispatch_async(dispatch_get_main_queue(), {
            if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? AppointmentCell {
              cellToUpdate.saloonImage.image = image
            }
          })
        }
        else {
          println("Error: \(error.localizedDescription)")
        }
      })
      
    }
    else {
      dispatch_async(dispatch_get_main_queue(), {
        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? AppointmentCell {
          cellToUpdate.saloonImage.image = image
        }
      })
    }
    
    //getting images
    
    return cell
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    var constantHeight = 110 as CGFloat
    if arr[indexPath.row] == 1 {
      if totalServices[indexPath.row] > 3 {
        constantHeight = constantHeight * totalServices[indexPath.row] * 0.6
      } else {
        constantHeight = constantHeight * 2
      }
      
      
      height = constantHeight
    }
    
    return constantHeight
  }
  
  //MARK: Cell Height Function
  
  func increaseHeight(sender : AnyObject) {
    
    var buttonOriginInTableView = sender.convertPoint(CGPointZero, toView: table)   as CGPoint
    var indexPath = table.indexPathForRowAtPoint(buttonOriginInTableView)!
    var cell = table.cellForRowAtIndexPath(indexPath)
    if  arr[indexPath.row] == 1 {
      arr[indexPath.row] = 0
    } else if arr[indexPath.row] == 0 {
      arr[indexPath.row] = 1
      
    }
    table.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
  }
  
  //MARK: Expanded cell view
  
  func expandView(cell: AppointmentCell, row: Int) {
    var serviceLabel = [UILabel]()
    var costLabel = [UILabel]()
    var i = 0
    println(serviceArray)
    println(costArray)
    //Adding Labels and cost when view is expanded
    if arr[row] == 1 {
      for i = 0 ; i < serviceArray[row].count ; i++ {
        var tempserviceLabel = UILabel(frame: CGRectMake(73.66, cell.totalLabel.frame.origin.y + CGFloat(i*20) , 200, 20))
        tempserviceLabel.text = "\(serviceArray[row][i])"
        tempserviceLabel.textAlignment = NSTextAlignment.Natural
        tempserviceLabel.font = (UIFont(name: "raleway", size: 17.5))
        tempserviceLabel.textColor = UIColor(red: 38/255, green: 38/255, blue: 38/255, alpha: 1)
        serviceLabel.append(tempserviceLabel)
        serviceLabel[i].tag = 100 + i
        cell.contentView.addSubview(serviceLabel[i])
        
        var tempcostLabel = UILabel(frame: CGRectMake(UIScreen.mainScreen().bounds.maxX-62, cell.totalLabel.frame.origin.y + CGFloat(i*20) ,80,20))
        tempcostLabel.text = "$\(costArray[row][i])"
        tempcostLabel.textAlignment = NSTextAlignment.Natural
        tempcostLabel.font = (UIFont(name: "raleway", size: 17.5))
        tempcostLabel.textColor = UIColor(red: 38/255, green: 38/255, blue: 38/255, alpha: 1)
        costLabel.append(tempcostLabel)
        costLabel[i].tag = 150 + i
        cell.contentView.addSubview(costLabel[i])
      }
      
      
      var totalLabel = UILabel(frame: CGRectMake(73.66, cell.totalLabel.frame.origin.y + CGFloat(i*22) , 200, 20))
      totalLabel.text = "TOTAL"
      totalLabel.textAlignment = NSTextAlignment.Natural
      totalLabel.font = (UIFont(name: "raleway-bold", size: 17.5))
      totalLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
      totalLabel.tag = 300
      
      var line = UIView(frame: CGRectMake(73.66, cell.totalLabel.frame.origin.y + CGFloat(i*21) , cell.frame.width-105, 0.33))
      line.backgroundColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
      line.tag = 302
      cell.contentView.addSubview(line)
      
      var totalcostLabel = UILabel(frame: CGRectMake(UIScreen.mainScreen().bounds.maxX-62, cell.totalLabel.frame.origin.y + CGFloat(i*22) ,80,20))
      totalcostLabel.text = "$\(totalCostArray[row])"
      totalcostLabel.textAlignment = NSTextAlignment.Natural
      totalcostLabel.font = (UIFont(name: "raleway-bold", size: 17.5))
      totalcostLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
      totalcostLabel.tag = 301
      
      cell.contentView.addSubview(totalLabel)
      cell.contentView.addSubview(totalcostLabel)
      
      //Implementation of view and its subparts
      
      var checkoutTabView = UIView(frame: CGRectMake(0, height - 52 , cell.frame.width ,52))
      checkoutTabView.backgroundColor = UIColor(red: 59/255, green: 72/255, blue: 83/255, alpha: 1)
      checkoutTabView.tag = 303
      
      var editbutton = UIButton(frame: CGRectMake(cell.frame.maxX-305, 10 , 80 ,30))
      let editImage = UIImage(named: "edit_small_icon_03x")
      editbutton.setImage(editImage, forState: UIControlState.Normal)
      editbutton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 50)
      
      var editLabel = UILabel(frame: CGRectMake(cell.frame.maxX-265,  16 , 50 ,18))
      editLabel.text = "EDITS"
      editLabel.font = UIFont(name: "raleway-bold", size: 14.5)
      editLabel.textColor = UIColor.whiteColor()
      
      var checkoutbutton = UIButton(frame: CGRectMake(cell.frame.maxX-198, 10  , 120,30))
      let checkoutImage = UIImage(named: "small_checkout_icon_3x")
      checkoutbutton.setImage(checkoutImage, forState: UIControlState.Normal)
      checkoutbutton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 90)
      
      
      var checkoutLabel = UILabel(frame: CGRectMake(cell.frame.maxX-162, 16  ,100,18))
      checkoutLabel.text = "CHECKOUT"
      checkoutLabel.font = UIFont(name: "raleway-bold", size: 14.5)
      checkoutLabel.textColor = UIColor.whiteColor()
      
      var moreButton = UIButton(frame: CGRectMake(cell.frame.maxX-56, 10  ,25 ,30))
      moreButton.contentMode = UIViewContentMode.Left
      
      let selectImage = UIImage(named: "more-icon")
      moreButton.setImage(selectImage, forState: UIControlState.Normal)
      
      checkoutTabView.addSubview(editbutton)
      checkoutTabView.addSubview(editLabel)
      checkoutTabView.addSubview(checkoutLabel)
      checkoutTabView.addSubview(checkoutbutton)
      checkoutTabView.addSubview(moreButton)
      cell.contentView.addSubview(checkoutTabView)
      
    }
    
  }
  //MARK: Picker View Implementation for selecting stylist
  
  func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // number of rows in picker
    return stylistNames.count
  }
  
  func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 60
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var pickerLabel = view as UILabel!
    if view == nil {
      pickerLabel = UILabel()
      pickerLabel.font = UIFont(name: "Raleway", size: 22.0)
      pickerLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
      pickerLabel.textAlignment = .Center
    }
    
    pickerLabel.text = "\(stylistNames[row])"
    
    return pickerLabel
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
   self.blurView.hidden = false
    numberOfRows = 0
    height = CGFloat(0)
    totalServices = []
    arr = []
    serviceArray = []
    costArray = []
    containsAppointment = true
    customerNames = []
    appointmentDuration = []
    totalCostArray = []
    defaultStylistId = stylistId[row]
    imageUrls = []
    imageCache = [String : UIImage]()
    let cell = table.dequeueReusableCellWithIdentifier("cell") as AppointmentCell
    
    var count = 0
    for i in 0...highestNumberOfServices {
      cell.viewWithTag(100+count)?.removeFromSuperview()
      cell.viewWithTag(150+count)?.removeFromSuperview()
      count++
    }
    
    cell.viewWithTag(300)?.removeFromSuperview()
    cell.viewWithTag(301)?.removeFromSuperview()
    cell.viewWithTag(302)?.removeFromSuperview()
    cell.viewWithTag(303)?.removeFromSuperview()
    
  //intialization steps to be performed everytime the row is selected
    
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(businessId)","business_id",
      "\(stylistId[row])","stylist_id",
      "1","flag")
    
    json.session("get_default_stylist_appointment_today", parameters: arr_parameters, completion: {
      result in
      var emptyDictionary = NSDictionary()
      let parsedResult = result as NSDictionary
      println("when he is selected\(parsedResult)")
      
      dispatch_async(dispatch_get_main_queue()){
        if parsedResult ==  emptyDictionary {
          //  self.AddedAppointmentView.removeFromSuperview()
          // self.noAppointmentView.hidden = false
          
          dispatch_async(dispatch_get_main_queue()){
            
            var finder = find(self.stylistId,self.defaultStylistId)
            self.selectStylistLabel.textColor = PinkColor
            self.selectStylistLabel.text = "\(self.stylistNames[finder!])"
            
          }
          
          self.numberOfRows = 0
          self.table.reloadData()
        } else {
          
          var data = parsedResult["AppointmentDetail"]! as NSArray
          self.numberOfRows = data.count
          var counter = loopIntial
          for names in data {
            
            var startTime : NSString = data[counter]["appointment_start_time"]! as NSString
            startTime = startTime.componentsSeparatedByString("T")[1] as NSString
            startTime = startTime.componentsSeparatedByString(":00.000Z")[0] as NSString
            var endTime : NSString = data[counter]["appointment_end_time"]! as NSString
            endTime = endTime.componentsSeparatedByString("T")[1] as NSString
            endTime = endTime.componentsSeparatedByString(":00.000Z")[0] as NSString
            
            if data[counter]["customer_name"]! as NSString == "" {
              self.customerNames.append("No Name")
              self.appointmentDuration.append("\(startTime) - \(endTime)")
              
            } else {
              self.customerNames.append(data[counter]["customer_name"]! as NSString)
              self.appointmentDuration.append("\(startTime)-\(endTime)")
            }
            
            var customerServices : NSArray = data[counter]["Services"] as NSArray
            println(customerServices)
            self.serviceArray.append([])
            self.costArray.append([])
            var serviceCounter = loopIntial
            var totalCost = loopIntial
            for services in customerServices {
              
              self.serviceArray[counter].append(customerServices[serviceCounter]["service_name"] as String)
              println(customerServices[serviceCounter]["business_service_id"])
              self.costArray[counter].append(customerServices[serviceCounter]["service_cost"] as Int)
              totalCost += customerServices[serviceCounter]["service_cost"]! as Int
              serviceCounter++
            }
            
            self.totalServices.append(CGFloat(serviceCounter))
            if self.highestNumberOfServices < serviceCounter {
              self.highestNumberOfServices = serviceCounter
            }
            
            self.arr.append(0)
            self.totalCostArray.append(totalCost)
            var imageLink = data[counter]["customer_image"] as String
            self.imageUrls.append(imageLink)
            counter++
          }
          
          self.table.reloadData()
          
          dispatch_async(dispatch_get_main_queue()){
            
            var finder = find(self.stylistId,self.defaultStylistId)
            self.selectStylistLabel.textColor = PinkColor
            self.selectStylistLabel.text = "\(self.stylistNames[finder!])"
            
          }
        }
        
      }
    })
    
    stylistPickerView.hidden = true
    self.blurView.hidden = true
  }
  
  
  //MARK: Dynamic updation of Stylist Name 
  
  func defaultStylistName() {
    
    if RoleId == 2 {
      
      var arr_parameters = NSDictionary(objectsAndKeys:"\(accessToken)","access_token",
        "\(businessId)","business_id")
      
      json.session("get_stylist_list", parameters: arr_parameters, completion: {
        result in
        let status  = result["status"]! as Int
        let message = result["message"]! as NSString
        
        if let parsedResult = result["data"] as? [NSDictionary] {
          println(parsedResult)
          
          if status == 200 {
        
            self.showAlert(message)
            var resultCounter = 0
            
            for num in parsedResult {
              let stylistNamesData =  parsedResult[resultCounter]["user_name"] as? String
              let stylistIdData = parsedResult[resultCounter]["service_provider_id"] as? Int
              
              self.stylistNames.append(stylistNamesData!)
              self.stylistId.append(stylistIdData!)
              
              resultCounter++
            }
            
            dispatch_async(dispatch_get_main_queue()){
              self.stylistPicker.reloadAllComponents()
              
              if self.defaultStylistId == -1 || self.defaultStylistId == 0 {
                // Code to be done
                
              }else{
                var finder = find(self.stylistId,self.defaultStylistId)
                self.selectStylistLabel.textColor = PinkColor
                self.selectStylistLabel.text = "\(self.stylistNames[finder!])"
                
              }
            }
          }
        }
        
      })
    }
    self.blurView.hidden = true
  }

  @IBAction func moreButtonPressed(sender: AnyObject) {
    let alertController = UIAlertController(title: "Error", message: "Are you sure you want to logout?", preferredStyle: .Alert)
    let OkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) in
      
      logout(self)
      
    }
    let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (action) in
      return
    }
    alertController.addAction(OkAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true) {
    }
    
  }
  
 /* func refresh() {
    
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(businessId)","business_id",
      "\(stylistId)","stylist_id",
      getFlagForStylistId(),"flag")
    
    println("get_default_stylist_appointment_today : \(arr_parameters)")
    
    json.session("Getdefaultstylist_appointment_today", parameters: arr_parameters, completion: {
      result in
      
      var emptyDictionary = NSDictionary()
      
      let status = jsonResult["status"]! as Int
      let message = jsonResult["message"]! as NSString
      let parsedResult = jsonResult["data"]! as NSDictionary
      //println(parsedResult)
      
      dispatch_async(dispatch_get_main_queue()){
        
        if status == 200 {
          
          self.showAlert(message)
          
          if parsedResult ==  emptyDictionary {
            self.AddedAppointmentView.hidden = true
            self.noAppointmentView.hidden = false
            println("imhere")
            
          } else {
            
            println("here")
            var data = parsedResult["AppointmentDetail"]! as NSArray
            self.defaultStylistId = data[0]["stylist_id"]! as Int
            self.numberOfRows = data.count
            var counter = loopIntial
            
            for names in data {
              var startTime : NSString = data[counter]["appointment_start_time"]! as NSString
              startTime = startTime.componentsSeparatedByString("T")[1] as NSString
              startTime = startTime.componentsSeparatedByString(":00.000Z")[0] as NSString
              var endTime : NSString = data[counter]["appointment_end_time"]! as NSString
              endTime = endTime.componentsSeparatedByString("T")[1] as NSString
              endTime = endTime.componentsSeparatedByString(":00.000Z")[0] as NSString
              
              if data[counter]["customer_name"]! as NSString == "" {
                self.customerNames.append("No Name")
                self.appointmentDuration.append("\(startTime) - \(endTime)")
                
              } else {
                
                self.customerNames.append(data[counter]["customer_name"]! as NSString)
                self.appointmentDuration.append("\(startTime)-\(endTime)")
              }
              
              var customerServices : NSArray = data[counter]["Services"] as NSArray
              println(customerServices)
              self.serviceArray.append([])
              self.costArray.append([])
              var serviceCounter = loopIntial
              var totalCost = loopIntial
              
              for services in customerServices {
                
                self.serviceArray[counter].append(customerServices[serviceCounter]["service_name"] as String)
                println(customerServices[serviceCounter]["business_service_id"])
                self.costArray[counter].append(customerServices[serviceCounter]["service_cost"] as Int)
                totalCost += customerServices[serviceCounter]["service_cost"]! as Int
                serviceCounter++
              }
              
              self.totalServices.append(CGFloat(serviceCounter))
              if self.highestNumberOfServices < serviceCounter {
                self.highestNumberOfServices = serviceCounter
              }
              
              self.arr.append(0)
              self.totalCostArray.append(totalCost)
              var imageLink = data[counter]["customer_image"] as String
              self.imageUrls.append(imageLink)
              
              counter++
            }
            self.table.reloadData()
          }
          self.defaultStylistName()
        }
      }
    })
  }
*/
  
//MARK: Get Flag
  
  func getFlagForStylistId() -> Int {
    var flag = 0
    var stylist = 0
    if roleId == 1  {
      stylist = stylistIdForDefault
      flag = 1
    }
    return flag
  }
  
//MARK: Show Alert
  
  func showAlert(string : NSString) {
    var alert = UIAlertView()
    alert.message = string
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
  }
  
}





