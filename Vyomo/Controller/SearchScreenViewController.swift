//
//  SearchScreenViewController.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/12/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SearchScreenViewController: UIViewController , UITextFieldDelegate ,UITableViewDataSource , UITableViewDelegate {
  
  @IBOutlet weak var searchTable: UITableView!
  @IBOutlet weak var searchField: UITextField!
  var searchActive : Bool = false
  var data = [String]()
  var buisenessId = [Int]()
  var filtered:[String] = []
  var searchString = [String]()
  var oldData = [String]()
  var searchCounter = 0
  var userSelection = String()
  var imageCache = [String : UIImage]()
  var imageUrls = [String]()
  var imageArray = [UIImage]()
  @IBOutlet weak var addOwnButton: UIButton!
  @IBOutlet weak var blurView: UIView!
  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var searchBarView: UIView!
  
  @IBAction func backButtonPressed(sender : AnyObject) {
/*
    let othstoryboard = UIStoryboard(name: "Main", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("signUp") as SignUpViewController
    let secondViewAnimation = Transition()
    secondViewAnimation.transitionBackward(self, destViewController: destinationView)*/
     self.navigationController!.popViewControllerAnimated(true)
    
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    
    activityIndicator.startAnimating()
    
    if RoleId == 1 {
     addOwnButton.removeFromSuperview()
      searchTable.removeFromSuperview()
     
      if self.view.frame.size == CGSize(width: 375, height: 667) && self.view.frame.size == CGSize(width: 414, height: 736) {
      
      let frame = CGRectMake(0, self.searchBarView.frame.maxY - 8  , self.view.bounds.width , self.view.bounds.height - (self.searchBarView.bounds.maxY + 98))
     let table = UITableView(frame: frame)
      table.delegate = self
      table.dataSource = self
      self.view.addSubview(table)
      
      searchTable = table
      table.separatorInset.left = 26.66
      table.separatorInset.right = 26.66
    
      } else {
        let frame = CGRectMake(0, self.searchBarView.frame.maxY - 16  , self.view.bounds.width , self.view.bounds.height - (self.searchBarView.bounds.maxY + 90))
        let table = UITableView(frame: frame)
        table.delegate = self
        table.dataSource = self
        self.view.addSubview(table)
   
        searchTable = table
        table.separatorInset.left = 26.66
        table.separatorInset.right = 26.66
        table.rowHeight = 76
        
      }
      self.view.bringSubviewToFront(blurView)
    }
    
    let color = UIColor.whiteColor()
    var str = NSAttributedString(string: "Enter Text Here", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
    self.searchField.attributedPlaceholder = str;
    searchTable.registerClass(TableCell.classForCoder(), forCellReuseIdentifier: "cell")
   
   var urlString = ""
    if RoleId == 1 {
      urlString = "get_all_business_for_stylist"
    } else {
      urlString = "get_all_business_for_manager"
    }
    
    var arr_parameters = NSDictionary()
      json.session(urlString, parameters: arr_parameters, completion: {
      result in
      
        let parsedResult = result as NSDictionary
        
      let resultArray = parsedResult["business_list"] as NSArray
      println(resultArray)
      var resultCounter = 0
      
        for num in resultArray {
        let buisenessData = resultArray[resultCounter]["business_name"] as String
        let buisenessIdData = resultArray[resultCounter]["business_id"]!! as Int
          let buisness_image = resultArray[resultCounter]["profile_image_path"] as String
          self.imageUrls.append(buisness_image)
        self.data.append(buisenessData)
        self.buisenessId.append(buisenessIdData)
        resultCounter++
      }
      self.oldData = self.data
      dispatch_async(dispatch_get_main_queue()){
        self.searchTable.reloadData()
        self.blurView.hidden = true
      }
       
    })
      
    
   
  
  }
  
  @IBAction func addNewButton(sender : AnyObject) {
  /*  let othstoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("addSaloonInfo") as AddSaloonInfoViewController
    let secondViewAnimation = Transition()
    secondViewAnimation.transitionForward(self, destViewController: destinationView)*/
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
    var viewController = mainStoryboard.instantiateViewControllerWithIdentifier("addSaloonInfo") as AddSaloonInfoViewController
    self.navigationController!.pushViewController(viewController, animated: true)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
  
  // MARK : TextField Action
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
    if string != "" {                 //if element is typed not deleted
      searchString.append(string)
      var modifiedString = String()
      
      for i in searchString {
        modifiedString += i
      }
      searchCounter++
      println(modifiedString)
      println(searchString)
      search(modifiedString)
    } else {
      searchString.removeLast()
      var modifiedString = String()
      
      for i in searchString {
        modifiedString += i
      }
      searchCounter--
      println(searchString)
      println(modifiedString)
      search(modifiedString)
      
    }
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    self.searchField.endEditing(true)
    
  }
  
  // MARK: Search Implementation
  
  func search(string: String) {
    filtered = oldData.filter({ (text) -> Bool in
      let tmp: NSString = text
      let range = tmp.rangeOfString("\(string)", options: NSStringCompareOptions.CaseInsensitiveSearch)
      return range.location != NSNotFound
    })
    
    if(filtered == []) && searchCounter == 0 {
      data = oldData
    } else {
      data = filtered
    }
    
    sort(&filtered)
    println(filtered)
    searchTable.reloadData()
    
  }
  
  // MARK: Table Data
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as TableCell
    cell.saloonNameLabel.text = "\(data[indexPath.row])"
    
    let urlString = "\(imageUrls[indexPath.row])"
    
    var image = self.imageCache[urlString]
    
    
    if( image == nil ) {
      // If the image does not exist, we need to download it
      var imgURL: NSURL = NSURL(string: urlString)!
      
      // Download an NSData representation of the image at the URL
      let request: NSURLRequest = NSURLRequest(URL: imgURL)
      NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
        if error == nil {
          image = UIImage(data: data)
          
          // Store the image in to our cache
          self.imageCache[urlString] = image
          
        
          dispatch_async(dispatch_get_main_queue(), {
            if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableCell {
              cellToUpdate.saloonImage.image = image
            }
          })
        }
        else {
          println("Error: \(error.localizedDescription)")
        }
      })
      
    }
   /* else {
      dispatch_async(dispatch_get_main_queue(), {
        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableCell {
          cellToUpdate.saloonImage.image = image
        }
      })
    }*/
    
    //getting images

    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    userSelection = "\(data[indexPath.row])"
    println(userSelection)
    var serviceNumber = 0
    var selectedBuisnessUserId = 0
    while ( serviceNumber < oldData.count ) {
      if userSelection == oldData[serviceNumber] {
        selectedBuisnessUserId = serviceNumber
        break
      } else {
        serviceNumber++
      }
    }
    
    BuisnessIdForRegistration = "\(buisenessId[selectedBuisnessUserId])"
    println(BuisnessIdForRegistration)
    let n: Int! = self.navigationController?.viewControllers?.count
    let prevViewController = self.navigationController?.viewControllers[n-2] as SignUpViewController
    prevViewController.salonSelected = userSelection
    self.navigationController!.popViewControllerAnimated(true)
    
  }
  
  
}

