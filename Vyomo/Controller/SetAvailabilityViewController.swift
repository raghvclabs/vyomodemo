//
//  SetAvailabilityViewController.swift
//  Vyomo
//
//  Created by Click Labs on 3/17/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SetAvailabilityViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
  
  var timeIntervalString = [String]()
  @IBOutlet weak var weekCollectionView: UICollectionView!
  @IBOutlet weak var timeCollectionView: UICollectionView!
  
  
  var panView = UIView()
  var upperPan = UIView()
  var lowerPan = UIView()
  var cellHeight = CGFloat()
  var cellWidth = CGFloat()
  var availabilitySchedule = Dictionary<String,[Int]>()
  let tickerHeight : CGFloat = 15
  var maxPanlimit : CGFloat = 0
  
  var cellArray = [UICollectionViewCell]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    timeIntervalForAppointments()
    availabilitySchedule["SUN"] = []
    availabilitySchedule["MON"] = []
    availabilitySchedule["TUE"] = []
    availabilitySchedule["WED"] = []
    availabilitySchedule["FRI"] = []
    availabilitySchedule["SAT"] = []
    availabilitySchedule["THU"] = []
    loadAvailability()
  }
  
  func loadAvailability() {
    for i in 0...95 {
      availabilitySchedule["SUN"]?.append(0)
      availabilitySchedule["MON"]?.append(0)
      availabilitySchedule["TUE"]?.append(0)
      availabilitySchedule["WED"]?.append(0)
      availabilitySchedule["FRI"]?.append(0)
      availabilitySchedule["SAT"]?.append(0)
      availabilitySchedule["THU"]?.append(0)
    }
    println(availabilitySchedule["SUN"]?.count)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView.tag == 7 {
      return 96 * 7
    } else if collectionView.tag == 8 {
      return 96
    } else {
      return 0
    }
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("week", forIndexPath: indexPath) as UICollectionViewCell
    // println(cell.frame.width)
    
    if collectionView.tag == 8 {
      let timeIntervalLabel : UILabel = cell.viewWithTag(102) as UILabel
      timeIntervalLabel.text = timeIntervalString[indexPath.row]
      
      //   cell.textLabel?.text = timeIntervalString[indexPath.row]
      if indexPath.row % 4 == 0 {
        timeIntervalLabel.font = UIFont(name: "Raleway-Bold", size: 18.0)
        timeIntervalLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
      } else {
        timeIntervalLabel.font = UIFont(name: "Raleway", size: 18.0)
        timeIntervalLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
      }
    }
    
    if collectionView.tag == 7 {
      let recognizer = UITapGestureRecognizer(target: self, action: "cellTapped:")
      cell.addGestureRecognizer(recognizer)
      var availabilityArray = [Int]()
      
      switch indexPath.row % 7 {
      case 0:
        availabilityArray = availabilitySchedule["SUN"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 1:
        availabilityArray = availabilitySchedule["MON"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 2:
        availabilityArray = availabilitySchedule["TUE"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 3:
        availabilityArray = availabilitySchedule["WED"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 4:
        availabilityArray = availabilitySchedule["THU"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 5:
        availabilityArray = availabilitySchedule["FRI"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      case 6:
        availabilityArray = availabilitySchedule["SAT"]!
        if availabilityArray[indexPath.row / 7] == 1 {
          cell.backgroundColor = MehronColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      default:
        break
      }
    }
    return cell
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    var sideLength : CGFloat = (weekCollectionView.frame.width - 8) / 7
    cellHeight = 42
    maxPanlimit = CGFloat(timeIntervalString.count) * (cellHeight + 1)
    cellWidth = sideLength
    if collectionView.tag == 8 {
      return CGSizeMake(collectionView.frame.width, 43)
    } else if collectionView.tag == 7 {
      return CGSizeMake(sideLength, 42)
    } else {
      return CGSizeMake(sideLength, 42)
    }
  }
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.tag == 7 {
      var otherScrollView = self.timeCollectionView
      otherScrollView.contentOffset = scrollView.contentOffset
    }
  }
  
  func cellTapped(tapRecognizer : UITapGestureRecognizer) {
    
    let cell : UIView = tapRecognizer.view!
    
    if panView.hidden == false {
      panView.removeFromSuperview()
    }
    
    if cell.backgroundColor == MehronColor {
      refreshAvailability(cell)
    } else {
      panView = UIView(frame: CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cellWidth, 42))
      panView.backgroundColor = LightMehronColor
      weekCollectionView.addSubview(panView)
      createPanUI()
    }
  }
  
  func viewTapped(tapRecognizer: UITapGestureRecognizer) {
    let startCell : Int = Int(panView.frame.origin.y) / Int(cellHeight + 1)
    let slotsBooked : Int = Int(panView.frame.size.height) / Int(cellHeight)
    let daySelected : Int = Int(panView.frame.origin.x) / Int(cellWidth)
    
    var availabilityChangedForDay = dayForChange(daySelected)
    
    var updateAvailabilityArray : [Int] = availabilitySchedule["\(availabilityChangedForDay)"]!
    
    for slot in 0..<slotsBooked {
      
      updateAvailabilityArray[slot + startCell] = 1
    }
    
    panView.removeFromSuperview()
    
    availabilitySchedule["\(availabilityChangedForDay)"] = updateAvailabilityArray
    weekCollectionView.reloadData()
    
    println(panView.frame.origin.y + cellHeight + 1)
    //println(availabilityChangedForDay)
    //println(updateAvailabilityArray)
    
  }
  
  func viewMoved(panRecognizer : UIPanGestureRecognizer) {
    let translation = panRecognizer.translationInView(weekCollectionView)
    let newCentre = CGPoint(x:panRecognizer.view!.center.x, y:panRecognizer.view!.center.y + translation.y)
    panRecognizer.view!.center = CGPoint(x:panRecognizer.view!.center.x, y:panRecognizer.view!.center.y + translation.y)
    panRecognizer.setTranslation(CGPointZero, inView: self.view)
    
    if panRecognizer.state == UIGestureRecognizerState.Ended {
      var newYCoordinate = CGFloat(Int(panView.frame.origin.y) - Int(panRecognizer.view!.frame.origin.y) % Int(cellHeight + 1) + 1)
      panView.frame = CGRectMake(panView.frame.origin.x, newYCoordinate, panView.frame.size.width, panView.frame.size.height)
    }
  }
  
  func upPan(panRecognizer : UIPanGestureRecognizer) {
    let translation = panRecognizer.locationInView(panView)
    
    if panView.frame.size.height >=  cellHeight && panView.frame.origin.y > 0.1 {
      panRecognizer.view!.center = CGPoint(x:panRecognizer.view!.center.x, y:panRecognizer.view!.center.y + translation.y)
      panRecognizer.setTranslation(CGPointZero, inView: self.view)
      panView.frame = CGRectMake(panView.frame.origin.x, panView.frame.origin.y + translation.y, panView.frame.size.width, panView.frame.size.height - translation.y)
      upperPan.frame = CGRectMake(0, 0, upperPan.frame.size.width, upperPan.frame.size.height)
      lowerPan.frame = CGRectMake(0,panView.bounds.height - tickerHeight, upperPan.frame.size.width, upperPan.frame.size.height)
    }
    
    if panRecognizer.state == UIGestureRecognizerState.Ended {
      if panView.frame.origin.y > 0.0 {
        if Int(panView.frame.origin.y) % Int(cellHeight) != 0 {
          var gapBetweenCells = Int(panView.frame.size.height) / Int(cellHeight)
          var panHeight = CGFloat((gapBetweenCells + 1) * Int(cellHeight + 1))
          panView.frame = CGRectMake(panView.frame.origin.x,panView.frame.origin.y - CGFloat(Int(panView.frame.origin.y)%Int(cellHeight + 1)), panView.frame.size.width,panHeight)
          
          lowerPan.frame = CGRectMake(0,panView.bounds.height - tickerHeight, upperPan.frame.size.width, upperPan.frame.size.height)
          
          println(panView.frame.origin.y)
        }
      }
    }
  }
  
  func downPan(panRecognizer : UIPanGestureRecognizer) {
    
    let location = panRecognizer.locationInView(panView)
    let translation = location.y - panView.frame.height - tickerHeight
    //if /*panView.frame.size.height >=  cellHeight && */(panView.frame.origin.y + panView.frame.size.height) < maxPanlimit {
      panRecognizer.view!.center = CGPoint(x:panRecognizer.view!.center.x, y:panRecognizer.view!.center.y + translation)
      panRecognizer.setTranslation(CGPointZero, inView: self.view)
      panView.frame = CGRectMake(panView.frame.origin.x, panView.frame.origin.y, panView.frame.size.width, panView.frame.size.height + translation)
      lowerPan.frame = CGRectMake(0,panView.bounds.height - tickerHeight, upperPan.frame.size.width, upperPan.frame.size.height)
    //}
    
    if panRecognizer.state == UIGestureRecognizerState.Ended {
      var gapBetweenCells = Int(panView.frame.size.height) / Int(cellHeight)
      var panHeight = CGFloat((Int(panView.frame.size.height) / Int(cellHeight) + 1) * Int(cellHeight) + gapBetweenCells)
      if (panView.frame.origin.y + panView.frame.size.height) < maxPanlimit {
        panView.frame = CGRectMake(panView.frame.origin.x, panView.frame.origin.y, panView.frame.size.width, panHeight)
        lowerPan.frame = CGRectMake(0,panView.bounds.height - tickerHeight, upperPan.frame.size.width, upperPan.frame.size.height)
      } else {
        panHeight = maxPanlimit - panView.frame.origin.y
        //println(maxPanlimit)
        panView.frame = CGRectMake(panView.frame.origin.x, panView.frame.origin.y, panView.frame.size.width, panHeight)
        lowerPan.frame = CGRectMake(0,panView.bounds.height - tickerHeight, upperPan.frame.size.width, upperPan.frame.size.height)
      }
      
      
    }
    
  }
  
  func refreshAvailability(cell : UIView) {
    
    let dayTapped : Int = Int(cell.frame.origin.x) / Int(cellWidth)
    let currentIndex : Int = Int(cell.frame.origin.y) / Int(cellHeight + 1)
    let daySelected : String = dayForChange(dayTapped)
    var newArray : [Int] = availabilitySchedule["\(daySelected)"]!
    var countUp : Int = 0
    var countDown : Int = 0
    var upIndex = currentIndex - 1
    var downindex = currentIndex + 1
    var daysAvailable = 0
    
    for i in 0...95 {
      if upIndex != -1 {
        if newArray[upIndex] == 1 {
          upIndex--
          countUp++
          
        } else {
          break
        }
      }
    }
    
    for i in 0...95 {
      if downindex != 96 {
        if newArray[downindex] == 1 {
          downindex++
          countDown++
          
        } else {
          break
        }
      }
    }
    
    for i in upIndex + 1...downindex - 1 {
      newArray[i] = 0
      //println(i)
    }
    
    daysAvailable = countUp + countDown + 1
    //println(CGFloat(countUp) * cellHeight)
    availabilitySchedule["\(daySelected)"] = newArray
    //var firstCell = cellArray[upIndex]
    weekCollectionView.reloadData()
    panView = UIView(frame: CGRectMake(cell.frame.origin.x, cell.frame.origin.y - CGFloat(countUp) * cellHeight - CGFloat(countUp), cellWidth, CGFloat(daysAvailable) * cellHeight + CGFloat(daysAvailable)))
    panView.backgroundColor = LightMehronColor
    weekCollectionView.addSubview(panView)
    createPanUI()
    
    
    
  }
  
  func createPanUI() {
    
    var sideLength : CGFloat = (weekCollectionView.frame.width - 8) / 7
    upperPan = UIView(frame: CGRectMake(0, 0, sideLength, tickerHeight))
    upperPan.backgroundColor = MehronColor
    panView.addSubview(upperPan)
    lowerPan = UIView(frame: CGRectMake(0,panView.bounds.height - tickerHeight, sideLength, tickerHeight))
    lowerPan.backgroundColor = MehronColor
    panView.addSubview(lowerPan)
    let panRecognizer = UIPanGestureRecognizer(target: self, action: "viewMoved:")
    let tapRecognizer = UITapGestureRecognizer(target: self, action: "viewTapped:")
    panView.addGestureRecognizer(panRecognizer)
    panView.addGestureRecognizer(tapRecognizer)
    let upperPanRecognizer = UIPanGestureRecognizer(target: self, action: "upPan:")
    upperPan.addGestureRecognizer(upperPanRecognizer)
    let lowerPanRecognizer = UIPanGestureRecognizer(target: self, action: "downPan:")
    lowerPan.addGestureRecognizer(lowerPanRecognizer)
    
  }
  
  func dayForChange(daynumber : Int) -> String  {
    var dayString = String()
    switch daynumber {
    case 0: dayString = "SUN"
    case 1: dayString = "MON"
    case 2: dayString = "TUE"
    case 3: dayString = "WED"
    case 4: dayString = "THU"
    case 5: dayString = "FRI"
    case 6: dayString = "SAT"
    default:
      break
    }
    
    return dayString
  }
  
  func timeIntervalForAppointments() {
    var meridiem = "AM"
    var hours = 12
    for hour in 0...23 {
      for minute in 0...3 {
        if minute == 0 && hour < 12 {
          timeIntervalString.append("\(hours) AM")
        } else if minute == 0 && hour >= 12 {
          timeIntervalString.append("\(hours) PM")
        } else {
          timeIntervalString.append("\(hours):\(minute * 15)")
        }
      }
      
      hours++
      if hours == 13 {
        hours = 1
      }
    }
    // println(timeIntervalString)
  }
  
  @IBAction func saveAvailabilityButtonClicked(sender: AnyObject) {
    println(availabilitySchedule)
    let othstoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("viewAppointment") as ViewAppointmentViewController
    self.navigationController!.pushViewController(destinationView, animated: true)
  }
}
