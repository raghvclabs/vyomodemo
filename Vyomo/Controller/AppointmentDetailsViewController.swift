//
//  AppointmentDetailsViewController.swift
//  Vyomo
//
//  Created by Click Labs on 4/3/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class AppointmentDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  @IBOutlet weak var profileImage: UIImageView!
  @IBOutlet weak var appointmentTable: UITableView!
  @IBOutlet var customerName: UILabel!
  @IBOutlet var appointmentTimeLabel: UILabel!
  @IBOutlet var todayDate: UILabel!
  
  var accessToken = "YW5kcm9pZF9tYW5hZ2VyU2F0IE1hciAyOCAyMDE1IDA4OjUwOjM4IEdNVCswMDAwIChVVEMp"
  var businessId = "1"
  var appointmentId = "7"
  var stylistId = "2"
  var flag = 0
  var numberOfRows = 0
  var todaysDate = NSDate()
  var dayName = String()
  var date = String()
  var year = String()
  var month = String()
  var serviceDurationArray = [NSString]()
  var appointmentServiceIdArray = [Int]()
  var serviceCostArray =  [Int]()
  var serviceNameArray = [NSString]()
  
  @IBAction func cancelButton(sender: UIButton) {
    let alert = UIAlertController(title: "CANCEL APPOINTMENT", message: "Do you want to cancel this appointment?", preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
    }))
    alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
    }))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  @IBAction func markAsCompleteButton(sender: UIButton) {
    let alert = UIAlertController(title: "CONFIRM", message: "Are you sure thet you want to mark this appointment as no show?\nNote: There's no undo!", preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
    }))
    alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
    }))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //showDate()
    getAppointmentDetail(7)
    // Do any additional setup after loading the view.
    appointmentTable.separatorColor = DarkGrayColor
    //appointmentTable.bounces = false
    self.appointmentTable.rowHeight = 98
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
  }
  
  func markNoShow(){
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(appointmentId )","appointment_id")
    println(arr_parameters)
    json.session("mark_no_show", parameters: arr_parameters, completion: {
      result in
      let jsonResult = result as NSDictionary
      println(jsonResult)
      let status = jsonResult["status"]! as Int
      let message = jsonResult["message"]! as NSString
      let parsedResult = jsonResult["data"]! as NSDictionary
      var emptyDictionary = NSDictionary()
      println("GetdefaultStylistAppointment_today: \(parsedResult)")
      dispatch_async(dispatch_get_main_queue()){
        if status == 200 {
          if parsedResult ==  emptyDictionary {
            println("imhere")
          } else {
            //code to be added
          }
        }
      }
    })
  }
  
  func cancelAppointment(){
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(appointmentId )","appointment_id"," \(flag)","flag")
    json.session("cancel_appointment", parameters: arr_parameters, completion: {
      result in
      let jsonResult = result as NSDictionary
      println(jsonResult)
      let status = jsonResult["status"]! as Int
      let message = jsonResult["message"]! as NSString
      let parsedResult = jsonResult["data"]! as NSDictionary
      var emptyDictionary = NSDictionary()
      dispatch_async(dispatch_get_main_queue()){
        if status == 200 {
          if parsedResult ==  emptyDictionary {
            println("imhere")
          } else {
            //code to be added
          }
        }
      }
    })
    
  }
  
  func showDate() {
    var dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd"
    var extractedDate = dateFormatter.stringFromDate(todaysDate)
    date = extractedDate
    dateFormatter.dateFormat = "MM"
    var extractedMonthName = dateFormatter.stringFromDate(todaysDate) as NSString
    dateFormatter.dateFormat = "YYYY"
    var extractedYear = dateFormatter.stringFromDate(todaysDate)
    year = extractedYear
    dateFormatter.dateFormat = "EEEE"
    var extractedday = dateFormatter.stringFromDate(todaysDate) as NSString
    var truncatedDayName = extractedday.substringWithRange(NSMakeRange(0, 3))
    todayDate.text = "\(truncatedDayName), \(date)/\(extractedMonthName)/\(year)"
  }
  

  
  func getAppointmentDetail(appointmentId:Int){
    refresh()
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","access_token",
      "\(appointmentId )","appointment_id")
    println(arr_parameters)
    // var get_appointment_detail =  "http://54.173.217.54:8080/get_appointment_details"
    json.session("get_appointment_details", parameters: arr_parameters, completion: {
      result in
      let jsonResult = result as NSDictionary
      println(jsonResult)
      let status = jsonResult["status"]! as Int
      let message = jsonResult["message"]! as NSString
      let parsedResult = jsonResult["data"]! as NSDictionary
      var emptyDictionary = NSDictionary()
      println("GetdefaultStylistAppointment_today: \(parsedResult)")
      dispatch_async(dispatch_get_main_queue()){
        if status == 200 {
          if parsedResult ==  emptyDictionary {
            println("imhere")
          } else {
            var data = parsedResult["appointment_details"]! as NSDictionary
            //println(data)
            var startTime : NSString = data["appointment_start_time"]! as NSString
            self.date = startTime.componentsSeparatedByString("T")[0] as NSString
            if let weekday = self.getDayOfWeek(self.date) {
              self.dayName = self.DayIndex(weekday)
            } else {
              println("bad input")
            }
            self.todayDate.text = " \(self.dayName), \(self.date) "
            self.todayDate.textColor =  TimeLabelColor
            self.todayDate.font = UIFont(name: "Raleway-Regular" , size: 16)
            startTime = startTime.componentsSeparatedByString("T")[1] as NSString
            startTime = startTime.componentsSeparatedByString(":00.000Z")[0] as NSString
            //println(startTime)
            var endTime : NSString = data["appointment_end_time"]! as NSString
            endTime = endTime.componentsSeparatedByString("T")[1] as NSString
            endTime = endTime.componentsSeparatedByString(":00.000Z")[0] as NSString
            //println(endTime)
            var customerName : NSString = data["customer_full_name"]! as NSString
            //println(customerName)
            self.appointmentTimeLabel.text = " \(startTime)  to \(endTime)"
            //self.appointmentTimeLabel.textColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
            self.appointmentTimeLabel.font = UIFont(name: "Raleway-Regular" , size: 16)
            self.customerName.text = customerName
            self.customerName.textColor =  TimeLabelColor
            self.customerName.font = UIFont(name: "Raleway-Regular" , size: 16)
            var appointmentServices = parsedResult["appointment_services_details"]! as NSArray
            //println(appointmentServices)
            self.numberOfRows = appointmentServices.count
            //println(self.numberOfRows)
            for i in  0..<appointmentServices.count{
              var otherAppointments :NSDictionary = appointmentServices[i] as NSDictionary
              //println(otherAppointments)
              var serviceName : NSString =  otherAppointments["service_name"]! as NSString
              self.serviceNameArray.append(serviceName)
              var serviceCost : Int = otherAppointments["service_cost"]! as Int
              self.serviceCostArray.append(serviceCost)
              var appointmentServiceId : Int = otherAppointments["appointment_service_id"]! as Int
              self.appointmentServiceIdArray.append(appointmentServiceId)
              var serviceDuration : NSString = otherAppointments["service_duration"]! as  NSString
              self.serviceDurationArray.append(serviceDuration)
            }
            self.appointmentTable.reloadData()
          }
        }
      }
    })
  }
  
  func getDayOfWeek(today:String)->Int? {
    let formatter  = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    if let todayDate = formatter.dateFromString(today) {
      let myCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)
      let myComponents = myCalendar?.components(.WeekdayCalendarUnit, fromDate: todayDate)
      let weekDay = myComponents?.weekday
      return weekDay
    } else {
      return nil
    }
  }
  
  func DayIndex(i:Int)-> String {
    var DayCheck = " "
    switch i {
    case 0:
      DayCheck = "Sun"
    case 1:
      DayCheck = "Mon"
    case 2:
      DayCheck = "Tue"
    case 3:
      DayCheck = "Wed"
    case 4:
      DayCheck = "Thur"
    case 5:
      DayCheck = "Fri"
    case 6:
      DayCheck = "Sat"
    default: break
    }
    return DayCheck
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return numberOfRows
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
    for  lbl in cell.subviews {
      if lbl.isKindOfClass(UIView)
      {
        lbl.removeFromSuperview()
      }
    }
    var myView = UIView(frame: CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height))
    //myView.backgroundColor = UIColor.greenColor()
    cell.addSubview(myView)
    var serviceNameLabel = UILabel(frame: CGRectMake(10 , 31 , cell.frame.size.width/3, cell.frame.size.height/3 ))
    serviceNameLabel.text  =  "\(self.serviceNameArray[indexPath.row])"
    serviceNameLabel.textAlignment = NSTextAlignment.Center
    serviceNameLabel.textColor = TimeLabelColor
    serviceNameLabel.font = UIFont(name: "Raleway-Regular" , size: 18)
    myView.addSubview(serviceNameLabel)
    var serviceDurationLabel: UILabel = UILabel()
    serviceDurationLabel.frame = CGRectMake(10, 52, cell.frame.size.width/3, cell.frame.size.height/3 )
    serviceDurationLabel.textColor = UIColor(red: 211/255, green: 211/255, blue: 214/255, alpha: 1)
    serviceDurationLabel.font = UIFont(name: "Raleway-Regular" , size: 15)
    serviceDurationLabel.textAlignment = NSTextAlignment.Center
    serviceDurationLabel.text = "Duration: \(self.serviceDurationArray[indexPath.row])"
    myView.addSubview(serviceDurationLabel)
    var currency: UIImageView = UIImageView ()
    currency.frame = CGRectMake(cell.frame.size.width-100, 38, cell.frame.size.width/6, cell.frame.size.height/4 )
    currency.contentMode = UIViewContentMode.ScaleAspectFit
    currency.image = UIImage(named: "rupee_3x.png")
    myView.addSubview(currency)
    var serviceCostLabel: UILabel = UILabel()
    serviceCostLabel.frame = CGRectMake(cell.frame.size.width-60, 31, cell.frame.size.width/4, cell.frame.size.height/2 )
    serviceCostLabel.textColor = PinkColor
    serviceCostLabel.font = UIFont(name: "Raleway-Regular" , size: 16)
    serviceCostLabel.text = "\(self.serviceCostArray[indexPath.row])"
    myView.addSubview(serviceCostLabel)
    return cell
  }
  
  
  
  //let TimeLabelColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
  
  //  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
  //
  //    getAppointmentDetail(self.appointmentServiceIdArray[indexPath.row])
  //
  //    //tableView.reloadData()
  //  }
  
  // MARK: - Navigation
  /*
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
  
  func getFlagForStylistId() -> Int {
    var flag = 1
    var stylist = 0
    //if roleId == 1  {
    //stylist = stylistIdForDefault
    //flag = 1
    //}
    return flag
  }
  
  func refresh(){
    self.serviceCostArray = []
    self.serviceDurationArray = []
    self.appointmentServiceIdArray = []
    self.serviceNameArray = []
  }
  
  func showAlert(string : NSString) {
    var alert = UIAlertView()
    alert.message = string
    alert.addButtonWithTitle("OK")
    alert.dismissWithClickedButtonIndex(0, animated: true)
    alert.show()
  }
  
}
