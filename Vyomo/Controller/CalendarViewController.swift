import UIKit


class CalendarViewController: UIViewController,UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
  
  
  @IBOutlet weak var stylistPickerView: UIPickerView!
  
  @IBOutlet weak var stylistName: UILabel!
  @IBOutlet weak var activityBarView: UIView!
  @IBOutlet weak var activityBar: UIActivityIndicatorView!
  
  var appointmentIdArray = [String]()
  var tableViewClientNameArray = [String]()
  var tableViewServiceDetailsArray = [String]()
  var eventBlockedState = [Int]()
  var scrollPointWeekView = CGPoint()
  var scrollPointMonthView = CGPoint()
  var relativeCounter = 0
  var relativeWeekCounter = 0
  var relativeMonthCounter = 0
  var previousMonthLabel = [UILabel]()
  var currentMonthLabel = [UILabel]()
  var nextMonthLabel = [UILabel]()
  var weekLabel = [UILabel]()
  
  var defaultStylistId = -1
  var stylistId = [Int]()
  var stylistNames = [String]()
  
  var monthChangeInWeekView = 0
  
  var monthViewButtonTapCount = 0
  var weekViewButtonTapCount = 0
  
  var todayLabelTagWeekView = 0
  var todayLabelTagMonthView = 0
  var tappedLabelTagWeekView = 0
  var tappedLabelTagMonthView = 0
  
  var daysMovedForward = 0
  var daysMovedBackward = 0
  
  var startDate = String()
  var endDate = String()
  
  var firstCall = 0
  
  var eventCheckTimer = NSTimer()
  var formattedAppointmentdate = String()
  var formattedAppointmentTime = Int()
  
  
  var timeIntervalString = [String]()
  var currentMonthName = String()
  var nextMonthName = String()
  var previousMonthName = String()
  
  var tappedDate = String()
  var tappedDay = String()
  var tappedMonth = String()
  var tappedYear = String()
  
  let calendarData = CalendarViewData()
  let dataClass = MonthAppointmentsData()
  var appointmentsOnDate = Dictionary<String, [NSDictionary]>()
  var weekAppointmentData = Dictionary<String,[Int]>()
  var defaultAvailability = Dictionary<String, [Int]>()
  var monthAppointmentArray = [NSDictionary]()
  
  var callStartTime = String()
  var callStopTime = String()
  
  var addAppointmentTime = String()
  var addAppointmentDate = String()
  var dayViewDayIndex = String()
  
  @IBOutlet weak var eventCollectionView: UICollectionView!
  @IBOutlet weak var timeCollectionView: UICollectionView!
  
  
  @IBOutlet weak var currentMonthNameLabel: UILabel!
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var monthLabel: UILabel!
  @IBOutlet weak var yearLabel: UILabel!
  @IBOutlet weak var dayLabel: UILabel!
  
  @IBOutlet weak var timeTableView: UITableView!
  @IBOutlet weak var eventTableView: UITableView!
  
  @IBOutlet weak var dayViewButton: UIButton!
  @IBOutlet weak var weekViewButton: UIButton!
  @IBOutlet weak var monthViewButton: UIButton!
  
  
  @IBOutlet weak var selectStylistView: UIView!
  @IBOutlet weak var dateDisplayView: UIView!
  @IBOutlet weak var monthNameDisplayView: UIView!
  @IBOutlet weak var monthWeekDisplayView: UIView!
  @IBOutlet weak var weekDisplayView: UIView!
  @IBOutlet weak var weekView: UIScrollView!
  @IBOutlet weak var monthView: UIScrollView!
  var screenWidth = CGFloat()
  
  var firstDayCheck = ""
  var currentMonthIndex = 0
  var previousMonthIndex = 0
  var nextMonthIndex = 0
  
  // @IBOutlet weak var butt: UIButton!
  override func viewDidLoad() {
    super.viewDidLoad()
  if defaultStylistId == 0 {
      defaultStylistId = -1
    }
    
    if RoleId == 1 {
      let constHeight = NSLayoutConstraint(item: self.selectStylistView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 0)
      selectStylistView.addConstraint(constHeight)
      selectStylistView.updateConstraints()
      selectStylistView.hidden = true
    } else {
      let aspectRatio = NSLayoutConstraint(item: self.selectStylistView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: self.selectStylistView, attribute: NSLayoutAttribute.Width, multiplier: 138.0/25.0, constant: 0)
      selectStylistView.addConstraint(aspectRatio)
      selectStylistView.updateConstraints()
      println("here")
    }
    
    
    for items in 0...95 {
      appointmentIdArray.append("")
      tableViewClientNameArray.append("")
      tableViewServiceDetailsArray.append("")
      eventBlockedState.append(0)
    }
    
    timeIntervalForAppointments()
    
    calendarData.currentWeek()
    calendarData.currentMonth()
    calendarData.today()
    relativeCounter = 0
    relativeWeekCounter = 0
    relativeMonthCounter = 0
    screenWidth = self.view.bounds.size.width
    weekViewButton.backgroundColor = PinkColor
    weekViewButton.highlighted = false
    weekViewFunction()
    weekCall()
    
    
  }
  
  
  
  func getAppointments (beginningDate : String, endingDate : String, mystylistId: String) {
    println("default stylist id  == \(mystylistId)")
    
    for i in 0..<stylistId.count{
      if stylistId[i] == mystylistId.toInt()! {
        stylistName.text = stylistNames[i]
        stylistName.textColor = PinkColor
      }
    }
    
    callStartTime = beginningDate
    callStopTime = endingDate
    startLoading()
    let json = JSON()
    var start = NSString(string: beginningDate)
    var end = NSString(string: endingDate)
    var arr_parameters = NSDictionary(objectsAndKeys:
      AccessToken,"access_token",BuisnessIdForRegistration,"business_id",mystylistId,"stylist_id",beginningDate,"start_date",endingDate,"end_date")
    if mystylistId != "-1" {
      dispatch_async(dispatch_get_main_queue()){
        json.session("stylist_month_calendar", parameters: arr_parameters, completion: {
          response in
          let data = response as NSDictionary
          self.monthAppointmentArray = []
          self.appointmentsOnDate = [:]
          self.defaultAvailability = [:]
          if let availabilityArray = data["defaultAvailabilityBit"] as? [NSDictionary] {
            for availability in availabilityArray {
              var day = availability["day_id"]! as Int
              day -= 1
              self.defaultAvailability["\(day)"] = availability["bit"]! as? [Int]
            }
          }
          
          if let appointmentArray = data["appointmentArray"] as? [NSDictionary] {
            for appointments in appointmentArray {
              self.monthAppointmentArray.append(appointments)
            }
            
            
            println(appointmentArray)
            
            for detail in self.monthAppointmentArray {
              let date = (detail["appointment_start_time"]! as String).componentsSeparatedByString("T")[0]
              if self.appointmentsOnDate[date] == nil {
                self.appointmentsOnDate[date] = []
                self.appointmentsOnDate[date]?.append(detail)
              } else {
                self.appointmentsOnDate[date]?.append(detail)
              }
            }
          }
          dispatch_async(dispatch_get_main_queue()){
            
            if self.monthView.hidden == false {
              
              let month = self.currentMonthName.componentsSeparatedByString("  ")[0]
              let year = self.currentMonthName.componentsSeparatedByString("  ")[1]
              self.displayMonthAppointments(month, year: year)
              self.stopLoading()
              
            } else if self.weekView.hidden == false {
              self.weekViewData()
              self.stopLoading()
            } else if self.eventTableView.hidden == false {
              self.dayViewData()
            }
          }
        })
      }
    } else {
      var arr_parameters = NSDictionary(objectsAndKeys: "\(AccessToken)","access_token","\(BuisnessIdForRegistration)","business_id")
      json.session("get_stylist_list", parameters: arr_parameters, completion: {
        result in
        let parsedResult = result as NSArray
        println(parsedResult)
        var resultCounter = 0
        for num in parsedResult {
          let stylistNamesData = parsedResult[resultCounter]["user_name"]! as String
          let stylistIdData = parsedResult[resultCounter]["service_provider_id"]!! as Int
          self.stylistNames.append(stylistNamesData)
          self.stylistId.append(stylistIdData)
          resultCounter++
        }
      })
    }
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func weekViewFunction() {
    
    tappedDate = ""
    
    var viewWidth = screenWidth - timeCollectionView.frame.size.width
    monthView.hidden = true
    monthWeekDisplayView.hidden = true
    refresh()
    weekView.hidden = false
    weekDisplayView.hidden = false
    timeCollectionView.hidden = false
    eventCollectionView.hidden = false
    refreshWeekDisplayView()
    weekLabel = []
    var constW = NSLayoutConstraint(item: weekDisplayView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: viewWidth)
    weekDisplayView.addConstraint(constW)
    weekDisplayView.updateConstraints()
    
    var xValue : CGFloat = 8
    let sideLength : CGFloat = (viewWidth - (8 * xValue)) / 7
    let spaceBetweenDates : CGFloat = (viewWidth - (7 * sideLength)) / 8
    
    for i in 0...20 {
      if i < 7 {
        var dayLabel = UILabel(frame: CGRectMake(xValue,0, sideLength, sideLength))
        switch i {
        case 0:
          dayLabel.text = "S"
        case 1:
          dayLabel.text = "M"
        case 2:
          dayLabel.text = "T"
        case 3:
          dayLabel.text = "W"
        case 4:
          dayLabel.text = "T"
        case 5:
          dayLabel.text = "F"
        case 6:
          dayLabel.text = "S"
        default: break
        }
        dayLabel.font = UIFont(name: "Raleway", size: 15.0)
        dayLabel.textColor = DarkGrayColor
        dayLabel.textAlignment = NSTextAlignment.Center
        weekDisplayView.addSubview(dayLabel)
      }
      
      var label = UILabel(frame: CGRectMake(xValue,0, sideLength, sideLength))
      label.font = UIFont(name: "Raleway", size: 15.0)
      label.textColor = DarkGrayColor
      xValue = xValue + sideLength + spaceBetweenDates
      weekLabel.append(label)
      if i < 7 {
        calendarData.previousWeek(-1)
        label.text = "\(calendarData.previousWeekString[i].toInt()!)"
      } else if i < 14 {
        calendarData.currentWeek()
        weekLabel[i].text = "\(calendarData.currentWeekString[i-7].toInt()!)"
        weekLabel[i].tag = i - 7 + 300
        let recognizer = UITapGestureRecognizer(target: self, action: "jumpWeekViewToDayView:")
        weekLabel[i].addGestureRecognizer(recognizer)
        weekLabel[i].userInteractionEnabled = true
      } else {
        calendarData.nextWeek(1)
        label.text = "\(calendarData.nextWeekString[i-14].toInt()!)"
      }
      
      if label.text?.toInt() == calendarData.todayDate.toInt() {
        label.backgroundColor = UIColor.whiteColor()
        label.textColor = PinkColor
        todayLabelTagWeekView = label.tag
      } else {
        label.backgroundColor = UIColor.clearColor()
      }
      
      label.layer.cornerRadius = sideLength / 2
      label.layer.masksToBounds = true
      label.textAlignment = NSTextAlignment.Center
      weekView.addSubview(label)
    }
    
    weekView.contentSize = CGSizeMake(xValue, 20)
    scrollPointWeekView = CGPointMake((xValue/3)-(spaceBetweenDates/2), 0)
    weekView.contentOffset = scrollPointWeekView
    
    
  }
  
  func monthViewFunction() {
    
    tappedDate = ""
    calendarData.currentMonth()
    println(calendarData.currentMonthArray)
    calendarData.nextMonth(1)
    calendarData.previousMonth(-1)
    refresh()
    weekView.hidden = true
    monthView.hidden = false
    weekDisplayView.hidden = true
    monthWeekDisplayView.hidden = false
    monthNameDisplayView.hidden = false
    previousMonthLabel = []
    currentMonthLabel = []
    nextMonthLabel = []
    var check = 1
    var daysCounter = 0
    var yValue : CGFloat = 1
    var xValue : CGFloat = 1
    let sideLengthXAxis : CGFloat = (screenWidth - 8) / 7
    let sideLengthYAxis : CGFloat = (monthView.frame.size.height - 7) / 6
    let spaceBetweenDatesXAxis : CGFloat = 1
    let spaceBetweenDatesYAxis : CGFloat = 1
    let weekHeight = monthWeekDisplayView.frame.size.height
    xValue = 1
    for i in 0...6 {
      var dayLabel = UILabel(frame: CGRectMake(xValue, 0 , sideLengthXAxis, weekHeight))
      switch i {
      case 0:
        dayLabel.text = "SUN"
        firstDayCheck = "Sunday"
      case 1:
        dayLabel.text = "MON"
        firstDayCheck = "Monday"
      case 2:
        dayLabel.text = "TUE"
        firstDayCheck = "Tuesday"
      case 3:
        dayLabel.text = "WED"
        firstDayCheck = "Wednesday"
      case 4:
        dayLabel.text = "THU"
        firstDayCheck = "Thursday"
      case 5:
        dayLabel.text = "FRI"
        firstDayCheck = "Friday"
      case 6:
        dayLabel.text = "SAT"
        firstDayCheck = "Saturday"
      default: break
      }
      
      if calendarData.firstDayCurrentMonth == firstDayCheck {
        currentMonthIndex = i
      }
      
      if calendarData.firstDayNextMonth == firstDayCheck {
        nextMonthIndex = i
      }
      
      if calendarData.firstDayPreviousMonth == firstDayCheck {
        previousMonthIndex = i
      }
      
      dayLabel.textColor = DarkBlueColor
      dayLabel.textAlignment = NSTextAlignment.Center
      dayLabel.numberOfLines = 1
      dayLabel.adjustsFontSizeToFitWidth = true
      monthWeekDisplayView.addSubview(dayLabel)
      /*
      var constH = NSLayoutConstraint(item: monthWeekDisplayView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: sideLengthYAxis)
      monthWeekDisplayView.addConstraint(constH)
      monthWeekDisplayView.updateConstraints()
      */
      xValue = xValue + dayLabel.bounds.width + spaceBetweenDatesXAxis
    }
    
    xValue = 1
    for i in 1...6 {
      for j in 0...6 {
        var monthArrayindex = 7*(i-1)+j
        var label = UILabel(frame: CGRectMake(xValue, yValue, sideLengthXAxis, sideLengthYAxis))
        label.textColor = DarkBlueColor
        label.font = UIFont(name: "Raleway", size: 15.0)
        xValue = xValue + sideLengthXAxis + spaceBetweenDatesXAxis
        if i >= 0 && j >= previousMonthIndex * check && daysCounter < calendarData.previousMonthArray.count {
          label.text = calendarData.previousMonthArray[monthArrayindex]
          daysCounter++
          check = 0
        } else {
          label.text = ""
        }
        label.backgroundColor = LightGrayColor
        label.textAlignment = NSTextAlignment.Center
        monthView.addSubview(label)
        previousMonthLabel.append(label)
      }
      
      xValue = 1
      yValue = yValue + sideLengthYAxis + spaceBetweenDatesYAxis
    }
    
    check = 1
    daysCounter = 0
    xValue = xValue + screenWidth
    yValue = 1
    for i in 1...6 {
      for j in 0...6 {
        var monthArrayindex = 7*(i-1)+j
        var label = UILabel(frame: CGRectMake(xValue, yValue, sideLengthXAxis, sideLengthYAxis))
        label.textColor = DarkBlueColor
        label.tag = 200 + 7*(i-1)+j
        label.font = UIFont(name: "Raleway", size: 15.0)
        label.backgroundColor = LightGrayColor
        currentMonthLabel.append(label)
        xValue = xValue + sideLengthXAxis + spaceBetweenDatesXAxis
        if i >= 0 && j >= currentMonthIndex * check && daysCounter < calendarData.currentMonthArray.count {
          
          label.text = calendarData.currentMonthArray[daysCounter]
          daysCounter++
          check = 0
          if label.text?.toInt() == calendarData.todayDate.toInt() {
            label.backgroundColor = DarkBlueColor
            label.textColor = LightGrayColor
            todayLabelTagMonthView = label.tag
          }
        } else {
          label.text = ""
        }
        
        label.textAlignment = NSTextAlignment.Center
        monthView.addSubview(label)
      }
      
      xValue = 1 + screenWidth
      yValue = yValue + sideLengthYAxis + spaceBetweenDatesYAxis
    }
    
    check = 1
    daysCounter = 0
    xValue = xValue + screenWidth
    yValue = 1
    for i in 1...6 {
      for j in 0...6 {
        var monthArrayindex = 7*(i-1)+j
        var label = UILabel(frame: CGRectMake(xValue, yValue, sideLengthXAxis, sideLengthYAxis))
        label.textColor = DarkBlueColor
        label.font = UIFont(name: "Raleway", size: 15.0)
        nextMonthLabel.append(label)
        xValue = xValue + sideLengthXAxis + spaceBetweenDatesXAxis
        if i >= 0 && j >= nextMonthIndex * check && daysCounter < calendarData.nextMonthArray.count {
          label.text = calendarData.nextMonthArray[daysCounter]
          daysCounter++
          check = 0
        } else {
          label.text = ""
        }
        
        label.backgroundColor = LightGrayColor
        label.textAlignment = NSTextAlignment.Center
        monthView.addSubview(label)
      }
      xValue = 1 + (2 * screenWidth)
      yValue = yValue + sideLengthYAxis + spaceBetweenDatesYAxis
    }
    
    yValue = 5
    monthView.contentSize = CGSizeMake(xValue + screenWidth, yValue)
    scrollPointMonthView = CGPointMake(((xValue + screenWidth)/3)-(spaceBetweenDatesXAxis/2), 0)
    monthView.contentOffset = scrollPointMonthView
    currentMonthName = calendarData.currentMonthNameString
    nextMonthName = calendarData.nextMonthNameString
    previousMonthName = calendarData.previousMonthNameString
    monthNameDisplay()
    
    for days in currentMonthLabel {
      let recognizer = UITapGestureRecognizer(target: self, action: "jumpMonthViewToDayView:")
      days.addGestureRecognizer(recognizer)
      days.userInteractionEnabled = true
    }
  }
  
  func presentPreviousMonth() {
    var daysCounter = 0
    var daysInCurrentMonth = 0
    relativeMonthCounter -= 1
    calendarData.previousMonth(relativeMonthCounter-1)
    checkFirstDayIndex()
    for i in 0...41 {
      currentMonthLabel[i].textColor = DarkBlueColor
    }
    
    for i in 0...41 {
      currentMonthLabel[i].backgroundColor = LightGrayColor
      nextMonthLabel[i].text = currentMonthLabel[i].text
    }
    
    for i in 0...41 {
      currentMonthLabel[i].text = previousMonthLabel[i].text
      if currentMonthLabel[i].text != "" {
        daysInCurrentMonth++
      }
    }
    
    for i in 0...41 {
      if i >= previousMonthIndex && daysCounter < calendarData.previousMonthArray.count {
        previousMonthLabel[i].text = calendarData.previousMonthArray[daysCounter]
        daysCounter++
      } else {
        previousMonthLabel[i].text = ""
      }
    }
    
    var nextmonthDaysCount = 0
    
    for day in 0...41 {
      if nextMonthLabel[day].text != "" {
        nextmonthDaysCount++
      }
    }
    println(nextmonthDaysCount)
    daysMovedBackward -= nextmonthDaysCount
    daysMovedForward -= nextmonthDaysCount
    
    nextMonthName = currentMonthName
    currentMonthName = previousMonthName
    previousMonthName = calendarData.previousMonthNameString
    monthNameDisplay()
    if relativeMonthCounter == 0 {
      for i in 0...41 {
        if currentMonthLabel[i].text?.toInt() == calendarData.todayDate.toInt() {
          currentMonthLabel[i].backgroundColor = DarkBlueColor
          currentMonthLabel[i].textColor = LightGrayColor
        }
      }
    }
    
    if relativeMonthCounter % 2 == 0 {
      let startMonthName = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[0]
      let endMonthName = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[0]
      let startYear = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[1]
      let endYear = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[1]
      startDate = dateFormatter("1", month: startMonthName, year: startYear)
      endDate = dateFormatter("\(daysInCurrentMonth)", month: endMonthName, year: endYear)
      getAppointments(startDate, endingDate: endDate, mystylistId: "\(defaultStylistId)")
      
    }
    
    let month = currentMonthName.componentsSeparatedByString("  ")[0]
    let year = currentMonthName.componentsSeparatedByString("  ")[1]
    displayMonthAppointments(month, year: year)
  }
  
  func presentNextMonth() {
    var daysCounter = 0
    var daysInCurrentMonth = 0
    relativeMonthCounter += 1
    calendarData.nextMonth(relativeMonthCounter+1)
    checkFirstDayIndex()
    for i in 0...41 {
      currentMonthLabel[i].textColor = DarkBlueColor
    }
    
    for i in 0...41 {
      currentMonthLabel[i].backgroundColor = LightGrayColor
      previousMonthLabel[i].text = currentMonthLabel[i].text
    }
    
    for i in 0...41 {
      currentMonthLabel[i].text = nextMonthLabel[i].text
      if currentMonthLabel[i].text != "" {
        daysInCurrentMonth++
      }
    }
    
    for i in 0...41 {
      if i >= nextMonthIndex && daysCounter < calendarData.nextMonthArray.count {
        nextMonthLabel[i].text = calendarData.nextMonthArray[daysCounter]
        daysCounter++
      } else {
        nextMonthLabel[i].text = ""
      }
    }
    
    var previousMonthDaysCount = 0
    
    for day in 0...41 {
      if previousMonthLabel[day].text != "" {
        previousMonthDaysCount++
      }
    }
    
    daysMovedForward += previousMonthDaysCount
    daysMovedBackward += previousMonthDaysCount
    
    previousMonthName = currentMonthName
    currentMonthName = nextMonthName
    nextMonthName = calendarData.nextMonthNameString
    monthNameDisplay()
    if relativeMonthCounter == 0 {
      for i in 0...41 {
        if currentMonthLabel[i].text?.toInt() == calendarData.todayDate.toInt() {
          currentMonthLabel[i].backgroundColor = DarkBlueColor
          currentMonthLabel[i].textColor = LightGrayColor
        }
      }
    }
    
    if relativeMonthCounter % 2 == 0 {
      let startMonthName = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[0]
      let endMonthName = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[0]
      let startYear = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[1]
      let endYear = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[1]
      startDate = dateFormatter("1", month: startMonthName, year: startYear)
      endDate = dateFormatter("\(daysInCurrentMonth)", month: endMonthName, year: endYear)
      getAppointments(startDate, endingDate: endDate, mystylistId: "\(defaultStylistId)")
    }
    
    let month = currentMonthName.componentsSeparatedByString("  ")[0]
    let year = currentMonthName.componentsSeparatedByString("  ")[1]
    displayMonthAppointments(month, year: year)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    
    if scrollView.tag == 0 {
      if weekLabel.count != 0 {
        var pageWidth = weekView.frame.size.width
        var pageNumber = floor((weekView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        for i in 0...20 {
          weekLabel[i].backgroundColor = UIColor.clearColor()
          weekLabel[i].textColor = DarkGrayColor
        }
        
        if pageNumber == 0.0 {
          relativeWeekCounter -= 1
          calendarData.previousWeek(relativeWeekCounter-1)
          for i in 14...20 {
            weekLabel[i].text = weekLabel[i-7].text
          }
          
          for i in 7...13 {
            weekLabel[i].text = weekLabel[i-7].text
          }
          
          for i in 0...6 {
            weekLabel[i].text = "\(calendarData.previousWeekString[i].toInt()!)"
          }
          
        } else if pageNumber == 2.0 {
          relativeWeekCounter += 1
          calendarData.nextWeek(relativeWeekCounter+1)
          for i in 7...13 {
            weekLabel[i-7].text = weekLabel[i].text
          }
          
          for i in 14...20 {
            weekLabel[i-7].text = weekLabel[i].text
          }
          
          for i in 14...20 {
            if let numStr = weekLabel[i].text {
              weekLabel[i].text = "\(calendarData.nextWeekString[i-14].toInt()!)"
            }
          }
        }
        
        if relativeWeekCounter == 0 {
          for i in 7...13 {
            if weekLabel[i].text?.toInt() == calendarData.todayDate.toInt() {
              weekLabel[i].backgroundColor = UIColor.whiteColor()
              weekLabel[i].textColor = PinkColor
            }
          }
        }
        
        weekView.contentOffset = scrollPointWeekView
        weekCall()
        weekViewData()
      }
      
      if currentMonthLabel.count != 0 {
        var pageWidth = monthView.frame.size.width
        var pageNumber = floor((monthView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        if pageNumber == 0.0 {
          presentPreviousMonth()
        } else if pageNumber == 2.0 {
          presentNextMonth()
        }
        
        monthView.contentOffset = scrollPointMonthView
      }
      
    }
  }
  
  func jumpMonthViewToDayView(recognizer: UITapGestureRecognizer) {
    var label = recognizer.view as UILabel
    tappedLabelTagMonthView = label.tag
    if label.text != "" {
      tappedMonth = currentMonthName.componentsSeparatedByString("  ")[0]
      tappedYear = currentMonthName.componentsSeparatedByString("  ")[1]
      tappedDate = label.text!
      switch ((label.tag-200)%7) {
      case 0: tappedDay = "SUNDAY"
      case 1: tappedDay = "MONDAY"
      case 2: tappedDay = "TUESDAY"
      case 3: tappedDay = "WEDNESDAY"
      case 4: tappedDay = "THURSDAY"
      case 5: tappedDay = "FRIDAY"
      case 6: tappedDay = "SATURDAY"
      default:
        break
      }
      
      var currentMonthDaysCount = 0
      
      for day in 0...41 {
        if currentMonthLabel[day].text != "" {
          currentMonthDaysCount++
        }
      }
      
      calendarData.currentDateCalculations()
      
      if relativeMonthCounter > 0 {
        relativeCounter = daysMovedForward - calendarData.getDate.toInt()! + tappedDate.toInt()!
      } else if relativeMonthCounter < 0 {
        relativeCounter = daysMovedBackward + (calendarData.currentMonthArray.count - calendarData.getDate.toInt()!) + (tappedDate.toInt()! - currentMonthDaysCount)
      } else {
        relativeCounter = tappedLabelTagMonthView - todayLabelTagMonthView
      }
      
      dayViewSelected(dayViewButton)
      dayViewButton.highlighted = false
    }
  }
  
  func jumpWeekViewToDayView(recognizer: UITapGestureRecognizer) {
    var label = recognizer.view as UILabel
    tappedLabelTagWeekView = label.tag
    
    if label.text != "" {
      println(label.tag - 300)
      if relativeWeekCounter > 0 {
        calendarData.nextWeek(relativeWeekCounter)
        tappedMonth = calendarData.nextWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[1]
        tappedYear = calendarData.nextWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[2]
      } else if relativeWeekCounter < 0 {
        calendarData.previousWeek(relativeWeekCounter)
        tappedMonth = calendarData.previousWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[1]
        tappedYear = calendarData.previousWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[2]
      } else {
        tappedMonth = calendarData.currentWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[1]
        tappedYear = calendarData.currentWeekDeatil[label.tag - 300].componentsSeparatedByString(" ")[2]
      }
      tappedDate = label.text!
      switch ((label.tag-300)%7) {
      case 0: tappedDay = "SUNDAY"
      case 1: tappedDay = "MONDAY"
      case 2: tappedDay = "TUESDAY"
      case 3: tappedDay = "WEDNESDAY"
      case 4: tappedDay = "THURSDAY"
      case 5: tappedDay = "FRIDAY"
      case 6: tappedDay = "SATURDAY"
      default:
        break
      }
      
      relativeCounter = (relativeWeekCounter * 7) + tappedLabelTagWeekView - todayLabelTagWeekView
      dayViewSelected(dayViewButton)
      dayViewButton.highlighted = false
    }
  }
  
  @IBAction func previousMonthDisplay(sender: AnyObject) {
    presentPreviousMonth()
  }
  
  @IBAction func nextMonthDisplay(sender: AnyObject) {
    presentNextMonth()
  }
  
  func refresh() {
    monthNameDisplayView.hidden = true
    timeTableView.hidden = true
    eventTableView.hidden = true
    timeCollectionView.hidden = true
    eventCollectionView.hidden = true
    monthView.hidden = true
    monthWeekDisplayView.hidden = true
    dateDisplayView.hidden = true
    weekView.hidden = true
    weekDisplayView.hidden = true
  }
  
  func refreshWeekDisplayView() {
    var labelDay = UILabel()
    for labelDay in weekDisplayView.subviews {
      labelDay.removeFromSuperview()
    }
    for labelDay in weekView.subviews {
      labelDay.removeFromSuperview()
    }
    for labelDay in monthView.subviews {
      labelDay.removeFromSuperview()
    }
  }
  
  func refreshCalendarType() {
    dayViewButton.backgroundColor = DarkBlueColor
    dayViewButton.highlighted = false
    weekViewButton.backgroundColor = DarkBlueColor
    weekViewButton.highlighted = false
    monthViewButton.backgroundColor = DarkBlueColor
    monthViewButton.highlighted = false
  }
  
  func checkFirstDayIndex() {
    for i in 0...6 {
      switch i {
      case 0:
        firstDayCheck = "Sunday"
      case 1:
        firstDayCheck = "Monday"
      case 2:
        firstDayCheck = "Tuesday"
      case 3:
        firstDayCheck = "Wednesday"
      case 4:
        firstDayCheck = "Thursday"
      case 5:
        firstDayCheck = "Friday"
      case 6:
        firstDayCheck = "Saturday"
      default: break
      }
      if calendarData.firstDayNextMonth == firstDayCheck {
        nextMonthIndex = i
      }
      if calendarData.firstDayPreviousMonth == firstDayCheck {
        previousMonthIndex = i
      }
    }
  }
  
  @IBAction func dayViewSelected(sender: UIButton) {
    refresh()
    refreshCalendarType()
    enableButtonInteractions()
    dayViewButton.userInteractionEnabled = false
    sender.highlighted = true
    sender.backgroundColor = PinkColor
    timeTableView.hidden = false
    eventTableView.hidden = false
    dateDisplayView.hidden = false
    calendarData.currentDateCalculations()
    dateLabel.text = calendarData.getDate
    dayLabel.text = calendarData.getDay
    monthLabel.text = calendarData.getMonth
    yearLabel.text = calendarData.getYear
    
    
    if tappedDate == "" {
      dateLabel.text = calendarData.getDate
      dayLabel.text = calendarData.getDay
      monthLabel.text = calendarData.getMonth
      yearLabel.text = calendarData.getYear
    } else {
      dateLabel.text = tappedDate
      dayLabel.text = tappedDay
      monthLabel.text = tappedMonth
      yearLabel.text = tappedYear
    }
    dayViewDayIndex = calendarData.availabilityDay(dayLabel.text!.lowercaseString)
    dayViewData()
    eventTableView.reloadData()
  }
  
  @IBAction func weekViewSelected(sender: UIButton) {
    refresh()
    refreshCalendarType()
    enableButtonInteractions()
    weekViewButton.userInteractionEnabled = false
    sender.highlighted = false
    sender.backgroundColor = PinkColor
    weekView.hidden = false
    weekDisplayView.hidden = false
    timeCollectionView.hidden = false
    eventCollectionView.hidden = false
  }
  
  @IBAction func monthViewSelected(sender: UIButton) {
    refresh()
    refreshCalendarType()
    enableButtonInteractions()
    monthViewButton.userInteractionEnabled = false
    sender.highlighted = true
    sender.backgroundColor = PinkColor
    if monthViewButtonTapCount == 0 {
      monthViewFunction()
      let startMonthName = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[0]
      let endMonthName = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[0]
      let startYear = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[1]
      let endYear = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[1]
      startDate = dateFormatter("1", month: startMonthName, year: startYear)
      endDate = dateFormatter("\(calendarData.daysInmonth(endMonthName, yearString: endYear))", month: endMonthName, year: endYear)
      getAppointments(startDate, endingDate: endDate, mystylistId: "\(defaultStylistId)")
      monthViewButtonTapCount = 1
    } else {
      weekView.hidden = true
      monthView.hidden = false
      weekDisplayView.hidden = true
      monthWeekDisplayView.hidden = false
      monthNameDisplayView.hidden = false
    }
  }
  
  @IBAction func dateChangeButtonClicked(sender: UIButton) {
    if sender.tag == 30 {
      relativeCounter -= 1
      calendarData.previousDateCalculation(relativeCounter)
      dateLabel.text = calendarData.getDate
      dayLabel.text = calendarData.getDay
      monthLabel.text = calendarData.getMonth
      yearLabel.text = calendarData.getYear
      
    } else if sender.tag == 31 {
      relativeCounter += 1
      calendarData.nextDateCalculation(relativeCounter)
      dateLabel.text = calendarData.getDate
      dayLabel.text = calendarData.getDay
      monthLabel.text = calendarData.getMonth
      yearLabel.text = calendarData.getYear
    }
    
    if relativeCounter == 0 {
      calendarData.currentDateCalculations()
      dateLabel.text = calendarData.getDate
      dayLabel.text = calendarData.getDay
      monthLabel.text = calendarData.getMonth
      yearLabel.text = calendarData.getYear
    }
    dayViewDayIndex = calendarData.availabilityDay(dayLabel.text!.lowercaseString)
    dayViewData()
    eventTableView.reloadData()
  }
  
  func monthNameDisplay() {
    currentMonthNameLabel.text = currentMonthName
    let month = currentMonthName.componentsSeparatedByString("  ")[0]
    let year = currentMonthName.componentsSeparatedByString("  ")[1]
    
    var constH = NSLayoutConstraint(item: monthNameDisplayView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 2 * weekView.frame.height)
    monthNameDisplayView.addConstraint(constH)
    monthNameDisplayView.updateConstraints()
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    if collectionView.tag == 1 {
      return 96 * 7
    } else if collectionView.tag == 2 {
      return 96
    } else {
      return 0
    }
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
    if collectionView.tag == 2 {
      let timeIntervalLabel : UILabel = cell.viewWithTag(102) as UILabel
      timeIntervalLabel.text = timeIntervalString[indexPath.row]
      
      if indexPath.row % 4 == 0 {
        timeIntervalLabel.font = UIFont(name: "Raleway-Bold", size: 18.0)
        timeIntervalLabel.textColor = TimeLabelColor
        
      } else {
        // to be filled with data from server
        timeIntervalLabel.font = UIFont(name: "Raleway", size: 18.0)
        timeIntervalLabel.textColor = TimeLabelColor
      }
    } else {
      cell.backgroundColor = LightGrayColor
      let dayIndex = indexPath.row % 7
      let timeIndex = indexPath.row / 7
      
      if let defaultAvailabilityArray = defaultAvailability["\(dayIndex)"] {
        if defaultAvailabilityArray[timeIndex] == 1 {
          cell.backgroundColor = defaultAvailabilityColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      }
      
      if let dayAppointmentArray = weekAppointmentData["\(dayIndex)"] {
        if dayAppointmentArray[timeIndex] == 1 {
          if relativeWeekCounter < 0 {
            cell.backgroundColor = LightMehronColor
          } else {
            if relativeWeekCounter > 0 {
              cell.backgroundColor = MehronColor
            } else {
              println(weekLabel[dayIndex + 7].tag)
              println(todayLabelTagWeekView)
              if weekLabel[dayIndex + 7].tag < todayLabelTagWeekView {
                cell.backgroundColor = LightMehronColor
              } else if weekLabel[dayIndex + 7].tag == todayLabelTagWeekView {
                cell.backgroundColor = eventColor(timeIndex, weekDay: dayIndex + 7)
              } else {
                cell.backgroundColor = MehronColor
              }
            }
          }
          
        }
      }
    }
    
    return cell
  }
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.tag == 1 {
      var otherScrollView = self.timeCollectionView
      otherScrollView.contentOffset = scrollView.contentOffset
    }
    
    if scrollView.tag == 3 {
      var otherScrollView = self.timeTableView
      otherScrollView.contentOffset = scrollView.contentOffset
    }
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    var sideLength : CGFloat = (weekView.frame.width - 8) / 7
    if collectionView.tag == 2 {
      return CGSizeMake(collectionView.frame.width, 43)
    } else if collectionView.tag == 1 {
      return CGSizeMake(sideLength, 42)
    } else {
      return CGSizeMake(sideLength, 42)
    }
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 96
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if tableView.tag == 4 {
      let cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
      let timeIntervalLabel : UILabel = cell.viewWithTag(101) as UILabel
      timeIntervalLabel.text = timeIntervalString[indexPath.row]
      if indexPath.row % 4 == 0 {
        timeIntervalLabel.font = UIFont(name: "Raleway-Bold", size: 18.0)
        timeIntervalLabel.textColor = TimeLabelColor
      } else {
        timeIntervalLabel.font = UIFont(name: "Raleway", size: 18.0)
        timeIntervalLabel.textColor = TimeLabelColor
      }
      
      return cell
    } else {
      let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
      cell.backgroundColor = LightGrayColor
      
      if let defaultAvailabilityArray = defaultAvailability[dayViewDayIndex] {
        if defaultAvailabilityArray[indexPath.row] == 1 {
          cell.backgroundColor = defaultAvailabilityColor
        } else {
          cell.backgroundColor = LightGrayColor
        }
      }
      
      cell.textLabel?.font = UIFont(name: "Raleway", size: 18.0)
      cell.textLabel?.textColor = UIColor.whiteColor()
      cell.textLabel?.text = tableViewClientNameArray[indexPath.row]
      if tableViewClientNameArray[indexPath.row] != "" {
        //cell.textLabel?.text = tableViewClientNameArray[indexPath.row]
        //cell.detailTextLabel?.text = tableViewServiceDetailsArray[indexPath.row]
        cell.backgroundColor = eventColor(indexPath.row, weekDay: 0)
      } else {
        if eventBlockedState[indexPath.row] == 1 {
          cell.backgroundColor = LightMehronColor
        }
      }
      
      cell.tag = 400 + indexPath.row
      let recognizerSwipeRight = UISwipeGestureRecognizer(target: self, action: "swipeRight:")
      recognizerSwipeRight.direction = UISwipeGestureRecognizerDirection.Right
      cell.addGestureRecognizer(recognizerSwipeRight)
      let recognizerSwipeLeft = UISwipeGestureRecognizer(target: self, action: "swipeLeft:")
      recognizerSwipeLeft.direction = UISwipeGestureRecognizerDirection.Left
      cell.addGestureRecognizer(recognizerSwipeLeft)
      return cell
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if tableView.tag == 3{
      if dateLabel.text == calendarData.getDate && dayLabel.text == calendarData.getDay && monthLabel.text == calendarData.getMonth && yearLabel.text == calendarData.getYear {
        let hour = calendarData.currentTime.componentsSeparatedByString(":")[0].toInt()!
        let minute = calendarData.currentTime.componentsSeparatedByString(":")[1].toInt()!
        let currentTime : Int = 100 * hour + minute
        calendarData.timeIntervalArrayIn24HourFormat()
        if calendarData.timeArray[indexPath.row] >= currentTime {
          addAppointmentTime = timeIntervalString[indexPath.row]
          formattedAppointmentTime = calendarData.timeArray[indexPath.row]
          addAppointment()
        }
      } else if relativeCounter > 0 || relativeMonthCounter > 0 || relativeWeekCounter > 0 || tappedLabelTagWeekView > todayLabelTagWeekView || tappedLabelTagMonthView > todayLabelTagMonthView {
        addAppointmentTime = timeIntervalString[indexPath.row]
        formattedAppointmentTime = calendarData.timeArray[indexPath.row]
        addAppointment()
        //tableViewClientNameArray[indexPath.row] = "New Appointment"
        //tableViewServiceDetailsArray[indexPath.row] = "Service (\(timeIntervalString[indexPath.row]))"
      }
    }
    eventTableView.reloadData()
  }
  
  func swipeRight(recognizer: UISwipeGestureRecognizer) {
    let cell = recognizer.view as UITableViewCell
    if cell.textLabel?.text == "" {
      eventBlockedState[cell.tag - 400] = 1
      cell.backgroundColor = LightMehronColor
    }
    
    eventTableView.reloadData()
  }
  
  func swipeLeft(recognizer: UISwipeGestureRecognizer) {
    let cell = recognizer.view as UITableViewCell
    if cell.textLabel?.text != "" {
      cell.backgroundColor = LightGrayColor
      getAppointmentId()
      let appointmentId = appointmentIdArray[cell.tag - 400]
      println(appointmentId)
      tableViewServiceDetailsArray[cell.tag - 400] = ""
      var arr_parameters = NSDictionary(objectsAndKeys:
        AccessToken,"access_token","\(appointmentId)","appointment_id","0","flag")
      dispatch_async(dispatch_get_main_queue()){
        json.session("cancel_appointment", parameters: arr_parameters, completion: {
          response in
          let data = response as NSDictionary
          for i in 0...95 {
            if self.appointmentIdArray[i] == appointmentId {
              self.appointmentIdArray[i] = ""
              self.tableViewClientNameArray[i] = ""
            }
          }
          //dispatch_async(dispatch_get_main_queue()){
          
            //self.eventTableView.reloadData()
          //}
        })
      }
      
      getAppointments(self.callStartTime, endingDate: self.callStopTime, mystylistId: "\(self.defaultStylistId)")
    }
    
    if eventBlockedState[cell.tag - 400] == 1 {
      eventBlockedState[cell.tag - 400] = 0
    }
    
    eventTableView.reloadData()
  }
  
  func enableButtonInteractions() {
    monthViewButton.userInteractionEnabled = true
    dayViewButton.userInteractionEnabled = true
    weekViewButton.userInteractionEnabled = true
  }
  
  func timeIntervalForAppointments() {
    var hours = 12
    for hour in 0...23 {
      for minute in 0...3 {
        if minute == 0 && hour < 12 {
          timeIntervalString.append("\(hours) AM")
        } else if minute == 0 && hour >= 12 {
          timeIntervalString.append("\(hours) PM")
        } else {
          timeIntervalString.append("\(hours):\(minute * 15)")
        }
      }
      
      hours++
      if hours == 13 {
        hours = 1
      }
    }
  }
  
  @IBAction func homeButtonPressed(sender: AnyObject) {
    println("home")
  }
  
  func addAppointment() {
    addAppointmentDate = monthLabel.text! + " " + dateLabel.text! + "," + yearLabel.text!
    formattedAppointmentdate = calendarData.appointmentDate(dateLabel.text!, month: monthLabel.text!, year: yearLabel.text!)
    let othstoryboard = UIStoryboard(name: "Main", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("addAppointment") as AddAppointmentViewController
    destinationView.displayDate = addAppointmentDate
    destinationView.currentDate = formattedAppointmentdate
    destinationView.currentTime = formattedAppointmentTime
    destinationView.displayTime = addAppointmentTime
    destinationView.selectedStylistId = defaultStylistId
    self.navigationController!.pushViewController(destinationView, animated: true)
  }
  
  @IBAction func backButtonPressed(sender: AnyObject) {
    let othstoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
    let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("viewAppointment") as ViewAppointmentViewController
    self.navigationController!.popViewControllerAnimated(true)
  }
  
  func dateFormatter(date : String, month : String, year : String) -> String {
    var formattedDate = String()
    var monthNumber = ""
    if monthFormatter(month) < 10 {
      monthNumber = "0\(monthFormatter(month))"
    } else {
      monthNumber = "\(monthFormatter(month))"
    }
    
    if date.toInt()! < 10 {
      formattedDate = "0\(date)"
    } else {
      formattedDate = date
    }
    
    return "\(year)-\(monthNumber)-\(formattedDate)"
    
  }
  
  func monthFormatter(month : String) -> Int {
    var monthNumber = 0
    switch month {
    case "January" : monthNumber = 1
    case "February" : monthNumber = 2
    case "March" : monthNumber = 3
    case "April" : monthNumber = 4
    case "May" : monthNumber = 5
    case "June" : monthNumber = 6
    case "July" : monthNumber = 7
    case "August" : monthNumber = 8
    case "September" : monthNumber = 9
    case "October" : monthNumber = 10
    case "November" : monthNumber = 11
    case "December" : monthNumber = 12
    default: break
    }
    
    return monthNumber
  }
  
  func startLoading() {
    activityBarView.hidden = false
    activityBar.startAnimating()
  }
  
  func stopLoading() {
    activityBar.stopAnimating()
    activityBarView.hidden = true
  }
  
  func displayMonthAppointments(month: String, year: String) {
    
    var formattedDate = ""
    for i in 0...41 {
      if currentMonthLabel[i].text != "" {
        let date = currentMonthLabel[i].text!.toInt()!
        formattedDate = calendarData.appointmentDate("\(date)", month: month, year: year)
        println(formattedDate)
        if appointmentsOnDate[formattedDate] != nil {
          if relativeMonthCounter < 0 || date < calendarData.todayDate.toInt() {
            currentMonthLabel[i].backgroundColor = LightMehronColor
          } else {
            currentMonthLabel[i].backgroundColor = MehronColor
          }
          currentMonthLabel[i].textColor = LightGrayColor
        } else {
          currentMonthLabel[i].backgroundColor = LightGrayColor
          currentMonthLabel[i].textColor = DarkBlueColor
        }
        
        
      }
    }
  }
  
  func eventColor(index: Int, weekDay: Int) -> UIColor {
    if dateLabel.text == calendarData.getDate && dayLabel.text == calendarData.getDay && monthLabel.text == calendarData.getMonth && yearLabel.text == calendarData.getYear || weekLabel[weekDay].tag == todayLabelTagWeekView {
      let hour = calendarData.currentTime.componentsSeparatedByString(":")[0].toInt()!
      let minute = calendarData.currentTime.componentsSeparatedByString(":")[1].toInt()!
      var currentTime : Int = 100 * hour + minute
      if calendarData.currentMeridiem == "AM" {
        if hour == 0 {
          currentTime = 100 * 0 + minute
        } else {
          currentTime = 100 * hour + minute
        }
        
      } else {
        if hour == 12 {
          currentTime = 100 * hour + minute
        } else {
          currentTime = 100 * (hour + 12) + minute
        }
      }
      
      calendarData.timeIntervalArrayIn24HourFormat()
      if calendarData.timeArray[index] >= currentTime {
        return MehronColor
      } else {
        return LightMehronColor
      }
    } else if relativeCounter > 0 || relativeMonthCounter > 0 || relativeWeekCounter > 0 || tappedLabelTagWeekView > todayLabelTagWeekView || tappedLabelTagMonthView > todayLabelTagMonthView {
      return MehronColor
    } else {
      return LightMehronColor
    }
  }
  
  func weekCall() {
    for i in 7...13 {
      if weekLabel[i].text! == "1" {
        if relativeWeekCounter > 0 {
          monthChangeInWeekView += 1
        } else if relativeWeekCounter == 0 {
          monthChangeInWeekView = 1
        } else {
          monthChangeInWeekView -= 1
        }
        
        
        if monthChangeInWeekView % 2 == 0 {
          calendarData.previousMonth(monthChangeInWeekView - 1)
          calendarData.nextMonth(monthChangeInWeekView + 1)
          let startMonthName = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[0]
          let endMonthName = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[0]
          let startYear = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[1]
          let endYear = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[1]
          startDate = dateFormatter("1", month: startMonthName, year: startYear)
          endDate = dateFormatter("\(calendarData.daysInmonth(endMonthName, yearString: endYear))", month: endMonthName, year: endYear)
          getAppointments(startDate, endingDate: endDate, mystylistId: "\(defaultStylistId)")
        }
      } else if firstCall == 0 {
        calendarData.previousMonth(monthChangeInWeekView - 1)
        calendarData.nextMonth(monthChangeInWeekView + 1)
        let startMonthName = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[0]
        let endMonthName = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[0]
        let startYear = calendarData.previousMonthNameString.componentsSeparatedByString("  ")[1]
        let endYear = calendarData.nextMonthNameString.componentsSeparatedByString("  ")[1]
        startDate = dateFormatter("1", month: startMonthName, year: startYear)
        endDate = dateFormatter("\(calendarData.daysInmonth(endMonthName, yearString: endYear))", month: endMonthName, year: endYear)
        if defaultStylistId != -1 {
          getAppointments(startDate, endingDate: endDate, mystylistId: "\(defaultStylistId)")
        }
        
        firstCall = 1
      }
    }
  }
  
  func dayViewData() {
    for i in 0...95 {
      tableViewClientNameArray[i] = ""
    }
    if monthAppointmentArray.count != 0 {
      let date : String = calendarData.appointmentDate(dateLabel.text!, month: monthLabel.text!, year: yearLabel.text!)
      println(date)
      if let todayAppointmentArray = appointmentsOnDate[date] {
        if todayAppointmentArray.count != 0 {
          for appointments in todayAppointmentArray {
            let startTime = appointments["appointment_start_time"]! as String
            let stopTime = appointments["appointment_end_time"]! as String
            let startIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).startIndex
            let stopIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).stopIndex
            let length = calendarData.appointmentInfo(startTime, stopTime: stopTime).length
            for i in 0...95 {
              if startIndex <= i && i <= stopIndex {
                if startIndex == i {
                  tableViewClientNameArray[i] = appointments["customer_name"]! as String
                } else {
                  tableViewClientNameArray[i] = " "
                }
                
                if tableViewClientNameArray[i] == "" {
                  tableViewClientNameArray[i] = "Unknown"
                }
              }
            }
          }
        }
      }
    }
    println(tableViewClientNameArray)
    eventTableView.reloadData()
  }
  
  func getAppointmentId() {
    if monthAppointmentArray.count != 0 {
      let date : String = calendarData.appointmentDate(dateLabel.text!, month: monthLabel.text!, year: yearLabel.text!)
      if let todayAppointmentArray = appointmentsOnDate[date] {
        if todayAppointmentArray.count != 0 {
          for appointments in todayAppointmentArray {
            let startTime = appointments["appointment_start_time"]! as String
            let stopTime = appointments["appointment_end_time"]! as String
            let startIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).startIndex
            let stopIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).stopIndex
            let length = calendarData.appointmentInfo(startTime, stopTime: stopTime).length
            for i in 0...95 {
              if startIndex <= i && i <= stopIndex {
                let id = appointments["appointment_id"]! as Int
                appointmentIdArray[i] = "\(id)"
              }
            }
          }
        }
      }
    }
  }
  
  func weekViewData() {
    
    var emptyArray = [Int]()
    for i in 0...95 {
      emptyArray.append(0)
    }
    
    weekAppointmentData["0"] = emptyArray
    weekAppointmentData["1"] = emptyArray
    weekAppointmentData["2"] = emptyArray
    weekAppointmentData["3"] = emptyArray
    weekAppointmentData["4"] = emptyArray
    weekAppointmentData["5"] = emptyArray
    weekAppointmentData["6"] = emptyArray
    
    calendarData.nextWeek(relativeWeekCounter)
    let weekArray = calendarData.nextWeekDeatil
    for index in 0...6 {
      let day = weekArray[index].componentsSeparatedByString(" ")[0]
      let month = weekArray[index].componentsSeparatedByString(" ")[1]
      let year = weekArray[index].componentsSeparatedByString(" ")[2]
      let date = calendarData.appointmentDate(day, month: month, year: year)
      //println(date)
      
      if monthAppointmentArray.count != 0 {
        if let todayAppointmentArray = appointmentsOnDate[date] {
          if todayAppointmentArray.count != 0 {
            for appointments in todayAppointmentArray {
              let startTime = appointments["appointment_start_time"]! as String
              let stopTime = appointments["appointment_end_time"]! as String
              let startIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).startIndex
              let stopIndex = calendarData.appointmentInfo(startTime, stopTime: stopTime).stopIndex
              let length = calendarData.appointmentInfo(startTime, stopTime: stopTime).length
              var dayArray = weekAppointmentData["\(index)"]!
              
              for i in 0...95 {
                if startIndex <= i && i <= stopIndex {
                  //if startIndex == i {
                  dayArray[i] = 1
                  //}
                }
              }
              
              weekAppointmentData["\(index)"] = dayArray
            }
          }
        }
      }
    }
    println("here")
    eventCollectionView.reloadData()
  }
  
  func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // number of rows in picker
    return stylistNames.count
  }
  
  func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 60
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var pickerLabel = view as UILabel!
    if view == nil {
      pickerLabel = UILabel()
      pickerLabel.font = UIFont(name: "Raleway", size: 22.0)
      pickerLabel.textColor = PinkColor
      pickerLabel.textAlignment = .Center
    }
    if stylistNames.count != 0 {
      pickerLabel.text = "\(stylistNames[row])"
    }
    
    return pickerLabel
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if stylistId.count != 0 {
      defaultStylistId = stylistId[row]
      stylistPickerView.hidden = true
      getAppointments(callStartTime, endingDate: callStopTime, mystylistId: "\(defaultStylistId)")
    }
  }
  
  
  
  @IBAction func selectStylist(sender: AnyObject) {
    if stylistId.count != 0 {
      stylistPickerView.hidden = false
    }
  }
  @IBAction func moreButtonPressed(sender: AnyObject) {
    
    let alertController = UIAlertController(title: "Error", message: "Are you sure you want to logout?", preferredStyle: .Alert)
    let OkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) in
      
      logout(self)
      
    }
    let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (action) in
      return
    }
    alertController.addAction(OkAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true) {
    }
    
  }
  
}