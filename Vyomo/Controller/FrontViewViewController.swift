import UIKit

class FrontViewViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var frontImagesCollectionView: UICollectionView!
  
  var imageArray = [UIImage]()
  var visibleImageViews = [UIImageView]()
  var deleteButtonArray = [UIButton]()
  var deleteCountPressed = 0
  var titleString  = String()
  var picker:UIImagePickerController? = UIImagePickerController()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    picker?.delegate = self
    titleLabel.text = titleString
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //println(imageArray.count)
    if imageArray.count == 0 {
      return 1
    } else {
      return imageArray.count
    }
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("frontViewCell", forIndexPath: indexPath) as UICollectionViewCell
    
    if imageArray.count != 0 {
      if let deleteButton = cell.viewWithTag(850) as? UIButton {
        deleteButtonArray.append(deleteButton)
        deleteButtonArray[indexPath.row].addTarget(self, action: "deletePicture:", forControlEvents: UIControlEvents.TouchUpInside)
      }
      
      if let imageView = cell.viewWithTag(800) as? UIImageView {
        imageView.backgroundColor = UIColor.clearColor()
        imageView.contentMode = UIViewContentMode.ScaleToFill
        imageView.image = imageArray[indexPath.row]
        visibleImageViews.append(imageView)
      }
    } else {
      if let imageView = cell.viewWithTag(800) as? UIImageView {
        imageView.backgroundColor = FrontViewPlaceholderBackgroundColor
        imageView.contentMode = UIViewContentMode.Center
        imageView.image = UIImage(named: "placeholder_front_view_1x.png")
      }
    }
    
    return cell
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    var sideLength : CGFloat = (self.view.bounds.width - 39) / 2
    return CGSizeMake(sideLength, sideLength)
  }
  
  func deletePicture(sender : UIButton) {
    for buttonIndex in 0..<deleteButtonArray.count {
      if deleteButtonArray[buttonIndex] == sender {
        imageArray.removeAtIndex(buttonIndex)
        break
      }
    }
    
    deleteButtonArray = []
    visibleImageViews = []
    frontImagesCollectionView.reloadData()
  }
  
  func loadImages() {
    // load images from server
  }

  @IBAction func addMoreImages(sender: AnyObject) {
    let addImageAlertView = UIAlertView()
    addImageAlertView.delegate = self
    addImageAlertView.title = "Add new Image"
    addImageAlertView.addButtonWithTitle("Gallery")
    addImageAlertView.addButtonWithTitle("Camera")
    addImageAlertView.addButtonWithTitle("Cancel")
    addImageAlertView.show()
  }
  
  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    if buttonIndex == 0 {
      openGallery()
    } else if buttonIndex == 1 {
      openCamera()
    }
  }
  
  func openGallery() {
    picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
    self.presentViewController(picker!, animated: true, completion: nil)
  }
  
  func openCamera() {
    // if the device has camera provision open it otherwise open gallery
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
      picker!.sourceType = UIImagePickerControllerSourceType.Camera
      self .presentViewController(picker!, animated: true, completion: nil)
    } else {
      openGallery()
    }
  }
  
  func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]!) {
    // save the image in the global variable and display the selected image
    picker .dismissViewControllerAnimated(true, completion: nil)
    var pickedImage  = info[UIImagePickerControllerOriginalImage] as UIImage
    imageArray.append(pickedImage)
    visibleImageViews = []
    deleteButtonArray = []
    frontImagesCollectionView.reloadData()
  }
  
  @IBAction func backButtonPressed(sender: AnyObject) {
    let n: Int! = self.navigationController?.viewControllers?.count
    let prevViewController = self.navigationController?.viewControllers[n-2] as AddSaloonInfoViewController
    //prevViewController.imageArray = imageArray
    if titleString == "Front View" {
      prevViewController.front_view_images_count = imageArray.count
      prevViewController.frontImageArray = imageArray
    } else if titleString == "Inside View" {
      prevViewController.inside_view_images_count = imageArray.count
      prevViewController.insideImageArray = imageArray
    } else if titleString == "Stylist Photo" {
      prevViewController.side_view_images_count = imageArray.count
      prevViewController.sideImageArray = imageArray
    }
    
    self.navigationController!.popViewControllerAnimated(true)
    
    
  }
}
