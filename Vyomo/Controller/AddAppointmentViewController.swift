//
//  AddAppointmentViewController.swift
//  Vyomo
//
//  Created by Bhasker on 3/20/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//
//

//31 and 32 tags used
import UIKit

class AddAppointmentViewController: UIViewController, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate {
  
    @IBOutlet weak var activityView: UIActivityIndicatorView!
  @IBOutlet weak var tableHeight: NSLayoutConstraint!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var customerNameLabel: UILabel!
  var customerName = "Customer"
  var customerButtonName = "Select Customer"
  
  @IBOutlet weak var taxTextField: UITextField!
  
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  var displayDate = String()
  var currentTime = Int()
  var displayTime = String()
  var currentDate = String()
    var timeString = String()
    var customerId = 0
  
  var selectedStylistId = 0
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  @IBOutlet weak var selectCustomerLabel: UILabel!
  @IBOutlet weak var selectServicesLabel: UILabel!
  
  @IBOutlet weak var noteTextView: UITextView!
  @IBOutlet weak var characterCountLabel: UILabel!
  
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var discountLabel: UILabel!
  @IBOutlet weak var costLabel: UILabel!
  @IBOutlet weak var productCostLabel: UILabel!
  @IBOutlet weak var totalAmountLabel: UILabel!
  @IBOutlet weak var taxLabel: UILabel!
  
  
  @IBOutlet weak var weekPickerView: UIPickerView!
  var weekPickerArray = ["1 week", "2 week", "3 week"]
  @IBOutlet weak var weekLabel: UILabel!
  
  @IBOutlet weak var timesPickerView: UIPickerView!
  var timesPickerArray = ["1", "2", "3"]
  @IBOutlet weak var timesLabel: UILabel!
    //var selectState = [Int]()

  var selectedService = [String]()
  
  var selectedBusinessServiceId = [Int]()
  var selectedServiceDuration = [Int]()
  var selectedServiceProductCost = [Int]()
  var selectedServiceCost = [Int]()
  var selectedIndexArray = [Int]()
  
  var selectedId = ""
  var selectedDuration = 0
  var selectedProductCost = 0
  var selectedCost = 0
    var businessServiceId = ""

  @IBAction func durationDecrease(sender: AnyObject) {
    if var duration = durationLabel.text?.toInt() {
      if duration != 0 {
        duration = duration - 15
        durationLabel.text = "\(duration)"
      }
    }
  }
  
  @IBAction func durationIncrease(sender: AnyObject) {
    if var duration = durationLabel.text?.toInt() {
      duration = duration + 15
      durationLabel.text = "\(duration)"
    }
    
  }
  
  @IBAction func discountDecrease(sender: AnyObject) {
    if var discount = discountLabel.text?.toInt() {
      if discount != 0 {
        discount = discount - 5
        discountLabel.text = "\(discount)"
        let total = ((selectedProductCost + selectedCost)*(100-((discountLabel.text?.toInt())!%100)))/100
        //totalAmountLabel.text = "\((total*(100+(taxTextField.text?.toInt())!))/100)"
      }
    }
    
  }
  
  @IBAction func discountIncrese(sender: AnyObject) {
    if var discount = discountLabel.text?.toInt() {
      if discount != 100 {
        discount = discount + 5
        discountLabel.text = "\(discount)"
        let total = ((selectedProductCost + selectedCost)*(100-((discountLabel.text?.toInt())!%100)))/100
        //totalAmountLabel.text = "\((total*(100+(taxTextField.text?.toInt())!))/100)"
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    var tapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewTapped")
    scrollView.addGestureRecognizer(tapRecognizer)
  }
  
  func scrollViewTapped() {
    //noteTextView.resignFirstResponder()
    self.view.endEditing(true)
    //weekPickerView.hidden = true
    //timesPickerView.hidden = true
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    noteTextView.resignFirstResponder()
    //weekPickerView.hidden = true
    //timesPickerView.hidden = true
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    registerForKeyboardNotifications()
    customerNameLabel.text = customerName
    selectCustomerLabel.text = customerButtonName
    if currentTime / 100 < 12 {
        displayTime = displayTime.componentsSeparatedByString(" ")[0]
        displayTime += " AM"
    } else {
        displayTime = displayTime.componentsSeparatedByString(" ")[0]
        displayTime += " PM"
    }
    if currentTime / 100 < 10 {
        //timeString = displayTime.componentsSeparatedByString(":")[1]
        timeString = "0"+"\(currentTime/100)"+":"+"\(currentTime%100)"
    } else {
        //timeString = displayTime.componentsSeparatedByString(":")[1]
      timeString = "\(currentTime/100)"+":"+"\(currentTime%100)"
    }
    
    timeLabel.text = displayTime
    dateLabel.text = displayDate
    if selectedService.count != 0 {
      selectServicesLabel.text = "Add More"
    } else {
        selectServicesLabel.text = "Select Service"
    }
    businessServiceId = selectedId
    tableView.reloadData()
    durationLabel.text = "\(selectedDuration)"
    productCostLabel.text = "\(selectedProductCost)"
    costLabel.text = "\(selectedCost)"
    let total = ((selectedProductCost + selectedCost)*(100-((discountLabel.text?.toInt())!%100)))/100
    //totalAmountLabel.text = "\((total*(100+(taxTextField.text?.toInt())!))/100)"
    tableHeight.constant = tableView.contentSize.height
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func scheduleWeekClicked(sender: AnyObject) {
    //weekPickerView.hidden = false
  }
  
  @IBAction func scheduleTimeClicked(sender: AnyObject) {
    //timesPickerView.hidden = false
  }
  
  @IBAction func selectService(sender: AnyObject) {
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("service") as SelectServicesViewController
    //destinationView.selectedService = selectedService
    //destinationView.selectedBusinessServiceId = selectedBusinessServiceId
    destinationView.selectedIndexArray = selectedIndexArray
    //destinationView.selectState = selectState
    self.navigationController!.pushViewController(destinationView, animated: true)
    
  }
  
  @IBAction func selectCustomer(sender: AnyObject) {
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("customer") as SelectCustomerViewController
    self.navigationController!.pushViewController(destinationView, animated: true)
    
  }
  
  @IBAction func backButtonPressed(sender: AnyObject) {
    self.navigationController?.popViewControllerAnimated(true)
  }
  
  //var businessId = 1001
  //var stylistId = 1
  
  
  @IBAction func addAppointment(sender: AnyObject) {
    //var tax = taxLabel.text?.stringByReplacingOccurrencesOfString("%", withString: "")
    
    for (index,element) in enumerate(selectedBusinessServiceId) {
      selectedId = selectedId + "\(element)"
      if index != selectedBusinessServiceId.count-1 {
        selectedId += ","
      }
    }
    
    if customerButtonName == "Select Customer" {
        showAlert("Please select a customer")
        return
    }
    
    if selectServicesLabel.text == "Select Service" {
        showAlert("Please select a service")
        return
    }
    

    
    var json = JSON()
    let null = NSNull()
    var arr_parameters = NSDictionary(objectsAndKeys:
      "\(AccessToken)", "access_token",
      "\(BuisnessIdForRegistration)", "business_id",
      "\(customerId)", "customer_id",
      "\(selectedStylistId)", "stylist_id",
      "\(noteTextView.text!)", "notes",
      "\(costLabel.text!)", "cost",
      "\(productCostLabel.text!)", "product_cost",
      "\(discountLabel.text!)", "discount",
      "\(taxTextField.text!.toInt()!)", "tax",
      "\(totalAmountLabel.text!)", "total_amount",
      "\(currentDate)", "current_time",//
      "\(durationLabel.text!)", "duration",//
        "\(currentDate) \(timeString):00", "appointment_start_time",
      "\(currentDate) \(timeString):00", "appointment_end_time",
      "0", "recurring_status",
      "\(selectedId)", "business_service_id",
      "0", "recurring_week",
      "0", "recurring_times" )
    
    println(arr_parameters)
    activityView.hidden = false
    activityView.startAnimating()
    json.session("add_appointments", parameters: arr_parameters, completion: { result in
        
        //println(result)
        self.activityView.stopAnimating()
        if let resultArray = result["data"] as? String {
            //println(resultArray)
            /*let mainStoryboard = UIStoryboard(name: "AppointmentStoryboard", bundle: nil)
            let destinationView = mainStoryboard.instantiateViewControllerWithIdentifier("calendar") as SelectCategoryViewController
          */
          self.navigationController?.popViewControllerAnimated(true)
        } else {
            
            self.showAlert("Add Appointment not successful. Try Again.")
        }
    })
    
  }
  
    func showAlert(message : String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let OkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action) in
            return        }
        alertController.addAction(OkAction)
        self.presentViewController(alertController, animated: true) {
        }
    }

  //MARK: -Picker view implemented
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView == weekPickerView {
      return weekPickerArray.count
    } else {
      return timesPickerArray.count
    }
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var pickerLabel = view as UILabel!
    if view == nil {
      pickerLabel = UILabel()
      pickerLabel.font = UIFont(name: "Raleway", size: 18.0)
      pickerLabel.textColor = UIColor(red: 227/255, green: 20/255, blue: 89/255, alpha: 1)
      pickerLabel.textAlignment = .Center
    }
    
    if pickerView == weekPickerView {
      pickerLabel.text = weekPickerArray[row]
    } else {
      pickerLabel.text = timesPickerArray[row]
    }
    
    return pickerLabel
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if pickerView == weekPickerView {
      weekLabel.text = weekPickerArray[row]
    } else {
      timesLabel.text = timesPickerArray[row]
    }
  }
  
  //MARK: - Text View Delegate
  
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    characterCountLabel.text = "\(range.location)"
    return true
  }
  
  //MARK: - Keyboard Management Methods
  
  func registerForKeyboardNotifications() {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    notificationCenter.addObserver(self,
      selector: "keyboardWillBeShown:",
      name: UIKeyboardWillShowNotification,
      object: nil)
    notificationCenter.addObserver(self,
      selector: "keyboardWillBeHidden:",
      name: UIKeyboardWillHideNotification,
      object: nil)
  }
  
  func keyboardWillBeShown(sender: NSNotification) {
    let info: NSDictionary = sender.userInfo!
    let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
    let keyboardSize: CGSize = value.CGRectValue().size
    let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
    scrollView.contentInset = contentInsets
    scrollView.scrollIndicatorInsets = contentInsets
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    var aRect: CGRect = self.view.frame
    aRect.size.height -= keyboardSize.height
    let noteTextviewRect: CGRect? = noteTextView.frame
    println(noteTextView.frame)
    let noteTextviewOrigin: CGPoint? = noteTextviewRect?.origin
    if (!CGRectContainsPoint(aRect, noteTextviewOrigin!)) {
      scrollView.scrollRectToVisible(noteTextviewRect!, animated:true)
    }
  }
  
  func keyboardWillBeHidden(sender: NSNotification) {
    let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
    scrollView.contentInset = contentInsets
    scrollView.scrollIndicatorInsets = contentInsets
  }
  
  // MARK: - table view implementation
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    //tableHeight.constant = tableView.contentSize.height
    println(selectedService.count)
    return selectedService.count
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as SelectedServicesTableViewCell
    //cell.smallView.hidden = false
    cell.titleLabel.text = selectedService[indexPath.row]
    cell.deleteButton.addTarget(self, action: "deleteButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
    
    return cell
  }
  
  func deleteButtonClicked(sender : UIButton) {
    var position: CGPoint = sender.convertPoint(CGPointZero, toView: self.tableView)
    if let indexPath = self.tableView.indexPathForRowAtPoint(position)
    {
      selectedService.removeAtIndex(indexPath.row)
      selectedBusinessServiceId.removeAtIndex(indexPath.row)
      selectedIndexArray.removeAtIndex(indexPath.row)
      
      tableView.reloadData()
      tableHeight.constant = tableView.contentSize.height
    }
  }
  
  @IBAction func moreButtonPressed(sender: AnyObject) {
    
    let alertController = UIAlertController(title: "Error", message: "Are you sure you want to logout?", preferredStyle: .Alert)
    let OkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) in
      
      logout(self)
      
    }
    let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (action) in
      return
    }
    alertController.addAction(OkAction)
    alertController.addAction(cancelAction)
    
    self.presentViewController(alertController, animated: true) {
    }
    
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
