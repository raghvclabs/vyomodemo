//
//  Transition.swift
//  Vyomo
//
//  Created by Click Labs on 3/23/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//
import UIKit

class Transition : UIViewController , UIPopoverControllerDelegate {
  func transitionForward(sourceViewController : UIViewController,destViewController: UIViewController){
    var transition = CATransition()
    transition.duration = 0.7
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromRight
    sourceViewController.view.window?.layer.addAnimation(transition, forKey: nil)
    sourceViewController.modalPresentationStyle = UIModalPresentationStyle.FullScreen
    sourceViewController.modalInPopover = true
    sourceViewController.presentViewController(destViewController, animated: false, completion: nil)
  }
  
  func transitionBackward(sourceViewController : UIViewController,destViewController: UIViewController){
    var transition = CATransition()
    transition.duration = 0.7
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromLeft
    sourceViewController.view.window?.layer.addAnimation(transition, forKey: nil)
    sourceViewController.presentViewController(destViewController, animated: false, completion: nil)
  }
}


