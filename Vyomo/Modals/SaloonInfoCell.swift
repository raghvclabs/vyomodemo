//
//  SaloonInfoCell.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit


class SaloonInfoCell: UITableViewCell , UIPickerViewDelegate , UIPickerViewDataSource  {
  var cellFrame: CGRect!
  let titleLabel: UILabel!
  let subTitleLabel: UILabel!
  let midsubTitleLabel: UILabel!
  let textInputField: UITextField!
  let countryCodeLabel: UILabel!
  let phoneNumberField: UITextField!
  let countryCodeButton: UIButton!
  let selectButton:UIButton!
   let addressButton:UIButton!
  let selectLabel: UILabel!
  let descriptionTextArea: UITextView!
  let salonPhotoOne : UIImageView!
 let salonPhotoTwo : UIImageView!
  let salonPhotoThree : UIImageView!
  let salonPhotoFour : UIImageView!
  let phoneSeparator: UIView!
  let countryCodePicker: UIPickerView!
 
  var height = CGFloat(250)
  var countryCodes = [ "+91", "+11", "+01", "+44" ]
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
    super.init(coder: aDecoder)

  }
  
override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
   super.init(style: style, reuseIdentifier: reuseIdentifier)
  
     self.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width,  76)
  selectionStyle = UITableViewCellSelectionStyle.None
  
  
      
  let textFrame = CGRect(x:26.66, y: 13, width:286, height: 25)
    titleLabel = UILabel(frame: textFrame)
    titleLabel.text = "test"
    titleLabel.textAlignment = NSTextAlignment.Natural
    titleLabel.font = (UIFont(name: "raleway", size: 18))
    titleLabel.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
    titleLabel.numberOfLines = 3
    titleLabel.hidden = false
  
    contentView.addSubview(titleLabel)

    let subTitleFrame = CGRect(x:26.66, y: 55, width:250, height: 25)
    subTitleLabel = UILabel(frame: subTitleFrame)
    subTitleLabel.text = ""
    subTitleLabel.textAlignment = NSTextAlignment.Natural
    subTitleLabel.font = (UIFont(name: "raleway", size: 18))
    subTitleLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    subTitleLabel.hidden = true
    contentView.addSubview(subTitleLabel)
  
  let middleframe = CGRect(x:26.66, y: 29, width:250, height: 25)
  midsubTitleLabel = UILabel(frame: middleframe)
  midsubTitleLabel.text = ""
  midsubTitleLabel.textAlignment = NSTextAlignment.Natural
  midsubTitleLabel.font = (UIFont(name: "raleway", size: 18))
  midsubTitleLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
  midsubTitleLabel.hidden = true
  contentView.addSubview(midsubTitleLabel)

  
  
    let textFieldframe = CGRect(x:26.66, y: 55, width:286, height: 25)
    textInputField = UITextField(frame: textFieldframe)
    textInputField.text = ""
    textInputField.placeholder = ""
    textInputField.textAlignment = NSTextAlignment.Natural
    textInputField.font = (UIFont(name: "raleway", size: 18))
    textInputField.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    textInputField.hidden = true
  
  textInputField.keyboardType = UIKeyboardType.Default
    textInputField.autocapitalizationType = UITextAutocapitalizationType.Words
    contentView.addSubview(textInputField)
    
    // MARK: Adding phone number field
    
    let countryCodeFrame = CGRect(x:26.66, y: 55, width:30, height: 18)
    countryCodeLabel = UILabel(frame: countryCodeFrame)
    countryCodeLabel.text = "+91"
    countryCodeLabel.textAlignment = NSTextAlignment.Natural
    countryCodeLabel.font = (UIFont(name: "raleway", size: 18))
    countryCodeLabel.textColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
    countryCodeLabel.hidden = true
    contentView.addSubview(countryCodeLabel)

    let countryCodeButtonFrame = CGRect(x:79.98, y: 55+2, width:26.66, height: 18)
    countryCodeButton = UIButton(frame: countryCodeButtonFrame)
    countryCodeButton.contentMode = UIViewContentMode.ScaleAspectFit
    countryCodeButton.opaque = true
    let image = UIImage(named: "dropdown_icon_3x")
    countryCodeButton.setImage(image, forState: UIControlState.Normal)
    countryCodeButton.hidden = true
    //countryCodeButton.addTarget(self, action: "showPicker", forControlEvents: UIControlEvents.TouchUpInside)
    contentView.addSubview(countryCodeButton)
    
    let phoneSepFrame = CGRect(x:23.66*5, y: 55, width:1, height: 18)
    phoneSeparator = UIView(frame: phoneSepFrame)
    phoneSeparator.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)
    phoneSeparator.hidden = true
    contentView.addSubview(phoneSeparator)
  
    let phoneNumberFieldFrame = CGRect(x:136.333, y: 55, width:237, height: 18)
    phoneNumberField = UITextField(frame: phoneNumberFieldFrame)
    phoneNumberField.text = ""
    phoneNumberField.placeholder = "Phonenumber"
    phoneNumberField.textAlignment = NSTextAlignment.Natural
    phoneNumberField.font = (UIFont(name: "raleway", size: 18))
    phoneNumberField.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
  
    phoneNumberField.hidden = true
  
    phoneNumberField.keyboardType = UIKeyboardType.NumberPad
    contentView.addSubview(phoneNumberField)
    
    let countryCodePickerFrame = CGRect(x:26.66, y: -21, width:30, height: 18)
    countryCodePicker = UIPickerView(frame: countryCodePickerFrame)
    countryCodePicker.delegate = self
    countryCodePicker.dataSource = self
    countryCodePicker.hidden = true
    contentView.addSubview(countryCodePicker)
    
    
    // MARK: Right Details
    
    let selectButtonFrame = CGRect(x: self.frame.maxX-76 , y: contentView.frame.midY-3, width:74, height: 44)
    selectButton = UIButton(frame: selectButtonFrame)
    selectButton.contentMode = UIViewContentMode.Right
    selectButton.opaque = true
    let selectImage = UIImage(named: "rightside_arrow_icon_3x")
    selectButton.setImage(selectImage, forState: UIControlState.Normal)
   // selectButton.titleLabel?.text = "Select"
    selectButton.hidden = true
    contentView.addSubview(selectButton)
  
  let addressButtonFrame = CGRect(x: self.frame.maxX-76 , y: contentView.frame.midY-3, width:74, height: 44)
  addressButton = UIButton(frame: selectButtonFrame)
  addressButton.contentMode = UIViewContentMode.Right
  addressButton.opaque = true
  
  // selectButton.titleLabel?.text = "Select"
  addressButton.hidden = true
  contentView.addSubview(addressButton)

   let selectLabelFrame = CGRect(x: self.frame.maxX-116  , y: 45/1.4, width:60, height: 18)
    selectLabel = UILabel(frame: selectLabelFrame)
    selectLabel.text = "Select"
    selectLabel.textColor  = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    selectLabel.textAlignment = NSTextAlignment.Left
    selectLabel.font = (UIFont(name: "raleway", size: 18))
    selectLabel.hidden = true
    contentView.addSubview(selectLabel)

    // MARK: Text Area
    let textAreaFrame = CGRect(x:21.66, y: 44.66 , width: contentView.frame.width-35, height: 290)
    descriptionTextArea = UITextView(frame: textAreaFrame)
    descriptionTextArea.textColor = UIColor(red: 198/255, green: 198/255, blue: 198/255, alpha: 1)
    descriptionTextArea.text = "Enter Description Here"
    descriptionTextArea.font = UIFont(name: "raleway", size: 18)
    descriptionTextArea.hidden = true
  
    descriptionTextArea.scrollEnabled = false
    contentView.addSubview(descriptionTextArea)
  
  //Mark photo frames

  salonPhotoOne = UIImageView(frame: CGRect(x: 27, y: 55  , width:58, height:30))
  salonPhotoOne.contentMode = UIViewContentMode.ScaleAspectFit
 // salonPhotoOne.backgroundColor = UIColor.redColor()
  //salonPhotoOne.image = UIImage(named: "user_img_1x")
  salonPhotoOne.contentMode = UIViewContentMode.ScaleAspectFit
  contentView.backgroundColor=UIColor.whiteColor()
  contentView.addSubview(salonPhotoOne)
  contentView.bringSubviewToFront(salonPhotoOne)
  contentView.clipsToBounds = true

  salonPhotoTwo = UIImageView(frame: CGRect(x: 87 , y: 55  , width:58, height:30))
  salonPhotoTwo.contentMode = UIViewContentMode.ScaleAspectFit
 // salonPhotoTwo.backgroundColor = UIColor.redColor()
  //salonPhotoTwo.image = UIImage(named: "user_img_1x")
  salonPhotoTwo.contentMode = UIViewContentMode.ScaleAspectFit
  contentView.backgroundColor=UIColor.whiteColor()
  contentView.addSubview(salonPhotoTwo)
  contentView.bringSubviewToFront(salonPhotoTwo)
  contentView.clipsToBounds = true
  
  salonPhotoThree = UIImageView(frame: CGRect(x: 147, y: 55  , width:58, height:30))
  salonPhotoThree.contentMode = UIViewContentMode.ScaleAspectFit
  //salonPhotoThree.backgroundColor = UIColor.redColor()
  //salonPhotoThree.image = UIImage(named: "user_img_1x")
  salonPhotoThree.contentMode = UIViewContentMode.ScaleAspectFit
  contentView.backgroundColor=UIColor.whiteColor()
  contentView.addSubview(salonPhotoThree)
  contentView.bringSubviewToFront(salonPhotoThree)
  contentView.clipsToBounds = true
  
  salonPhotoFour = UIImageView(frame: CGRect(x: 207, y: 55  , width:58, height:30))
  salonPhotoFour.contentMode = UIViewContentMode.ScaleAspectFit
  //salonPhotoFour.backgroundColor = UIColor.redColor()
  //salonPhotoFour.image = UIImage(named: "user_img_1x")
  salonPhotoFour.contentMode = UIViewContentMode.ScaleAspectFit
  contentView.backgroundColor=UIColor.whiteColor()
  contentView.addSubview(salonPhotoFour)
  contentView.bringSubviewToFront(salonPhotoFour)
  contentView.clipsToBounds = true

      }
  
  
  
  //MARK: Picker View Implementation
  
  func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
    return 1
  }

  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return countryCodes.count
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var myView = UIView(frame: CGRectMake(0 , 0, pickerView.bounds.width , 18))   //view for picker
    var myLabel = UILabel(frame: CGRectMake(0 , 0, pickerView.bounds.width, 18))
    myLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    myLabel.font = UIFont(name: "raleway", size: 18)
    myLabel.text = "\(countryCodes[row])"
    myView.addSubview(myLabel)
    return myView
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    countryCodeLabel.text = "\(countryCodes[row])"
    countryCodeLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    countryCodePicker.hidden = true
  }
  
  func showPicker() {
    countryCodePicker.hidden = false
    countryCodeLabel.text = ""
  }
  
  
  override func setSelected(selected: Bool, animated: Bool)
  {
    super.setSelected(selected, animated: animated)
     }
  
 

}
