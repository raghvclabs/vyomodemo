//
//  SelectCategoryData.swift
//  Vyomo
//
//  Created by Click Labs on 3/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//
import UIKit
import Foundation

class SelectCategoryData : NSObject {
  
  override init() {
    super.init()
  } 
  var dataForParsing = NSDictionary()
  var data = NSDictionary()
  func listStylistCategories() {
    
    let json = JSON()
    var arr_parameters = NSDictionary(objectsAndKeys: "\(AccessToken)","access_token")
    dispatch_async(dispatch_get_main_queue()){
      json.session("get_category_services", parameters: arr_parameters, completion: {
        response in
        self.data = response as NSDictionary
        println(self.data)
      })
      
      var checkDataReceived : NSTimer = NSTimer.scheduledTimerWithTimeInterval(0.001, target: self, selector: Selector("checkData"), userInfo: nil, repeats: true)
    }
  }
  
  func checkData() {    
    if data.count != 0 {
      dataForParsing = data
    }
  }
}
