import UIKit
import Foundation

class CalendarViewData: NSObject {
  
  override init() {
    super.init()
  }
  
  var permanentCurrentWeekArray : [NSDate] = []
  var currentWeekArray : [NSDate] = []
  var nextWeekArray :[NSDate] = []
  var previousWeekArray :[NSDate] = []
  var currentMonthArray : [String] = []
  var nextMonthArray : [String] = []
  var previousMonthArray : [String] = []
  var dateFormatter = NSDateFormatter()
  var currentWeekString : [String] = []
  var nextWeekString : [String] = []
  var previousWeekString : [String] = []
  var timeArray = [Int]()
  
  
  
  var currentMonthNameString = String()
  var nextMonthNameString = String()
  var previousMonthNameString = String()
  var todayDate = String()
  var currentTime = String()
  var currentMeridiem = String()
  
  var currentWeekDeatil = [String]()
  var nextWeekDeatil = [String]()
  var previousWeekDeatil = [String]()
  
  var firstDayCurrentMonth = String()
  var firstDayNextMonth = String()
  var firstDayPreviousMonth = String()
  
  var getDate = String()
  var getDay = String()
  var getMonth = String()
  var getYear = String()
  
  
  
  func today() {
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    let components = calendar.components(.MonthCalendarUnit | .DayCalendarUnit | .YearCalendarUnit | .CalendarUnitWeekOfMonth | .CalendarUnitWeekday , fromDate: date)
    var dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd"
    var dayName = dateFormatter.stringFromDate(date)
    todayDate = dayName
    dateFormatter.dateFormat = "hh"
    var currentHour = "\(dateFormatter.stringFromDate(date).toInt()!)"
    dateFormatter.dateFormat = "mm"
    var currentMinute = "\(dateFormatter.stringFromDate(date).toInt()!)"
    currentTime = "\(currentHour):\(currentMinute)"
    //println(currentTime)
    dateFormatter.dateFormat = "hh:mm a"
    var meridiem = dateFormatter.stringFromDate(date).componentsSeparatedByString(" ")[1]
    currentMeridiem = meridiem
    println(currentTime)
  }
  
  func currentDateCalculations() {
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    dateFormatter.dateFormat = "dd"
    var currentDayDate = dateFormatter.stringFromDate(date).toInt()!
    getDate = "\(currentDayDate)"
    dateFormatter.dateFormat = "MMMM"
    var currentDayMonth = dateFormatter.stringFromDate(date)
    getMonth = currentDayMonth
    dateFormatter.dateFormat = "EEEE"
    var currentDayDay = dateFormatter.stringFromDate(date)
    getDay = currentDayDay.uppercaseString
    dateFormatter.dateFormat = "yyyy"
    var currentDayYear = dateFormatter.stringFromDate(date)
    getYear = currentDayYear
  }
  
  func previousDateCalculation(multiplier : Int) {
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    let components = NSDateComponents()
    components.day = multiplier
    var previousDate : NSDate =  calendar.dateByAddingComponents(components, toDate: date, options: nil)!
    dateFormatter.dateFormat = "dd"
    var previousDayDate = dateFormatter.stringFromDate(previousDate).toInt()!
    getDate = "\(previousDayDate)"
    dateFormatter.dateFormat = "MMMM"
    var previousDayMonth = dateFormatter.stringFromDate(previousDate)
    getMonth = previousDayMonth
    dateFormatter.dateFormat = "EEEE"
    var previousDayDay = dateFormatter.stringFromDate(previousDate)
    getDay = previousDayDay.uppercaseString
    dateFormatter.dateFormat = "yyyy"
    var previousDayYear = dateFormatter.stringFromDate(previousDate)
    getYear = previousDayYear
    
  }
  
  func nextDateCalculation(multiplier : Int) {
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    let components = NSDateComponents()
    components.day = multiplier
    var nextDate : NSDate =  calendar.dateByAddingComponents(components, toDate: date, options: nil)!
    dateFormatter.dateFormat = "dd"
    var nextDayDate = dateFormatter.stringFromDate(nextDate).toInt()!
    getDate = "\(nextDayDate)"
    dateFormatter.dateFormat = "MMMM"
    var nextDayMonth = dateFormatter.stringFromDate(nextDate)
    getMonth = nextDayMonth
    dateFormatter.dateFormat = "EEEE"
    var nextDayDay = dateFormatter.stringFromDate(nextDate)
    getDay = nextDayDay.uppercaseString
    dateFormatter.dateFormat = "yyyy"
    var nextDayYear = dateFormatter.stringFromDate(nextDate)
    getYear = nextDayYear
  }
  
  func currentWeek(){
    currentWeekDeatil = []
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateComponent = NSDateComponents()
    dateFormatter.dateFormat = "EEEE"
    var dayName = dateFormatter.stringFromDate(date)  //day name contains current day name
    var weeksSunday = ""
    if dayName == "Sunday" {
      weeksSunday = dateFormatter.stringFromDate(date)
      
    } else if dayName == "Monday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -1
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Tuesday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -2
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Wednesday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -3
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Thursday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -4
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Friday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -5
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Saturday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -6
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
    }
    
    var i = 0
    var newSunday : NSDate = dateFormatter.dateFromString(weeksSunday)!
    currentWeekArray.append(newSunday)
    
    // println(newSunday)
    for i in 1...6 {
      dateComponent.day = 1
      newSunday = calendar.dateByAddingComponents(dateComponent, toDate: newSunday, options: nil)!
      // println(newSunday)
      currentWeekArray.append(newSunday)
    }
    
    dateFormatter.dateFormat = "dd"
    for i in 0...6 {
      var currentDate = dateFormatter.stringFromDate(currentWeekArray[i])
      currentWeekString.append(currentDate)
      currentWeekDeatil.append(currentDate)
    }
    
    dateFormatter.dateFormat = "MMMM"
    for i in 0...6 {
      var currentMonth = dateFormatter.stringFromDate(currentWeekArray[i])
      currentWeekDeatil[i] += " " + currentMonth
    }
    
    dateFormatter.dateFormat = "yyyy"
    for i in 0...6 {
      var currentYear = dateFormatter.stringFromDate(currentWeekArray[i])
      currentWeekDeatil[i] += " " + currentYear
    }
  }
  
  func nextWeek(multiplier : Int){
    nextWeekDeatil = []
    nextWeekString = []
    nextWeekArray = []
    let calendar = NSCalendar.currentCalendar()
    var currentSundayDate : NSDate = currentWeekArray[0]
    var dateComponent = NSDateComponents()
    dateComponent.day = 7 * multiplier
    var newSundayDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: currentSundayDate, options: nil)!
    nextWeekArray.append(newSundayDate)
    
    for i in 1...6 {
      dateComponent.day = 1
      newSundayDate = calendar.dateByAddingComponents(dateComponent, toDate: newSundayDate, options: nil)!
      // println(newSundayDate)
      nextWeekArray.append(newSundayDate)
    }
    
    dateFormatter.dateFormat = "dd"
    for i in 0...6 {
      var nextDate = dateFormatter.stringFromDate(nextWeekArray[i])
      nextWeekString.append(nextDate)
      nextWeekDeatil.append(nextDate)
    }
    
    dateFormatter.dateFormat = "MMMM"
    for i in 0...6 {
      var nextMonth = dateFormatter.stringFromDate(nextWeekArray[i])
      nextWeekDeatil[i] += " " + nextMonth
    }
    
    dateFormatter.dateFormat = "yyyy"
    for i in 0...6 {
      var nextYear = dateFormatter.stringFromDate(nextWeekArray[i])
      nextWeekDeatil[i] += " " + nextYear
    }
  }
  
  func previousWeek(multiplier : Int){
    previousWeekDeatil = []
    previousWeekString = []
    previousWeekArray = []
    let calendar = NSCalendar.currentCalendar()
    var currentSundayDate : NSDate = currentWeekArray[0]
    var dateComponent = NSDateComponents()
    dateComponent.day = -7 * (multiplier * -1)
    var newSundayDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: currentSundayDate, options: nil)!
    let components = calendar.components(.MonthCalendarUnit , fromDate: newSundayDate)
    previousWeekArray.append(newSundayDate)
    
    for i in 1...6 {
      dateComponent.day = 1
      newSundayDate = calendar.dateByAddingComponents(dateComponent, toDate: newSundayDate, options: nil)!
      // println(newSundayDate)
      previousWeekArray.append(newSundayDate)
    }
    
    dateFormatter.dateFormat = "dd"
    for i in 0...6 {
      var previousDate = dateFormatter.stringFromDate(previousWeekArray[i])
      previousWeekString.append(previousDate)
      previousWeekDeatil.append(previousDate)
    }
    
    dateFormatter.dateFormat = "MMMM"
    for i in 0...6 {
      var previousMonth = dateFormatter.stringFromDate(previousWeekArray[i])
      previousWeekDeatil[i] += " " + previousMonth
    }
    
    dateFormatter.dateFormat = "yyyy"
    for i in 0...6 {
      var previousYear = dateFormatter.stringFromDate(previousWeekArray[i])
      previousWeekDeatil[i] += " " + previousYear
    }
  }
  
  func currentMonth(){
    
    currentMonthArray = []
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
    var dateComponent = NSDateComponents()
    dateFormatter.dateFormat = "MMMM"
    var monthName = dateFormatter.stringFromDate(date)  //day name contains current month name
    currentMonthNameString = monthName
    dateFormatter.dateFormat = "yyyy"
    var year = dateFormatter.stringFromDate(date)
    currentMonthNameString = currentMonthNameString + "  " + year
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(date)
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    dateFormatter.dateFormat = "EEEE"
    firstDayCurrentMonth = dateFormatter.stringFromDate(newDate)
    let components = calendar.components(.MonthCalendarUnit | .YearCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
    
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      
      for i in 1...31 {
        currentMonthArray.append(String(i))
      }
    } else if monthName == "February"  && leap == true {
      for i in 1...28 {
        currentMonthArray.append(String(i))
      }
    } else if monthName == "February" && leap == false {
      for i in 1...29 {
        currentMonthArray.append(String(i))
      }
    } else {
      for i in 1...30 {
        currentMonthArray.append(String(i))
        
      }
      
    }
    
    // println(currentMonthArray)
    
  }
  
  
  
  func nextMonth(multiplier : Int){
    
    nextMonthArray = []
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
    
    
    var dateComponent = NSDateComponents()
    dateComponent.month = 1 * multiplier
    var nextMonthDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    // println(nextMonthDate)
    
    
    dateFormatter.dateFormat = "MMMM"
    
    var monthName = dateFormatter.stringFromDate(nextMonthDate)  //day name contains current month name
    nextMonthNameString = monthName
    
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(nextMonthDate)
    // println("\nnextMonthDate  \(currentDate)")
    // println("nextMonth \(monthName)")
    
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    
    dateComponent.month = 0
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: nextMonthDate, options: nil)!
    // println(newDate)
    dateFormatter.dateFormat = "EEEE"
    firstDayNextMonth = dateFormatter.stringFromDate(newDate)
    // println("First Day is on \(firstDayNextMonth)")
    let components = calendar.components(.MonthCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
    dateFormatter.dateFormat = "yyyy"
    var year = dateFormatter.stringFromDate(newDate)
    nextMonthNameString = nextMonthNameString + "  " +  year
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      for i in 1...31 {
        nextMonthArray.append(String(i))
      }
      
    } else if monthName == "February"  && leap == true {
      for i in 1...28 {
        nextMonthArray.append(String(i))
      }
      
    } else if monthName == "February" && leap == false {
      for i in 1...29 {
        nextMonthArray.append(String(i))
      }
      
    } else {
      for i in 1...30 {
        nextMonthArray.append(String(i))
      }
    }
  }
  
  func previousMonth(multiplier : Int){
    
    previousMonthArray = []
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
    var dateComponent = NSDateComponents()
    dateComponent.month = -1 * (multiplier * -1)
    var previousMonthDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    dateFormatter.dateFormat = "MMMM"
    var monthName = dateFormatter.stringFromDate(previousMonthDate)  //day name contains current month name
    previousMonthNameString = monthName
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(previousMonthDate)
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    dateComponent.month = 0
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: previousMonthDate, options: nil)!
    dateFormatter.dateFormat = "EEEE"
    firstDayPreviousMonth = dateFormatter.stringFromDate(newDate)
    let components = calendar.components(.MonthCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
    dateFormatter.dateFormat = "yyyy"
    var year = dateFormatter.stringFromDate(newDate)
    previousMonthNameString = previousMonthNameString + "  " +  year
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      for i in 1...31 {
        previousMonthArray.append(String(i))
      }
    } else if monthName == "February"  && leap == false {
      for i in 1...28 {
        previousMonthArray.append(String(i))
      }
    } else if monthName == "February" && leap == true {
      for i in 1...29 {
        previousMonthArray.append(String(i))
      }
    } else {
      for i in 1...30 {
        previousMonthArray.append(String(i))
      }
    }
  }
  
  func timeIntervalArrayIn24HourFormat() {
    timeArray = []
    var hour = 0
    var minute = 0
    var hourString = ""
    var minuteString = ""
    var formattedTime = 0
    var formattedString = ""
    
    for interval in 1...96 {
      if hour < 10 {
        hourString = "0\(hour)"
      } else {
        hourString = "\(hour)"
      }
      
      if minute < 10 {
        minuteString = "0\(minute)"
      } else {
        minuteString = "\(minute)"
      }
      
      minute += 15
      
      if minute == 60 {
        minute = 0
        hour += 1
      }
      
      formattedString = hourString + minuteString
      formattedTime = formattedString.toInt()!
      timeArray.append(formattedTime)
    }
  }
  
  func appointmentInfo(startTime: String, stopTime: String) -> (startIndex: Int,stopIndex: Int,length: Int) {
    let startDate = startTime.componentsSeparatedByString("T")[0]
    let endDate = stopTime.componentsSeparatedByString("T")[0]
    let beginTime : String = startTime.componentsSeparatedByString("T")[1]
    let endTime : String = stopTime.componentsSeparatedByString("T")[1]
    let startHour = beginTime.componentsSeparatedByString(":")[0]
    let startMinute = beginTime.componentsSeparatedByString(":")[1]
    let stopHour = endTime.componentsSeparatedByString(":")[0]
    let stopMinute = endTime.componentsSeparatedByString(":")[1]
    let appointmentStartTime = appointmentTimeInfo(startHour, startMinute: startMinute, stopHour: stopHour, stopMinute: stopMinute).startTime
    let appointmentStopTime = appointmentTimeInfo(startHour, startMinute: startMinute, stopHour: stopHour, stopMinute: stopMinute).stopTime
    let startIndex = appointmentViewInfo(appointmentStartTime, stopTime: appointmentStopTime).startIndex
    let stopIndex = appointmentViewInfo(appointmentStartTime, stopTime: appointmentStopTime).stopIndex
    let length = appointmentViewInfo(appointmentStartTime, stopTime: appointmentStopTime).length
    
    
    
    return (startIndex,stopIndex,length)
  }
  
  func appointmentTimeInfo(startHour: String, startMinute: String, stopHour: String, stopMinute: String) -> (startTime:  Int, stopTime: Int) {
    let beginTime = startHour + startMinute
    let endTime = stopHour + stopMinute
    return (beginTime.toInt()!, endTime.toInt()!)
  }
  
  func appointmentViewInfo(startTime:  Int, stopTime: Int) -> (startIndex: Int,stopIndex: Int,length: Int) {
    timeIntervalArrayIn24HourFormat()
    var startIndex = 0
    var stopindex = 0
    var length = 0
    var startHour = 0
    var startMinute = 0
    var stopHour = 0
    var stopMinute = 0
    for time in 0...95 {
      if startTime == timeArray[time] {
        startIndex = time
      }
      
      if stopTime == timeArray[time] {
        stopindex = time
      }
    }
    
    startHour = startTime / 100
    stopHour = startTime / 100
    startMinute = startTime % 100
    stopMinute = stopTime % 100
    let minuteDifference = stopMinute - startMinute
    let hourDifference = stopHour - startHour
    if minuteDifference < 0 {
      length = (4 * hourDifference) + ((minuteDifference + 60) / 15)
    } else {
      length = (4 * hourDifference) + ((minuteDifference) / 15)
    }
    
    return (startIndex,stopindex,length)
  }
  
  func appointmentDate(date: String, month: String, year: String) -> String {
    let dateNumber = date.toInt()!
    var monthNumber = ""
    switch month {
    case "January" : monthNumber = "01"
    case "February" : monthNumber = "02"
    case "March" : monthNumber = "03"
    case "April" : monthNumber = "04"
    case "May" : monthNumber = "05"
    case "June" : monthNumber = "06"
    case "July" : monthNumber = "07"
    case "August" : monthNumber = "08"
    case "September" : monthNumber = "09"
    case "October" : monthNumber = "10"
    case "November" : monthNumber = "11"
    case "December" : monthNumber = "12"
    default: break
    }
    
    var appointmentDate = year + "-" + monthNumber + "-" + date
    if dateNumber < 10 {
      appointmentDate = year + "-" + monthNumber + "-" + "0\(date)"
    }
    
    return appointmentDate
  }
  
  func appointmentDateFetch(date: String) -> String {
    return date.componentsSeparatedByString("T")[0]
  }
  
  func daysInmonth(month: String, yearString: String) -> Int {
    let year = yearString.toInt()!
    var days = 0
    switch month {
    case "January" : days = 31
    case "February" :
      if year % 4 == 0 || year % 400 == 0 {
        days = 29
      } else {
        days = 28
      }
    case "March" : days = 31
    case "April" : days = 30
    case "May" : days = 31
    case "June" : days = 30
    case "July" : days = 31
    case "August" : days = 31
    case "September" : days = 30
    case "October" : days = 31
    case "November" : days = 30
    case "December" : days = 31
    default: break
    }
    
    return days
  }
  
  func availabilityDay(day: String) -> String {
    var index = ""
    switch day {
    case "sunday": index = "0"
    case "monday": index = "1"
    case "tuesday": index = "2"
    case "wednesday": index = "3"
    case "thursday": index = "4"
    case "friday": index = "5"
    case "saturday": index = "6"
    default : break
    }
    
    return index
  }
  
  
  func appointmentDetails() {
    
  }
}