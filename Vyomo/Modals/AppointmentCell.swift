//
//  AppointmentCell.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/19/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit


class AppointmentCell: UITableViewCell {
  
  let customerNameLabel: UILabel!
  let saloonImage: UIImageView!
  let subTitleLabel: UILabel!
  let totalLabel : UILabel!
  let selectButton:UIButton!
  let costLabel: UILabel!

  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
    super.init(coder: aDecoder)
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    self.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 140)
    selectionStyle = UITableViewCellSelectionStyle.None
    
    saloonImage = UIImageView(frame: CGRect(x: 27, y: 12  , width:28, height:28))
    saloonImage.contentMode = UIViewContentMode.ScaleAspectFit
 
    saloonImage.contentMode = UIViewContentMode.ScaleAspectFit
    contentView.backgroundColor=UIColor.whiteColor()
    contentView.addSubview(saloonImage)
    contentView.bringSubviewToFront(saloonImage)
    contentView.clipsToBounds = true

    
    
    let textFrame = CGRect(x:73.66, y: 12, width:286, height: 20)
    customerNameLabel = UILabel(frame: textFrame)
    customerNameLabel.text = ""
    customerNameLabel.textAlignment = NSTextAlignment.Natural
    customerNameLabel.font = (UIFont(name: "raleway", size: 18))
    customerNameLabel.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
    contentView.addSubview(customerNameLabel)
    
    
    let subTitleFrame = CGRect(x:73.66, y: 33, width:200, height: 20)
    subTitleLabel = UILabel(frame: subTitleFrame)
    subTitleLabel.text = ""
    subTitleLabel.textAlignment = NSTextAlignment.Natural
    subTitleLabel.font = (UIFont(name: "raleway", size: 15))
    subTitleLabel.textColor = UIColor(red: 116/255, green: 116/255, blue: 116/255, alpha: 1)
    contentView.addSubview(subTitleLabel)

    let totalFrame = CGRect(x:73.66, y:88, width:200, height: 20)
    totalLabel = UILabel(frame: totalFrame)
    totalLabel.text = "Total"
    totalLabel.textAlignment = NSTextAlignment.Natural
    totalLabel.font = (UIFont(name: "raleway", size: 17.5))
    totalLabel.textColor = UIColor(red: 38/255, green: 38/255, blue: 38/255, alpha: 1)
    contentView.addSubview(totalLabel)

    var selectButtonFrame = CGRect()
    selectButtonFrame = CGRect(x: self.frame.maxX-76 , y: 9 , width:74, height: 44)
    selectButton = UIButton(frame: selectButtonFrame)
    selectButton.contentMode = UIViewContentMode.Right
    selectButton.opaque = true
    let selectImage = UIImage(named: "expand_icon_3")
    selectButton.setImage(selectImage, forState: UIControlState.Normal)
    contentView.addSubview(selectButton)

    let costFrame = CGRect(x:self.frame.maxX-110, y:88, width:80, height: 20)
    costLabel = UILabel(frame: costFrame)
    costLabel.text = ""
    costLabel.textAlignment = NSTextAlignment.Right
    costLabel.font = (UIFont(name: "raleway", size: 17.5))
    costLabel.textColor = UIColor(red: 38/255, green: 38/255, blue: 38/255, alpha: 1)
    contentView.addSubview(costLabel)

    
   
    
}

  
  override func setSelected(selected: Bool, animated: Bool)
  {
    super.setSelected(selected, animated: animated)
}

}