//
//  TableCell.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/13/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//


import UIKit


class TableCell: UITableViewCell {
  
  let saloonNameLabel: UILabel!
  let saloonImage: UIImageView!
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
     super.init(coder: aDecoder)
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  
   
    let textFrame = CGRect(x:73.66, y: frame.height/2 - 1, width:286, height: 40)
    saloonNameLabel = UILabel(frame: textFrame)
    saloonNameLabel.text = ""
    saloonNameLabel.textAlignment = NSTextAlignment.Natural
    saloonNameLabel.font = (UIFont(name: "raleway", size: 18))
    saloonNameLabel.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
    contentView.addSubview(saloonNameLabel)
    
    
    saloonImage = UIImageView(frame: CGRect(x: 27, y: frame.height/2 + 5  , width:28, height:28))
    saloonImage.contentMode = UIViewContentMode.ScaleAspectFit
    //saloonImage.backgroundColor = UIColor.redColor()
    saloonImage.image = UIImage(named: "user_img_1x")
    saloonImage.contentMode = UIViewContentMode.ScaleAspectFit
    contentView.backgroundColor=UIColor.whiteColor()
    contentView.addSubview(saloonImage)
    contentView.bringSubviewToFront(saloonImage)
    contentView.clipsToBounds = true
  }
 
  override func setSelected(selected: Bool, animated: Bool)
  {
    
    super.setSelected(selected, animated: animated)
    
    
  }
}
    

  

