//
//  MonthAppointmentsData.swift
//  Vyomo
//
//  Created by Click Labs on 3/26/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import Foundation

class MonthAppointmentsData : NSObject {
  var data = NSDictionary()
  var dataForParsing = NSDictionary()
  var checkDataReceived : NSTimer!
  
  func getAppointments (beginningDate : String, endingDate : String) {
    let json = JSON()
    dataForParsing = [:]
    data = [:]
    var start = NSString(string: beginningDate)
    var end = NSString(string: endingDate)
    println(beginningDate)
    println(endingDate)
    var arr_parameters = NSDictionary(objectsAndKeys:
      "YW5kcm9pZF9zdHlsaXN0U2F0IE1hciAyOCAyMDE1IDA5OjM1OjQ1IEdNVCswMDAwIChVVEMp","access_token","1","business_id","2","stylist_id",beginningDate,"start_date",endingDate,"end_date")
    dispatch_async(dispatch_get_main_queue()){
      json.session("stylist_month_calendar", parameters: arr_parameters, completion: {
        response in
        self.data = response as NSDictionary        
      })
      
      self.checkDataReceived = NSTimer.scheduledTimerWithTimeInterval(0.001, target: self, selector: Selector("checkData"), userInfo: nil, repeats: true)
    }
  }
  
  func checkData() {
    if data.count != 0 {
      dataForParsing = data
      checkDataReceived.invalidate()
    }
  }
}